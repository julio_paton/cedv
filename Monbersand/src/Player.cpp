/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "Player.h"
#include "PlayState.h"
#include <Ogre.h>

using namespace Ogre;
using namespace std;

Player::Player(string id, string model, unsigned int mask, Ogre::Vector3 position, string materialName) {
    _id = id;
    _mask = mask;
    _vida = 100;
    _posInicial = position;
    _velocidad = VELOCIDAD_JUGADOR;
    _maxVida = 150;
    _adelante = false;
    _atras = false;
    _izquierda = false;
    _derecha = false;
    _giroIzquierda = false;
    _giroDerecha = false;
    _shooting = false;

    Ogre::SceneManager *scnManager =
            Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
    _entidad = scnManager->createEntity("cube.mesh");
    _entidad->setQueryFlags(_mask);
    

    _nodeModel = scnManager->getRootSceneNode()->createChildSceneNode(_id);
    _nodeModel->setPosition(_posInicial);
    _nodeModel->attachObject(_entidad);
    _nodeModel->setScale(1, 1, 1);
    _nodeModel->showBoundingBox(false);
    _nodeModel->setVisible(false);
//    
    Ogre::SceneNode *nodebox = _nodeModel->createChildSceneNode("M"+_id);
    
    _entidadModel = scnManager->createEntity(model);
    
    if (!materialName.empty())
        _entidadModel->setMaterialName(materialName);
    _entidadModel->setQueryFlags(VOID);
    _entidadModel->setVisibilityFlags(PLAYER_AMIGO);
    nodebox->yaw(Degree(180));
    nodebox->attachObject(_entidadModel);
    nodebox->setScale(0.1, 0.05, 0.1);
    
    _animations.push_back(_entidadModel->getAnimationState("Idle"));
    _animations.push_back(_entidadModel->getAnimationState("Run"));
    
    // Default behaviour
    disableAnimations();
    _activeAnimation = _animations.at(0);
    selectAnimation(0, true);
    
    _nodeWeapon = _nodeModel->createChildSceneNode("WeaponP1");
    cout << "creado player: " << _id << endl;
}

Player::~Player(){
	cout << "Destructor player" << endl;
}

void Player::setVida(int vida) {
    _vida += vida;
    if (_vida > _maxVida) {
        _vida = _maxVida;
    } else if (_vida <= 0) {
        _vida = 0;
    }
}

void Player::setMunition(unsigned int munition) {
    for (WeaponPtr weapon : _weapons) {
        weapon->setMunition(munition);
    }
}
void Player::selectAnimation(const unsigned int pIndex, bool pLoop) {
    _activeAnimation->setEnabled(false);

    if (pIndex < _animations.size()) {
        _activeAnimation = _animations.at(pIndex);
        _activeAnimation->setEnabled(true);
        _activeAnimation->setTimePosition(0.0);
        _activeAnimation->setLoop(pLoop);

    } else {
        // Animation doesn't exist
        // Default state
        _activeAnimation = _animations.at(0);
        _activeAnimation->setLoop(true);
    }

}

void Player::disableAnimations() {
    // Desactivate all animations
    itAnimation = _animations.begin();
    while (itAnimation != _animations.end()) {
        (*itAnimation)->setEnabled(false);
        itAnimation++;
    }

}

void Player::update(Ogre::Real pTime, Ogre::Real _rotx, Ogre::Real _roty) {
    if(_adelante || _atras || _izquierda || _derecha){
        if (_activeAnimation->getAnimationName() == "Idle")
            selectAnimation(1, true);
    }else if (_activeAnimation->getAnimationName() == "Run"){
        selectAnimation(0, true);
    }
    
    if (!_activeAnimation->hasEnded()) {
        _activeAnimation->addTime(pTime);
    }
    Ogre::Vector3 vt = Ogre::Vector3::ZERO;
    Ogre::Real rY = 0;
    Ogre::Vector3 vOrientacionZ= _nodeModel->getOrientation() * Ogre::Vector3::UNIT_Z;
    Ogre::Vector3 vOrientacionX= _nodeModel->getOrientation() * Ogre::Vector3::UNIT_X;
    bool adelanteT,atrasT,izquierdaT,derechaT,gizqT,gdxaT;
    //Comprobamos si admitimos movimientos
    adelanteT = _adelante;
    atrasT = _atras;
    izquierdaT = _izquierda;
    derechaT = _derecha;
    gizqT = _giroIzquierda;
    gdxaT = _giroDerecha;
    //Adelante
    if (_adelante) {
        if ((PlayState::getSingletonPtr()->isCollision(_nodeModel->getPosition(), -vOrientacionZ, (WALL|PLAYER_ENEMIGO), 3)) ||
            (PlayState::getSingletonPtr()->isCollision(_nodeWeapon->_getDerivedPosition()+Ogre::Vector3(0, 0, -1.0), -vOrientacionZ, (WALL|PLAYER_ENEMIGO), 2)) ){
            adelanteT = false;
        }
    }
    //Atras
    if (_atras) {
        if ((PlayState::getSingletonPtr()->isCollision(_nodeModel->getPosition(), vOrientacionZ, (WALL|PLAYER_ENEMIGO), 2)) ||
            (PlayState::getSingletonPtr()->isCollision(_nodeWeapon->_getDerivedPosition()+Ogre::Vector3(0, 0, -1.0), vOrientacionZ, (WALL|PLAYER_ENEMIGO), 3)) ){
            atrasT = false;
        }
    }
    //Lateral Izquierda
    if (_izquierda) {
        if ((PlayState::getSingletonPtr()->isCollision(_nodeModel->getPosition(), -vOrientacionX, (WALL|PLAYER_ENEMIGO), 2)) ||
            (PlayState::getSingletonPtr()->isCollision(_nodeWeapon->_getDerivedPosition()+Ogre::Vector3(0, 0, -1.0), -vOrientacionX, (WALL|PLAYER_ENEMIGO), 0.5)) ||
            (PlayState::getSingletonPtr()->isCollision(_nodeWeapon->_getDerivedPosition()+Ogre::Vector3(0, 0, -0.7), -vOrientacionX, (WALL|PLAYER_ENEMIGO), 0.5)) ||
            (PlayState::getSingletonPtr()->isCollision(_nodeWeapon->_getDerivedPosition()+Ogre::Vector3(0, 0, -0.3), -vOrientacionX, (WALL|PLAYER_ENEMIGO), 0.4)) ){
            izquierdaT = false;
        }
    }
    //Lateral Derecha
    if (_derecha) {
        if ((PlayState::getSingletonPtr()->isCollision(_nodeModel->getPosition(), vOrientacionX, (WALL|PLAYER_ENEMIGO), 3)) ||
            (PlayState::getSingletonPtr()->isCollision(_nodeWeapon->_getDerivedPosition()+Ogre::Vector3(0, 0, -1.0), vOrientacionX, (WALL|PLAYER_ENEMIGO), 0.5)) ||
            (PlayState::getSingletonPtr()->isCollision(_nodeWeapon->_getDerivedPosition()+Ogre::Vector3(0, 0, -0.7), vOrientacionX, (WALL|PLAYER_ENEMIGO), 0.5)) ||
            (PlayState::getSingletonPtr()->isCollision(_nodeWeapon->_getDerivedPosition()+Ogre::Vector3(0, 0, -0.3), vOrientacionX, (WALL|PLAYER_ENEMIGO), 0.4)) ){
            derechaT = false;
        }
    }
    //Giro Izquierda
    if (_giroIzquierda || _rotx>0.0) {
        if ((PlayState::getSingletonPtr()->isCollision(_nodeWeapon->_getDerivedPosition()+Ogre::Vector3(0, 0, -1.0), -vOrientacionX, WALL, 0.5)) ||
            (PlayState::getSingletonPtr()->isCollision(_nodeWeapon->_getDerivedPosition()+Ogre::Vector3(0, 0, -0.7), -vOrientacionX, WALL, 0.5)) ||
            (PlayState::getSingletonPtr()->isCollision(_nodeWeapon->_getDerivedPosition()+Ogre::Vector3(0, 0, -0.3), -vOrientacionX, WALL, 0.4)) ){
            gizqT = false;
            _rotx=0.0;
        }
    }
    //Giro Derecha
    if (_giroDerecha || _rotx<0.0) {
        if ((PlayState::getSingletonPtr()->isCollision(_nodeWeapon->_getDerivedPosition()+Ogre::Vector3(0, 0, -1.0), vOrientacionX, WALL, 0.5)) ||
            (PlayState::getSingletonPtr()->isCollision(_nodeWeapon->_getDerivedPosition()+Ogre::Vector3(0, 0, -0.7), vOrientacionX, WALL, 0.5)) ||
            (PlayState::getSingletonPtr()->isCollision(_nodeWeapon->_getDerivedPosition()+Ogre::Vector3(0, 0, -0.3), vOrientacionX, WALL, 0.4)) ){
            gdxaT = false;
            _rotx=0.0;
        }
    }
   
    //Nos movemos en lo marcado y lo que podemos
    if (adelanteT) vt += Ogre::Vector3(0, 0, -1);
    if (atrasT) vt += Ogre::Vector3(0, 0, 1);
    if (gizqT) rY = 1.5;
    if (gdxaT) rY = -1.5;
    if (izquierdaT) vt += Ogre::Vector3(-1, 0, 0);
    if (derechaT) vt += Ogre::Vector3(1, 0, 0);

    if (pTime > (1.0 / 30.0)) pTime = 1.0 / 30.0;

    if (vt != Ogre::Vector3(0, 0, 0) || _rotx != 0.0 || _roty != 0.0 || rY != 0)
        PlayState::getSingletonPtr()->moverCamara(vt * pTime * _velocidad, Ogre::Radian(_rotx * 0.005), Ogre::Radian(_roty * 0.005), Ogre::Radian(rY * pTime));
    
}

void Player::addWeapon(WeaponPtr lWeapon) {
    _weapons.push_back(lWeapon);
    _selectedWeapon = lWeapon;
}
