/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "CreditsState.h"

template<> CreditsState* Ogre::Singleton<CreditsState>::msSingleton = 0;

using namespace std;

void CreditsState::enter() {
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->createCamera("CreditsCamera");

    _viewport = _root->getAutoCreatedWindow()->getViewport(0);

    _exitGame = false;
    
    createGUI();

}

void CreditsState::exit() {
    CEGUI::WindowManager::getSingleton().destroyWindow("CreditsWindow");
    _sceneMgr->destroyCamera("CreditsCamera");
}

void CreditsState::pause() {
}

void CreditsState::resume() {
}

bool CreditsState::frameStarted(const Ogre::FrameEvent& evt) {
    return true;
}

bool CreditsState::frameEnded(const Ogre::FrameEvent& evt) {
    if (_exitGame)
        return false;

    return true;
}

void CreditsState::keyPressed(const OIS::KeyEvent &e) {
}

void CreditsState::keyReleased(const OIS::KeyEvent &e) {
}

void CreditsState::mouseMoved(const OIS::MouseEvent &e) {
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton CreditsState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void CreditsState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void CreditsState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

CreditsState* CreditsState::getSingletonPtr() {
    return msSingleton;
}

CreditsState& CreditsState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void CreditsState::createGUI() {

    //Mouse
    CEGUI::MouseCursor::getSingleton().setVisible(true);
    CEGUI::System::getSingleton().setDefaultFont("Scarface-36");
    CEGUI::Window* credits = CEGUI::WindowManager::getSingleton().loadWindowLayout(
            "Credits.layout");
    (CEGUI::WindowManager::getSingleton().getWindow("Credits/ExitButton"))->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&CreditsState::Volver, this));
   
    CEGUI::System::getSingleton().getGUISheet()->addChildWindow(credits);
    
}

bool CreditsState::Volver(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    popState();
    return true;
}