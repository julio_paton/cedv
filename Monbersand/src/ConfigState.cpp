/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "ConfigState.h"
#include "PlayState.h"
#include "IntroState.h"

template<> ConfigState* Ogre::Singleton<ConfigState>::msSingleton = 0;

void ConfigState::enter() {
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->createCamera("ConfigCamera");

    _viewport = _root->getAutoCreatedWindow()->getViewport(0);

    // Mapa de teclas
    _mapKeyName[OIS::KC_Q] = "Q";
    _mapKeyName[OIS::KC_A] = "A";
    _mapKeyName[OIS::KC_Z] = "Z";
    _mapKeyName[OIS::KC_W] = "W";
    _mapKeyName[OIS::KC_S] = "S";
    _mapKeyName[OIS::KC_X] = "X";
    _mapKeyName[OIS::KC_E] = "E";
    _mapKeyName[OIS::KC_D] = "D";
    _mapKeyName[OIS::KC_C] = "C";
    _mapKeyName[OIS::KC_R] = "R";
    _mapKeyName[OIS::KC_F] = "F";
    _mapKeyName[OIS::KC_V] = "V";
    _mapKeyName[OIS::KC_T] = "T";
    _mapKeyName[OIS::KC_G] = "G";
    _mapKeyName[OIS::KC_B] = "B";
    _mapKeyName[OIS::KC_Y] = "Y";
    _mapKeyName[OIS::KC_H] = "H";
    _mapKeyName[OIS::KC_N] = "N";
    _mapKeyName[OIS::KC_U] = "U";
    _mapKeyName[OIS::KC_J] = "J";
    _mapKeyName[OIS::KC_M] = "M";
    _mapKeyName[OIS::KC_I] = "I";
    _mapKeyName[OIS::KC_K] = "K";
    _mapKeyName[OIS::KC_O] = "O";
    _mapKeyName[OIS::KC_L] = "L";
    _mapKeyName[OIS::KC_P] = "P";
    _mapKeyName[OIS::KC_UP] = "F_ARR";
    _mapKeyName[OIS::KC_DOWN] = "F_BAJO";
    _mapKeyName[OIS::KC_LEFT] = "F_IZQ";
    _mapKeyName[OIS::KC_RIGHT] = "F_DCHA";

    createGUI();

}

void ConfigState::exit() {
    CEGUI::WindowManager::getSingleton().destroyWindow("ConfigWindow");
    _sceneMgr->destroyCamera("ConfigCamera");
}

void ConfigState::pause() {
}

void ConfigState::resume() {
}

void ConfigState::loadConf() {

    _filename = "media/conf/player.conf";

    //Load file
    std::ifstream file;
    file.open(_filename.c_str());

    valorDefecto();

    if (!file) {
        _foundConf = false;
    } else {
        _foundConf = true;
        std::string line;
        size_t lineNo = 0;
        while (std::getline(file, line)) {
            lineNo++;
            std::string temp = line;

            if (temp.empty())
                continue;

            if (lineNo == 1)
                extractName(line);
            else
                loadKeys(line);

        }

        file.close();
    }

}

void ConfigState::extractName(const std::string &line) {
    std::string temp = line;
    temp.erase(0, temp.find_first_not_of("\t "));
    size_t sepPos = temp.find('#');

    std::string name;
    std::string volumen;

    name = line.substr(0, sepPos);
    volumen = line.substr(sepPos + 1);
    
    volumen.erase(0, volumen.find_first_not_of("\t "));
    volumen.erase(volumen.find_last_not_of("\t ") + 1);

    _playerName = name;
    _volumen = Ogre::StringConverter::parseInt(volumen, 0);
    
    GameManager::getSingletonPtr()->setVolumen(_volumen);

}

void ConfigState::loadKeys(const std::string &line) {
    std::string temp = line;
    std::string par;
    int action;
    int code;
    temp.erase(0, temp.find_first_not_of("\t "));
    size_t sepPos;

    sepPos = temp.find(':');
    action = Ogre::StringConverter::parseInt(line.substr(0, sepPos), 0);
    code = Ogre::StringConverter::parseInt(line.substr(sepPos + 1), 0);
    PlayState::getSingletonPtr()->_mapKeys[action] = static_cast<OIS::KeyCode> (code);
}

std::string ConfigState::textoTeclaAccion(int action) {
    return _mapKeyName[(PlayState::getSingletonPtr()->_mapKeys[action])];
}

void ConfigState::valorDefecto() {
    // Nombre del Jugador
    _playerName = "Jugador";

    // Volumen
    _volumen = 5;

    // Inicializamos teclas
    PlayState::getSingletonPtr()->_mapKeys[A_UP] = OIS::KC_W;
    PlayState::getSingletonPtr()->_mapKeys[A_DOWN] = OIS::KC_S;
    PlayState::getSingletonPtr()->_mapKeys[A_LEFT] = OIS::KC_A;
    PlayState::getSingletonPtr()->_mapKeys[A_RIGHT] = OIS::KC_D;
    PlayState::getSingletonPtr()->_mapKeys[A_TURN_LEFT] = OIS::KC_Q;
    PlayState::getSingletonPtr()->_mapKeys[A_TURN_RIGHT] = OIS::KC_E;
    PlayState::getSingletonPtr()->_mapKeys[A_RELOAD] = OIS::KC_R;
}

bool ConfigState::frameStarted(const Ogre::FrameEvent& evt) {

    CEGUI::WindowManager::getSingleton().getWindow("Opciones/Pause")->setVisible(_cambioTecla);

    (CEGUI::WindowManager::getSingleton().getWindow(
            "Opciones/Adelante"))->setText(textoTeclaAccion(A_UP));
    (CEGUI::WindowManager::getSingleton().getWindow(
            "Opciones/Atras"))->setText(textoTeclaAccion(A_DOWN));
    (CEGUI::WindowManager::getSingleton().getWindow(
            "Opciones/Izquierda"))->setText(textoTeclaAccion(A_LEFT));
    (CEGUI::WindowManager::getSingleton().getWindow(
            "Opciones/Derecha"))->setText(textoTeclaAccion(A_RIGHT));
    (CEGUI::WindowManager::getSingleton().getWindow(
            "Opciones/GiroIzq"))->setText(textoTeclaAccion(A_TURN_LEFT));
    (CEGUI::WindowManager::getSingleton().getWindow(
            "Opciones/GiroDcha"))->setText(textoTeclaAccion(A_TURN_RIGHT));
    (CEGUI::WindowManager::getSingleton().getWindow(
            "Opciones/Reload"))->setText(textoTeclaAccion(A_RELOAD));

    return true;
}

bool ConfigState::frameEnded(const Ogre::FrameEvent& evt) {
    if (_exitGame)
        return false;

    return true;
}

void ConfigState::keyPressed(const OIS::KeyEvent &e) {
    CEGUI::System::getSingleton().injectKeyDown(e.key);
    CEGUI::System::getSingleton().injectChar(e.text);
    if (_cambioTecla && teclaValida(e)) {
        if(e.key != OIS::KC_ESCAPE){
            PlayState::getSingletonPtr()->_mapKeys[_accionActiva] = e.key;
            CEGUI::WindowManager::getSingleton().getWindow("Opciones/Error")->setVisible(false);
        }
        _cambioTecla = false;
    }else{
        CEGUI::WindowManager::getSingleton().getWindow("Opciones/Error")->setVisible(true);
    }
}

void ConfigState::keyReleased(const OIS::KeyEvent &e) {
    CEGUI::System::getSingleton().injectKeyUp(e.key);
}

bool ConfigState::teclaValida(const OIS::KeyEvent &e){
    if(e.key == OIS::KC_ESCAPE)
        return true;
    if (_mapKeyName.find(e.key) != _mapKeyName.end())
        return true;
    else
        return false;
}
void ConfigState::mouseMoved(const OIS::MouseEvent &e) {
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton ConfigState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void ConfigState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void ConfigState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

ConfigState* ConfigState::getSingletonPtr() {
    return msSingleton;
}

ConfigState& ConfigState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void ConfigState::createGUI() {

    //Mouse
    CEGUI::MouseCursor::getSingleton().setVisible(true);
    CEGUI::System::getSingleton().setDefaultFont("Scarface-36");
    CEGUI::Window* conf = CEGUI::WindowManager::getSingleton().loadWindowLayout(
            "Config.layout");
    CEGUI::WindowManager::getSingleton().getWindow("Opciones/Pause")->setVisible(false);
    CEGUI::WindowManager::getSingleton().getWindow("Opciones/Error")->setVisible(false);
    CEGUI::WindowManager::getSingleton().getWindow("Opciones/NickEditBox")->setText(_playerName);
    CEGUI::WindowManager::getSingleton().getWindow("Opciones/VolumenSpinner")->setProperty("CurrentValue", Ogre::StringConverter::toString(_volumen));
    (CEGUI::WindowManager::getSingleton().getWindow("Opciones/VolumenSpinner"))->subscribeEvent(CEGUI::PushButton::EventKeyUp, CEGUI::Event::Subscriber(&ConfigState::Volumen, this));
    (CEGUI::WindowManager::getSingleton().getWindow("Opciones/AdelanteButton"))->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ConfigState::cambiarAdelante, this));
    (CEGUI::WindowManager::getSingleton().getWindow("Opciones/AtrasButton"))->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ConfigState::cambiarAtras, this));
    (CEGUI::WindowManager::getSingleton().getWindow("Opciones/IzquierdaButton"))->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ConfigState::cambiarIzquierda, this));
    (CEGUI::WindowManager::getSingleton().getWindow("Opciones/DerechaButton"))->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ConfigState::cambiarDerecha, this));
    (CEGUI::WindowManager::getSingleton().getWindow("Opciones/GiroIzqButton"))->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ConfigState::cambiarGiroIzq, this));
    (CEGUI::WindowManager::getSingleton().getWindow("Opciones/GiroDchaButton"))->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ConfigState::cambiarGiroDcha, this));
    (CEGUI::WindowManager::getSingleton().getWindow("Opciones/ReloadButton"))->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ConfigState::cambiarReload, this));
    (CEGUI::WindowManager::getSingleton().getWindow("Opciones/SaveButton"))->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ConfigState::Guardar, this));
    (CEGUI::WindowManager::getSingleton().getWindow("Opciones/ResetButton"))->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ConfigState::Restablecer, this));
    (CEGUI::WindowManager::getSingleton().getWindow("Opciones/ExitButton"))->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ConfigState::Volver, this));

    CEGUI::System::getSingleton().getGUISheet()->addChildWindow(conf);
}

void ConfigState::saveName(string name) {
    _playerName = name;
    
    std::ofstream file;
    file.open(_filename.c_str(), std::ios::out | std::ios::trunc);
    
    if (file.is_open()) {
        file << name << "#" << _volumen << std::endl;
        file << A_UP << ":" << PlayState::getSingletonPtr()->_mapKeys[A_UP] << std::endl;
        file << A_DOWN << ":" << PlayState::getSingletonPtr()->_mapKeys[A_DOWN] << std::endl;
        file << A_LEFT << ":" << PlayState::getSingletonPtr()->_mapKeys[A_LEFT] << std::endl;
        file << A_RIGHT << ":" << PlayState::getSingletonPtr()->_mapKeys[A_RIGHT] << std::endl;
        file << A_TURN_LEFT << ":" << PlayState::getSingletonPtr()->_mapKeys[A_TURN_LEFT] << std::endl;
        file << A_TURN_RIGHT << ":" << PlayState::getSingletonPtr()->_mapKeys[A_TURN_RIGHT] << std::endl;
        file << A_RELOAD << ":" << PlayState::getSingletonPtr()->_mapKeys[A_RELOAD] << std::endl;
        file.close();
    }
}

bool ConfigState::cambiarAdelante(const CEGUI::EventArgs &e) {
    _accionActiva = A_UP;
    _cambioTecla = true;
    return true;
}

bool ConfigState::cambiarAtras(const CEGUI::EventArgs &e) {
    _accionActiva = A_DOWN;
    _cambioTecla = true;
    return true;
}

bool ConfigState::cambiarIzquierda(const CEGUI::EventArgs &e) {
    _accionActiva = A_LEFT;
    _cambioTecla = true;
    return true;
}

bool ConfigState::cambiarDerecha(const CEGUI::EventArgs &e) {
    _accionActiva = A_RIGHT;
    _cambioTecla = true;
    return true;
}

bool ConfigState::cambiarGiroIzq(const CEGUI::EventArgs &e) {
    _accionActiva = A_TURN_LEFT;
    _cambioTecla = true;
    return true;
}

bool ConfigState::cambiarGiroDcha(const CEGUI::EventArgs &e) {
    _accionActiva = A_TURN_RIGHT;
    _cambioTecla = true;
    return true;
}

bool ConfigState::cambiarReload(const CEGUI::EventArgs &e) {
    _accionActiva = A_RELOAD;
    _cambioTecla = true;
    return true;
}

bool ConfigState::Volumen(const CEGUI::EventArgs &e) {
    std::string aux = (CEGUI::WindowManager::getSingleton().getWindow("Opciones/VolumenSpinner")->getProperty("CurrentValue")).c_str();
    _volumen = Ogre::StringConverter::parseInt(aux, 0);
    GameManager::getSingletonPtr()->setVolumen(_volumen);
    return true;
}

bool ConfigState::Guardar(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    Volumen(e);
    saveName((CEGUI::WindowManager::getSingleton().getWindow("Opciones/NickEditBox")->getText()).c_str());
    return true;
}

bool ConfigState::Restablecer(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    valorDefecto();
    return true;
}

bool ConfigState::Volver(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    popState();
    return true;
}