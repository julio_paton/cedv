/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#include "ExplosionsManager.h"

static const float EXPLOSION_TIME = 0.01;

ExplosionsManager::ExplosionsManager() {

	_explosionsBillSet = Ogre::Root::getSingletonPtr()->getSceneManager(
			"SceneManager")->createBillboardSet("Explosions");
	_explosionsBillSet->setMaterialName("Explosions/Explosion1");

	Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->getRootSceneNode()->attachObject(
			_explosionsBillSet);

}

ExplosionsManager::~ExplosionsManager() {
	_explosionsBoards.clear();
}

void ExplosionsManager::finishExplosions() {

	time_t current;
	time(&current);

	for (unsigned int i = 0; i < _explosionsBoards.size(); i++) {
		double diff = difftime(current,_explosionsBoards[i].second);
		if ( diff >= EXPLOSION_TIME){
			_explosionsBillSet->removeBillboard(_explosionsBoards[i].first);
			_explosionsBoards.erase(_explosionsBoards.begin() + i);
		}
	}
}

void ExplosionsManager::createExplosion(Ogre::Vector3 pos, Ogre::Real size) {

	Ogre::Billboard *lExplosion = _explosionsBillSet->createBillboard(pos);
	lExplosion->setDimensions(size, size);
	time_t start;
	time(&start);
	_explosionsBoards.push_back(std::pair<Ogre::Billboard*, time_t>(lExplosion, start));
}
