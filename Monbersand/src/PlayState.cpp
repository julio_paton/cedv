/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#include "IntroState.h"
#include "PlayState.h"
#include "PauseState.h"
#include "FinishState.h"

#include <algorithm>

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

using namespace Ogre;
using namespace std;

void PlayState::enter() {
    _root = Ogre::Root::getSingletonPtr();
    _mInputDevice = GameManager::getSingletonPtr()->getInputManager();
    GameManager::getSingletonPtr()->stopTrackMenu();
    GameManager::getSingletonPtr()->playTrackGame();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->createCamera("PlayerCamera");
    _camera->setPosition(Ogre::Vector3(0, 1, 0));
    _camera->lookAt(Ogre::Vector3(0, 1, -1.0));
    _camera->setNearClipDistance(0.1);
    _camera->setFarClipDistance(500);

    _cameraNode = _sceneMgr->getRootSceneNode()->createChildSceneNode();
    _cameraNode->setPosition(0, 1, 0);

    _cameraYawNode = _cameraNode->createChildSceneNode();
    _cameraPitchNode = _cameraYawNode->createChildSceneNode();
    _cameraRollNode = _cameraPitchNode->createChildSceneNode();
    _cameraRollNode->attachObject(_camera);

    _cameraGod = _sceneMgr->createCamera("GodCamera");
    _cameraGod->setPosition(Ogre::Vector3(0, 40, 25));
    _cameraGod->lookAt(Ogre::Vector3(0, 0, 0));
    _cameraGod->setNearClipDistance(5);
    _cameraGod->setFarClipDistance(200);

    _cameraMap = _sceneMgr->createCamera("MapCamera");
    _cameraMap->setPosition(Ogre::Vector3(0, 20, 0));
    _cameraMap->lookAt(Ogre::Vector3(1, 0, 0));
    _cameraMap->setNearClipDistance(5);
    _cameraMap->setFarClipDistance(200);
    _cameraMap->yaw(Ogre::Radian(Ogre::Degree(90)));

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
    _viewport->setVisibilityMask(~PLAYER_AMIGO);
    // Nuevo background colour.
    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    _camera->setAspectRatio(width / height);

    //Inicializa rayQuery
    _raySceneQuery = _sceneMgr->createRayQuery(Ogre::Ray());
    _raySceneQueryMov = _sceneMgr->createRayQuery(Ogre::Ray());

    // Creamos la escena
    cout << "1 - creando escena" << endl;
    createScene();
    // Creando la gui
    cout << "2 - creando gui" << endl;
    createGUI();
    // Inicializando variables
    cout << "3 - inicializando variables" << endl;
    initializeVars();
    // Empezamos juego nuevo (llamada a los personajes??)
    cout << "4 - empezamos juego" << endl;
    newGame();

    //Se inicializa la semilla para el random
    srand(time(NULL));
}

void PlayState::exit() {
    GameManager::getSingletonPtr()->stopTrackGame();
    GameManager::getSingletonPtr()->playTrackMenu();
    CEGUI::ImagesetManager::getSingleton().destroy(CEGUI::ImagesetManager::getSingleton().get("RTTImageset"));
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    _sceneMgr->destroyAllCameras();

    _root->getTextureManager()->remove("RTT");
    _root->getAutoCreatedWindow()->removeAllViewports();

    _enemies.clear();
    _items.clear();
    _sceneMgr->clearScene();
}

void PlayState::pause() {
    _cronoFX->pause();
    _cronoTXT->pause();
    _cronoPAUSE->pause();
    _cronoGAME->pause();
    GameManager::getSingletonPtr()->pauseTrackGame();
}

void PlayState::resume() {
    _cronoFX->resume();
    _cronoTXT->resume();
    _cronoPAUSE->pause();
    _cronoGAME->resume();
    GameManager::getSingletonPtr()->playTrackGame();
    CEGUI::MouseCursor::getSingleton().setVisible(false);
}

bool PlayState::frameStarted(const Ogre::FrameEvent& evt) {

    _deltaT = evt.timeSinceLastFrame;
    CEGUI::System::getSingleton().injectTimePulse(_deltaT);
    if (_inPause) {
        if (_cronoPAUSE->isOver(1.2)) {
            _inPause = false;
            _cronoGAME->start();
        }
    } else {
        _playerOne->update(_deltaT, _rotx, _roty);
        _rotx = 0.0;
        _roty = 0.0;

        if (_blood) {
            if (_cronoFX->isOver(0.6)) {
                _blood = false;
                CEGUI::WindowManager::getSingleton().getWindow("Game/Sangre")->setVisible(
                        false);
            }
        }
        if (_text) {
            if (_cronoTXT->isOver(1.2)) {
                _text = false;
                CEGUI::WindowManager::getSingleton().getWindow(
                        "Game/Player/Mensajes")->setVisible(false);
            }
        }

        //Enemies
        for (EnemyPtr enemy : _enemies) {
            enemy->update(_deltaT);
        }
        if (_enemyDead != nullptr) {
            _enemyFactory->checkNewEnemyCreation(_enemies, _enemyDead);
            _enemyDead = nullptr;
        }

        //Items
        for (ItemPtr item : _items) {
            item->update(_deltaT);
        }
        //Comprobamos si chocamos con la puerta   _sceneMgr->getEntity("Gateway")
        Ogre::Vector3 vOrientacionZ = _sceneMgr->getSceneNode("WallGateway")->getOrientation() * Ogre::Vector3::UNIT_Z;

        if (isCollision(_sceneMgr->getSceneNode("WallGateway")->getPosition(), -vOrientacionZ, PLAYER_AMIGO, 8)) {
            enterInGateway();
        }

        _playerOne->getWeapon()->update(_deltaT, _playerOne->_isShooting());

        if (_playerOne->_isShooting()) {
            if (_playerOne->getWeapon()->isShoot()) {
                cout << "Disparo " << endl;
                _raySceneQuery->setRay(Ogre::Ray(Ogre::Vector3(_cameraNode->getPosition().x, 2, _cameraNode->getPosition().z), _camera->getDerivedDirection()));
                _raySceneQuery->setSortByDistance(true);
                _raySceneQuery->setQueryMask(PLAYER_ENEMIGO | PLAYER_ENEMIGO_HEAD);
                Ogre::RaySceneQueryResult &result = _raySceneQuery->execute();
                Ogre::RaySceneQueryResult::iterator it;
                it = result.begin();
                if (it != result.end()) {
                    for (EnemyPtr enemy : _enemies) {
                        if (it->movable->getParentSceneNode()->getName() == enemy->getId()) {
                            enemy->setHit(_playerOne->getWeapon()->getDamage());
                        } else if (it->movable->getParentSceneNode()->getName() == ("H" + enemy->getId())) {
                            enemy->setHit(_playerOne->getWeapon()->getDamage() + 50);
                        }
                    }
                }

            }
        }

        Ogre::Vector3 vt(0, 0, 0);
        Ogre::Real tSpeed = 20.0;
        bool mbmiddle;
        // Si usamos la rueda, desplazamos en Z la camara ------------------
        vt += Ogre::Vector3(0, 0, -50) * _deltaT
                * _mInputDevice->getMouse()->getMouseState().Z.rel;
        _cameraGod->moveRelative(vt * _deltaT * tSpeed);

        // Botones del raton pulsados? -------------------------------------
        mbmiddle = _mInputDevice->getMouse()->getMouseState().buttonDown(
                OIS::MB_Middle);

        if (mbmiddle) { // Con boton medio pulsado, rotamos camara ---------
            float rotx = _mInputDevice->getMouse()->getMouseState().X.rel * _deltaT
                    * -1;
            float roty = _mInputDevice->getMouse()->getMouseState().Y.rel * _deltaT
                    * -1;
            _cameraGod->yaw(Ogre::Radian(rotx));
            _cameraGod->pitch(Ogre::Radian(roty));
        }
        _cameraMap->setPosition(
                Ogre::Vector3(_playerOne->getNode()->getPosition().x, 20,
                _playerOne->getNode()->getPosition().z));
        // Empezamos con las querys de colisiones
        Ogre::SceneQueryMovableIntersectionList ilist;
        Ogre::SceneQueryMovableIntersectionList::iterator it;
        // Primero comprobamos
        Ogre::IntersectionSceneQueryResult &result = _intSceneQueryPlIt->execute();
        ilist = result.movables2movables;
        it = ilist.begin();

        if (it != ilist.end()) {
            Ogre::MovableObject* mov1 = it->first;
            Ogre::MovableObject* mov2 = it->second;

            string itemPicked = "";
            if (mov1->getParentSceneNode()->getName().find("Item")
                    != std::string::npos)
                itemPicked = mov1->getParentSceneNode()->getName();
            else if (mov2->getParentSceneNode()->getName().find("Item")
                    != std::string::npos)
                itemPicked = mov2->getParentSceneNode()->getName();

            for (ItemPtr item : _items) {
                if (itemPicked == item->getId()) {
                    pickItem(item->getType(), item->getValue());
                }
            }
        }
    }
    //Actualizamos la GUI
    if (_playerOne->getVida() >= 50) {
        CEGUI::WindowManager::getSingleton().getWindow("Game/Player/Salud")->setText(
                "[colour='FF00FFFD']"
                + Ogre::StringConverter::toString(
                _playerOne->getVida()));
    } else {
        CEGUI::WindowManager::getSingleton().getWindow("Game/Player/Salud")->setText(
                "[colour='FFFF0000']"
                + Ogre::StringConverter::toString(
                _playerOne->getVida()));
    }

    CEGUI::WindowManager::getSingleton().getWindow("Game/Player/TextoNivel")->setVisible(
            _inPause);

    if (_playerOne->getWeapon()->isInReload()) {
        CEGUI::WindowManager::getSingleton().getWindow("Game/Player/Recargando")->setVisible(
                true);
    } else {
        CEGUI::WindowManager::getSingleton().getWindow("Game/Player/Cargador")->setText(
                "[colour='FF00FFFD']"
                + Ogre::StringConverter::toString(
                _playerOne->getWeapon()->getMunitionMagazine()));
        CEGUI::WindowManager::getSingleton().getWindow("Game/Player/Municion")->setText(
                "[colour='FF00FFFD']"
                + Ogre::StringConverter::toString(
                _playerOne->getWeapon()->getMunition()));
        CEGUI::WindowManager::getSingleton().getWindow("Game/Player/Recargando")->setVisible(
                false);
    }

    return true;
}

bool PlayState::frameEnded(const Ogre::FrameEvent& evt) {

    if (_exitGame)
        return false;

    if (_itemPicked) {
        removeItem();
        _itemPicked = false;
    }

    if (_playerOne->getVida() <= 0 || _gameWin) {

        calculateScore();
        changeState(FinishState::getSingletonPtr());
    }

    return true;
}

void PlayState::keyPressed(const OIS::KeyEvent &e) {
    if (e.key == OIS::KC_P) {
        pushState(PauseState::getSingletonPtr());
    } else if (e.key == _mapKeys[A_UP]) {
        _playerOne->adelante(true);
    } else if (e.key == _mapKeys[A_DOWN]) {
        _playerOne->atras(true);
    } else if (e.key == _mapKeys[A_LEFT]) {
        _playerOne->izquierda(true);
    } else if (e.key == _mapKeys[A_RIGHT]) {
        _playerOne->derecha(true);
    } else if (e.key == _mapKeys[A_TURN_LEFT]) {
        _playerOne->giroIzquierda(true);
    } else if (e.key == _mapKeys[A_TURN_RIGHT]) {
        _playerOne->giroDerecha(true);
    } else if (e.key == _mapKeys[A_RELOAD]) {
        _playerOne->getWeapon()->reloadAmmo();
    }
}

void PlayState::keyReleased(const OIS::KeyEvent &e) {
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    } else if (e.key == _mapKeys[A_UP]) {
        _playerOne->adelante(false);
    } else if (e.key == _mapKeys[A_DOWN]) {
        _playerOne->atras(false);
    } else if (e.key == _mapKeys[A_LEFT]) {
        _playerOne->izquierda(false);
    } else if (e.key == _mapKeys[A_RIGHT]) {
        _playerOne->derecha(false);
    } else if (e.key == _mapKeys[A_TURN_LEFT]) {
        _playerOne->giroIzquierda(false);
    } else if (e.key == _mapKeys[A_TURN_RIGHT]) {
        _playerOne->giroDerecha(false);
    }
}

CEGUI::MouseButton PlayState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void PlayState::mouseMoved(const OIS::MouseEvent &e) {
    _rotx = e.state.X.rel * -1;
    _roty = e.state.Y.rel * 1;
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

void PlayState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
    if (_mapMouse.find(id) != _mapMouse.end()) {
        switch (_mapMouse[id]) {
            case A_SHOOT:
                _playerOne->setShooting(true);
                break;
            default:
                break;
        }
    }
}

void PlayState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
    if (_mapMouse.find(id) != _mapMouse.end()) {
        switch (_mapMouse[id]) {
            case A_SHOOT:
                _playerOne->setShooting(false);
                break;
            default:
                break;
        }
    }
}

PlayState* PlayState::getSingletonPtr() {
    return msSingleton;
}

PlayState& PlayState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void PlayState::createScene() {

    _worldCreator = WorldCreator();

    _intSceneQueryPlIt = _sceneMgr->createIntersectionQuery(
            PLAYER_AMIGO | ITEM);

}

void PlayState::initializeVars() {
    _rotx = 0.0;
    _roty = 0.0;

    _cronoFX = new Crono();
    _cronoTXT = new Crono();
    _cronoPAUSE = new Crono();
    _cronoGAME = new Crono();

    _inPause = false;
    _exitGame = false;
    _keyFound = false;
    _itemPicked = false;
    _blood = false;
    _text = false;
    _gameWin = false;
    _enemiesDeads = 0;
    _perHealt = 25;

    _lastLevel = 3;

    _mapMouse[OIS::MB_Left] = A_SHOOT;
}

void PlayState::setLevel(unsigned int level, unsigned int continues) {
    _continues = continues;
    _level = level;

    _levelText = "NIVEL " + Ogre::StringConverter::toString(level);
    _levelMap = "level" + Ogre::StringConverter::toString(level) + ".map";
}

void PlayState::setScore(unsigned int score) {
    _score = score;
}

void PlayState::newGame() {

    _worldCreator.createNewWorld(_levelMap);
    //Para ver los mapas
    //    _worldCreator.printMap();
    //    _worldCreator.printMapEsp();
    int worldHeight = _worldCreator.getWordlHeight();
    int worldWidth = _worldCreator.getWorldWidth();
    int** map = _worldCreator.getMatrixEsp();

    _pathFinder = make_shared<PathFinder>(worldWidth, worldHeight, map);

    _enemyFactory = make_shared<EnemyFactory>(_worldCreator.getEnemiesPosition());
    _itemFactory = ItemFactory();
    _weaponFactory = WeaponFactory();

    _cronoFX->start();
    _cronoTXT->start();

    Ogre::Vector3 playerPos = _worldCreator.getPosStartPlayer();
    playerPos += Vector3(0, 1, 0);
    _cameraNode->setPosition(playerPos);

    // Creamos el jugador
    _playerOne = make_shared<Player>("Player1", "Player.mesh", PLAYER_AMIGO, playerPos,
            "");
    // Su arma
    _playerOne->addWeapon(
            _weaponFactory.makeWeaponShotgun(_playerOne->getNodeWeapon()));
    // Creamos los enemigos
    _enemyFactory->initialCreation(_enemies);

    // Creamos el jefe
    _enemies.push_back(
            _enemyFactory->makeEnemyBoss(PLAYER_ENEMIGO,
            _worldCreator.getPosBoss()));

    _nodeCuerpo = _playerOne->getNode();

    //Temporal
    //        _playerOne->setVida(-90);


    _cronoPAUSE->start();
    _inPause = true;
    CEGUI::WindowManager::getSingleton().getWindow("Game/Player/TextoNivel")->setText(_levelText);
}

void PlayState::showText(string text) {
    _text = true;
    CEGUI::Window* wText = CEGUI::WindowManager::getSingleton().getWindow(
            "Game/Player/Mensajes");
    wText->setText(text);
    wText->setVisible(true);
    _cronoTXT->reset();

}

void PlayState::createGUI() {
    _CEGUIRenderer = IntroState::getSingleton().getCEGUIRenderer();
    CEGUI::MouseCursor::getSingleton().hide();
    //Sheet
    CEGUI::Window *sheet =
            CEGUI::WindowManager::getSingleton().loadWindowLayout(
            "Game.layout");
    CEGUI::WindowManager::getSingleton().getWindow("Game/Sangre")->setVisible(
            false);

    CEGUI::WindowManager::getSingleton().getWindow("Game/Player/Salud")->setText(
            "[colour='FF00FFFD']100");

    CEGUI::WindowManager::getSingleton().getWindow("Game/Player/Cargador")->setText(
            "[colour='FF00FFFD']10");
    CEGUI::WindowManager::getSingleton().getWindow("Game/Player/Municion")->setText(
            "[colour='FF00FFFD']100");
    CEGUI::WindowManager::getSingleton().getWindow("Game/Player/Recargando")->setVisible(
            false);
    CEGUI::WindowManager::getSingleton().getWindow("Game/Player/Mensajes")->setVisible(
            false);

    //Render Texture - to - target
    Ogre::TexturePtr tex = _root->getTextureManager()->getByName("RTT",
            Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    if (tex.isNull()) {
        tex = _root->getTextureManager()->createManual("RTT",
                Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                Ogre::TEX_TYPE_2D, 512, 512, 0, Ogre::PF_R8G8B8,
                Ogre::TU_RENDERTARGET);

        Ogre::RenderTexture* rtex = tex->getBuffer()->getRenderTarget();

        if (!rtex->hasViewportWithZOrder(0)) {
            Ogre::Viewport* v = rtex->addViewport(_cameraMap);
            v->setOverlaysEnabled(false);
            v->setClearEveryFrame(true);
            //Para que se vea el protagonista pero no la luz
            v->setVisibilityMask(PLAYER_AMIGO | ~LIGHT);
        }
        CEGUI::Texture& guiTex = _CEGUIRenderer->createTexture(tex);

        CEGUI::Imageset& imageSet =
                CEGUI::ImagesetManager::getSingleton().create("RTTImageset",
                guiTex);
        imageSet.defineImage("RTTImage", CEGUI::Point(0.0f, 0.0f),
                //            CEGUI::Size(guiTex.getSize().d_width, guiTex.getSize().d_height),
                CEGUI::Size(512, 512), CEGUI::Point(0.0f, 0.0f));
        CEGUI::Window* RTTWindow =
                CEGUI::WindowManager::getSingleton().getWindow(
                "Game/Player/Map");

        RTTWindow->setProperty("Image",
                CEGUI::PropertyHelper::imageToString(
                &imageSet.getImage("RTTImage")));
    }
    CEGUI::System::getSingleton().setGUISheet(sheet);
}

void PlayState::moverCamara(Ogre::Vector3 mov, Ogre::Radian rotX,
        Ogre::Radian rotY, Ogre::Radian rY) {

    Ogre::Real pitchAngle;
    Ogre::Real pitchAngleSign;
    Ogre::Real yawAngle;
    Ogre::Real yawAngleSign;

    _cameraYawNode->yaw(rotX);
    _cameraPitchNode->pitch(-rotY);

    _cameraNode->translate(mov, Ogre::SceneNode::TS_LOCAL);
    _nodeCuerpo->setPosition(_cameraNode->getPosition());

    _cameraNode->yaw(_cameraYawNode->getOrientation().getYaw());
    _nodeCuerpo->yaw(_cameraYawNode->getOrientation().getYaw());
    _cameraYawNode->setOrientation(Ogre::Quaternion::IDENTITY);

    pitchAngle =
            (2
            * Ogre::Degree(
            Ogre::Math::ACos(
            _cameraPitchNode->getOrientation().w)).valueDegrees());
    pitchAngleSign = _cameraPitchNode->getOrientation().x;

    if (pitchAngle > 90.0f) {
        if (pitchAngleSign > 0)

            _cameraPitchNode->setOrientation(
                Ogre::Quaternion(Ogre::Math::Sqrt(0.5f),
                Ogre::Math::Sqrt(0.5f), 0, 0));
        else if (pitchAngleSign < 0)
            _cameraPitchNode->setOrientation(
                Ogre::Quaternion(Ogre::Math::Sqrt(0.5f),
                -Ogre::Math::Sqrt(0.5f), 0, 0));
    }
}

bool PlayState::isCollision(const Ogre::Vector3& position,
        const Ogre::Vector3& direction, Ogre::uint32 mask, Ogre::Real distance) {

    Ogre::Ray ray(position, direction);
    _raySceneQueryMov->setRay(ray);
    _raySceneQueryMov->setSortByDistance(true);
    _raySceneQueryMov->setQueryMask(mask);
    Ogre::RaySceneQueryResult &result = _raySceneQueryMov->execute();
    Ogre::RaySceneQueryResult::iterator it;
    it = result.begin();
    if (it != result.end()) {
        if (it->distance <= distance)
            return true;
    }
    return false;
}

void PlayState::removeEnemy() {
    EnemyPtr deathEnemy;
    for (EnemyPtr enemy : _enemies) {
        if (enemy->isDead()) {
            deathEnemy = enemy;
        }
    }

    _enemies.erase(remove(_enemies.begin(), _enemies.end(), deathEnemy),
            _enemies.end());
}

void PlayState::removeItem() {
    ItemPtr emptyItem;
    for (ItemPtr item : _items) {
        if (item->isEmpty())
            emptyItem = item;
    }

    _items.erase(remove(_items.begin(), _items.end(), emptyItem), _items.end());
}

void PlayState::pickItem(int type, unsigned int value) {

    _itemPicked = true;
    switch (type) {
        case I_HEALT:
            GameManager::getSingletonPtr()->playSoundPicked();
            _playerOne->setVida(value);
            break;
        case I_MUNITION:
            GameManager::getSingletonPtr()->playSoundPicked();
            _playerOne->setMunition(value);
            break;
        case I_KEY:
            GameManager::getSingletonPtr()->playSoundKey();
            _keyFound = true;
            _sceneMgr->getEntity("Gateway")->setQueryFlags(GATEWAY);
            showText("Llave encontrada");
            break;
    }
}

unsigned int PlayState::calculateItem() {
    unsigned int type;
    unsigned int porcentaje;
    unsigned int sumVida = (int) ((150 - _playerOne->getVida()) / 5);

    sumVida += _perHealt;
    porcentaje = (std::rand() % 100);
    if (porcentaje <= sumVida) {
        type = I_HEALT;
    } else if (porcentaje > sumVida && porcentaje < 80) {
        type = I_MUNITION;
    } else {
        type = I_EMPTY;
    }
    return type;
}

void PlayState::dropItem(int type, Ogre::Vector3 position) {
    if (type == I_EMPTY) {
        type = calculateItem();
    }
    Ogre::Vector3 createPosition = Ogre::Vector3(position.x, 1.5, position.z);
    switch (type) {
        case I_HEALT:
            _items.push_back(_itemFactory.makeItemHealt(createPosition));
            break;
        case I_MUNITION:
            _items.push_back(_itemFactory.makeItemMunition(createPosition));
            break;
        case I_KEY:
            _items.push_back(_itemFactory.makeItemKey(createPosition + Vector3(0, 0.6, 0)));
            break;
    }
}

void PlayState::enterInGateway() {
    if (_keyFound)
        _gameWin = true;
    else
        showText("PUERTA CERRADA, BUSCA LA LLAVE");
}

void PlayState::hitByEnemy(unsigned int damage) {
    _blood = true;
    GameManager::getSingletonPtr()->playSoundHitPlayer();
    CEGUI::WindowManager::getSingleton().getWindow("Game/Sangre")->setVisible(
            true);
    _cronoFX->reset();

    _playerOne->setVida(-damage);

}

void PlayState::calculateScore() {
    int tmp;
    int tiempoPasado = (_cronoGAME->getTime() / 1000);
    int scoreTMP;

    if (_score > 0)
        scoreTMP = _score;
    else
        scoreTMP = PUNTUACION_BASE;

    scoreTMP += _playerOne->getVida()*10;
    scoreTMP += _enemiesDeads * 100;

    //    cout << "tiempo: " << tiempoPasado << " scoreTMP:" << scoreTMP << endl;

    if (tiempoPasado < TIEMPO_MAXIMO) {
        tmp = (TIEMPO_MAXIMO - tiempoPasado)*2;
    } else {
        tmp = 600;
    }

    //    cout << "tmp: " << tmp << " scoreTMP:" << scoreTMP << endl;

    if (_gameWin) {
        scoreTMP += tmp;
    } else {
        if (scoreTMP >= PUNTUACION_BASE)
            scoreTMP -= tmp;
    }

    _score = scoreTMP;

}

void PlayState::setName(std::string name) {
    _name = name;
}

void PlayState::anEnemyWasKilled(std::string id) {
    _enemiesDeads++;
    cout << "Enemigos muertos: " << _enemiesDeads << endl;

    //Cuando un enemigo muere se comprueba si quedan por salir más enemigos de su zona
    auto it = find_if(_enemies.begin(), _enemies.end(), [id](EnemyPtr enemy) {
        return enemy->getId() == id;
    });

    if (it != _enemies.end()) {
        _enemyDead = *it;
    }
}
