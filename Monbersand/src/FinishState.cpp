/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "FinishState.h"
#include "PlayState.h"
#include "IntroState.h"

template<> FinishState* Ogre::Singleton<FinishState>::msSingleton = 0;

void FinishState::enter() {
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->createCamera("FinishCamera");

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _exitGame = false;
    _scoreSaved = false;
    createGUI();
}

void
FinishState::exit() {
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    _sceneMgr->destroyCamera("FinishCamera");
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void FinishState::pause() {
}

void FinishState::resume() {
}

bool FinishState::frameStarted(const Ogre::FrameEvent& evt) {
    return true;
}

bool FinishState::frameEnded(const Ogre::FrameEvent& evt) {
    if (_exitGame)
        return false;

    return true;
}

void FinishState::keyPressed(const OIS::KeyEvent &e) {
}

void FinishState::keyReleased(const OIS::KeyEvent &e) {
}

void
FinishState::mouseMoved(const OIS::MouseEvent &e) {
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton FinishState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void FinishState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void FinishState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

FinishState* FinishState::getSingletonPtr() {
    return msSingleton;
}

FinishState& FinishState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void FinishState::createGUI() {

    //Mouse
    CEGUI::MouseCursor::getSingleton().setVisible(true);
    CEGUI::System::getSingleton().setDefaultFont("Scarface-36");
    CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout(
            "Finish.layout");
    CEGUI::Window *textTitle = CEGUI::WindowManager::getSingleton().getWindow("Finish/Title");
    CEGUI::Window *textScore = CEGUI::WindowManager::getSingleton().getWindow("Finish/Score");
    CEGUI::Window *textContinues = CEGUI::WindowManager::getSingleton().getWindow("Finish/Continues");
    CEGUI::Window *botonNewGame = CEGUI::WindowManager::getSingleton().getWindow("Finish/NewGameButton");
    CEGUI::Window *botonNextLevel = CEGUI::WindowManager::getSingleton().getWindow("Finish/NextLevelButton");
    CEGUI::Window *botonScores = CEGUI::WindowManager::getSingleton().getWindow("Finish/ScoresButton");
    CEGUI::Window *botonPortada = CEGUI::WindowManager::getSingleton().getWindow("Finish/IntroButton");
    CEGUI::Window *botonExit = CEGUI::WindowManager::getSingleton().getWindow("Finish/ExitButton");
    
    botonPortada->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&FinishState::Portada, this));
        botonScores->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&FinishState::Puntuaciones, this));
        botonExit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&FinishState::Salir, this));

    textScore->setText("TU PUNTUACION ES: " + Ogre::StringConverter::toString(PlayState::getSingletonPtr()->getScore()));

    textContinues->setText("Continues " + Ogre::StringConverter::toString(PlayState::getSingletonPtr()->getContinues()));
    botonNewGame->setVisible(false);
    
    if (!PlayState::getSingletonPtr()->isGameWin()) {
        textTitle->setText("TU VIAJE A TERMINADO");
        botonNextLevel->setVisible(false);
        if (PlayState::getSingletonPtr()->getContinues() > 0) {
            botonNewGame->setText("INTENTARLO DE NUEVO");
            botonNewGame->setVisible(true);
            botonNewGame->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&FinishState::RepeatLevel, this));
        } else {
            textContinues->setText("No continues ");
        }
    } else {
        if (Ogre::StringConverter::toString(PlayState::getSingletonPtr()->getLevel()) < Ogre::StringConverter::toString(PlayState::getSingletonPtr()->getLastLevel())) {
            textTitle->setText("¡ NIVEL "+Ogre::StringConverter::toString(PlayState::getSingletonPtr()->getLevel())+" SUPERADO !");
            botonNextLevel->setText("IR AL NIVEL " + Ogre::StringConverter::toString(PlayState::getSingletonPtr()->getLevel()+1));
            botonNextLevel->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&FinishState::NextLevel, this));
        }else{
            textTitle->setText("¡ ENHORABUENA, JUEGO FINALIZADO !");
        }
    }
    CEGUI::System::getSingleton().setGUISheet(sheet);
}

bool FinishState::RepeatLevel(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    PlayState::getSingletonPtr()->setScore(PlayState::getSingletonPtr()->getScore() / 2);
    PlayState::getSingletonPtr()->setLevel(PlayState::getSingletonPtr()->getLevel(), PlayState::getSingletonPtr()->getContinues() - 1);
    changeState(PlayState::getSingletonPtr());
    return true;
}

bool FinishState::NextLevel(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    PlayState::getSingletonPtr()->setLevel(PlayState::getSingletonPtr()->getLevel() + 1, PlayState::getSingletonPtr()->getContinues());
    changeState(PlayState::getSingletonPtr());
    return true;
}

bool FinishState::Portada(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    saveScore();
    changeState(IntroState::getSingletonPtr());
    return true;
}
bool FinishState::Puntuaciones(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    saveScore();
    pushState(ScoresState::getSingletonPtr());
    return true;
}

bool FinishState::Salir(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    saveScore();
    _exitGame = true;
    return true;
}
void FinishState::saveScore(){
    if(!_scoreSaved){
        ScoresData::getInstance().addScore(PlayState::getSingletonPtr()->getName(),PlayState::getSingletonPtr()->getLevel(), PlayState::getSingletonPtr()->getScore());
        _scoreSaved = true;
    }
}