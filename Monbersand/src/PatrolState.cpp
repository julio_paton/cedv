/*
 * PatrolState.cpp
 *
 *  Created on: Jun 9, 2014
 *      Author: julio
 */

#include "PatrolState.h"
#include "ChaseState.h"
#include "Enemy.h"
#include "PlayState.h"
#include <iostream>

using namespace Ogre;
using namespace std;

shared_ptr<PatrolState> PatrolState::GetInstance() {
	static std::shared_ptr<PatrolState> instance =
			std::make_shared<PatrolState>();
	return instance;
}

void PatrolState::handleState(Enemy * enemyPtr, PlayState *playStatePtr) {

	if (enemyPtr->getType() == E_BOSS)
	{
		//Comprobar la distancia al jugador para cambiar de estado
		Real distance = enemyPtr->getNode()->getPosition().distance(
				playStatePtr->getPlayerPtr()->getNode()->getPosition());

		//Si el enemigo está dentro de su campo de visión empieza a perseguirlo
		if (distance < enemyPtr->_bossVisionDistance || enemyPtr->_hasBeenHit){
			enemyPtr->changeFace("alert");
			enemyPtr->changeState(ChaseState::GetInstance());
			//Reset variables del estado
			enemyPtr->_hasBeenHit = false;
		}
	}
	else
	{
		enemyPtr->changeState(ChaseState::GetInstance());
	}
}
