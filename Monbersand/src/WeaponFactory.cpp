/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "WeaponFactory.h"

using namespace Ogre;

WeaponPtr WeaponFactory::makeWeaponShotgun(Ogre::SceneNode* nodeCharacter) {
    WeaponPtr lWeapon = make_shared<Weapon>(nodeCharacter,
            "Weapon" + Ogre::StringConverter::toString(++_nextId), "ShotgunV2.mesh",
            Ogre::Vector3(0.5, 0.5, -1.3), Ogre::Vector3(0.3, 0.3, 0.15)
            , 30.0, 1.5f, 3.0f, 4, 20, 80);
    return lWeapon;
}
