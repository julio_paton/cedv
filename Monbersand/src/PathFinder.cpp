/*
 * PathFinder.cpp
 *
 *  Created on: Jun 25, 2014
 *      Author: julio
 */

#include "PathFinder.h"
#include "Defines.h"
#include <cmath>

PathFinder::PathFinder(int mapWidth, int mapHeight, int **map) {
	_map = map;
	_mapWidth = mapWidth;
	_mapHeight = mapHeight;
	_startX = 0;
	_startY = 0;

	_pather = std::make_shared<MicroPather>(this, _mapWidth * _mapHeight * COEF / 2, 8);
}

PathFinder::~PathFinder() {
}

void PathFinder::clearLastSearch() {
	_path.resize(0);
	_enemiesPos.resize(0);
	_pather->Reset();
}

int PathFinder::passable(int nx, int ny) {
	if (nx >= 0 && nx < _mapWidth && ny >= 0 && ny < _mapHeight) {
		int value = _map[ny][nx];
		if (value != T_WALL)
		{
			//Se evita que se acerquen a las paredes
			if ((nx - 1 >= 0 && _map[ny][nx - 1] == T_WALL)
					|| (nx + 1 < _mapWidth && _map[ny][nx + 1] == T_WALL)
					|| (nx - 2 >= 0 && _map[ny][nx - 2] == T_WALL)
					|| (nx + 2 < _mapWidth && _map[ny][nx + 2] == T_WALL))
			{
				return 0;
			}
			if ((ny - 1 >= 0 && _map[ny - 1][nx] == T_WALL)
					|| (ny + 1 < _mapWidth && _map[ny + 1][nx] == T_WALL)
					|| (ny - 2 >= 0 && _map[ny - 2][nx] == T_WALL)
					|| (ny + 2 < _mapWidth && _map[ny + 2][nx] == T_WALL))
			{
				return 0;
			}

			//Se evita que se acerquen al resto de enemigos
			if (isAnEnemyClose(nx, ny))
			{
				return 0;
			}

			return 1;
		}
	}
	return 0;
}

void PathFinder::setStartPos(int i, int j) {
	ogrePosToMap(i,j, _startX, _startY);
}

int PathFinder::setDestinyPos(int i, int j) {
	int nx =0, ny = 0;
	int result = 0;
	ogrePosToMap(i,j, nx, ny);
	float totalCost;

	if (passable(nx, ny) == 1)
	{
		result = _pather->Solve(XYToNode(_startX, _startY), XYToNode(nx, ny),
				&_path, &totalCost);
	}
	else
	{
		//Si la posición del jugador no es alcanzable por el enemigo, se busca una posición cercana que lo sea
		findNewDestiny( nx, ny );
		if (passable(ny, nx) == 1)
		{
			result = _pather->Solve(XYToNode(_startX, _startY), XYToNode(nx, ny),
					&_path, &totalCost);
		}
	}
	return result;
}

void PathFinder::nodeToXY(void* node, int* x, int* y) {
	//Se recuperan los valores de las coordenadas a partir del id único
	uintptr_t index = (uintptr_t) node;
	*y = index / _mapWidth;
	*x = index - *y * _mapWidth;
}

void* PathFinder::XYToNode(int x, int y) {
	//Se crea un identificador único para estas coordenadas
	return (void*) (long) (y * _mapWidth + x);
}

float PathFinder::LeastCostEstimate(void* nodeStart, void* nodeEnd) {
	int xStart, yStart, xEnd, yEnd;
	nodeToXY(nodeStart, &xStart, &yStart);
	nodeToXY(nodeEnd, &xEnd, &yEnd);

	/* Compute the minimum path cost using distance measurement. It is possible
	 to compute the exact minimum path using the fact that you can move only
	 on a straight line or on a diagonal, and this will yield a better result.
	 */
	int dx = xStart - xEnd;
	int dy = yStart - yEnd;
	return (float) sqrt((double) (dx * dx) + (double) (dy * dy));
}

void PathFinder::AdjacentCost(void* node, std::vector<StateCost>* neighbours) {
	int x, y;
	//Movimientos empezando por la derecha y en el sentido del reloj
	const int dx[8] = { 1, 1, 0, -1, -1, -1, 0, 1 };
	const int dy[8] = { 0, 1, 1, 1, 0, -1, -1, -1 };
	const float cost[8] = { 1.0f, 1.41f, 1.0f, 1.41f, 1.0f, 1.41f, 1.0f, 1.41f };

	nodeToXY(node, &x, &y);

	for (int i = 0; i < 8; ++i) {
		int nx = x + dx[i];
		int ny = y + dy[i];

		int pass = passable(nx, ny);
		if (pass == 1) {
			//Empty space
			StateCost nodeCost = { XYToNode(nx, ny), cost[i] };
			neighbours->push_back(nodeCost);
		}
	}
}

void PathFinder::PrintStateInfo(void* node) {
	int x, y;
	nodeToXY(node, &x, &y);
	printf("(%d,%d)", x, y);
}

void PathFinder::getComputedPath(std::vector<Ogre::Vector3>& computedPath) {
	computedPath.reserve(_path.size());

	for (unsigned int i = 0; i < _path.size(); i++) {
		int x, y;
		nodeToXY(_path[i], &x, &y);
		int oX,oY;
		mapPosToOgre(x,y,oX, oY);
		Ogre::Vector3 v(oX, 0, oY);
//		std::cout << "Camino: " << v << std::endl;
		computedPath.push_back(v);
	}
}

void PathFinder::ogrePosToMap(int i, int j, int& x, int& y) {
	//Hay que hacer la correspondencia entre la matriz y las coordenadas de Ogre
	x = _mapWidth / 2 - j;
	y = _mapHeight / 2 + i;

}

void PathFinder::mapPosToOgre(int x, int y, int& i, int& j) {
	//Hay que hacer la correspondencia entre la matriz y las coordenadas de Ogre
	i = -_mapWidth / 2 + y;
	j = +_mapHeight / 2 - x;

}

void PathFinder::setEnemiesPos(const std::vector<EnemyPtr>& enemies)
{
	for (EnemyPtr enemy : enemies)
	{
		int x, y, nextX, nextY;
		ogrePosToMap(lround(enemy->getNode()->getPosition().x),
				std::lround(enemy->getNode()->getPosition().z), x, y);
		ogrePosToMap(std::lround(enemy->_chaseDestination.x),
						std::lround(enemy->_chaseDestination.z), nextX, nextY);
		if ( !enemy->isDead())//Si no está muerto
		{
			_enemiesPos.push_back(Ogre::Vector2(y, x));
			_enemiesPos.push_back(Ogre::Vector2(nextY,nextX));
		}
	}
}

void PathFinder::findNewDestiny(int& nx, int& ny)
{
	//Pared a la izquierda
	int value = _map[ny-2][nx];
	if (value == T_WALL)
		ny += 2;
	//Pared a la derecha
	value = _map[ny+2][nx];
	if (value == T_WALL)
		ny -= 2;
	//Pared arriba
	value = _map[ny][nx-2];
	if (value == T_WALL)
		nx += 2;
	//Pared abajo
	value = _map[ny][nx+2];
	if (value == T_WALL)
		nx -= 2;

	cout << "new destiny pos: x->" << nx << " y:" << ny << endl;
}

bool PathFinder::isAnEnemyClose(int nx, int ny)
{
	for (Ogre::Vector2 enemyPos : _enemiesPos)
	{
		for (int i = -2; i <=2; ++i)
		{
			for (int j=-2; i<=2; ++i)
			{
				if ( ny+i == enemyPos.x && nx+j == enemyPos.y)
				{
					return true;
				}
			}
		}
	}
	return false;
}
