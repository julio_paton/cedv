/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/


#include "IntroState.h"
#include "ConfigState.h"
#include "HelpState.h"

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

void IntroState::enter() {
    GameManager::getSingletonPtr()->playTrackMenu();
    _root = Ogre::Root::getSingletonPtr();

    if (_root->hasSceneManager("SceneManager")) {
        _sceneMgr = _root->getSceneManager("SceneManager");
    } else {
        _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
        _CEGUIRenderer = &CEGUI::OgreRenderer::bootstrapSystem();
    }

    if (_sceneMgr->hasCamera("MenusCamera")) {
        _camera = _sceneMgr->getCamera("MenusCamera");
    } else {
        _camera = _sceneMgr->createCamera("MenusCamera");
    }

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));
    loadResources();

    //CEGUI
    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    CEGUI::Imageset::setDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

    CEGUI::SchemeManager::getSingleton().create("TaharezLook.scheme");
    CEGUI::SchemeManager::getSingleton().create("Monbersand.scheme");
    CEGUI::System::getSingleton().setDefaultFont("Scarface-36");
    CEGUI::System::getSingleton().setDefaultMouseCursor("TaharezLook", "MouseArrow");


    _frase = 0;
    _alpha = 0.0;
    _exitGame = false;
    _crono = new Crono();
    _crono->start();
    
    createGUI();


}

void IntroState::exit() {
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void IntroState::pause() {
}

void IntroState::resume() {
}

bool IntroState::frameStarted(const Ogre::FrameEvent& evt) {
    CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);

    if (_frase > 0 && _frase <= 6) {
        _alpha = _alpha + (1 * evt.timeSinceLastFrame);
        CEGUI::WindowManager::getSingleton().getWindow("Intro/P" + Ogre::StringConverter::toString(_frase))->setProperty("Alpha", Ogre::StringConverter::toString(_alpha));
        if (_crono->isOver(0.6)) {
            CEGUI::WindowManager::getSingleton().getWindow("Intro/P" + Ogre::StringConverter::toString(_frase))->setProperty("Alpha", "100");
            _alpha = 0;
            _frase++;
            _crono->reset();
        }
    } else if (_frase == 7) {
        CEGUI::WindowManager::getSingleton().getWindow("Intro/NickEditBox")->setVisible(true);

        //Start button
        CEGUI::WindowManager::getSingleton().getWindow("Intro/StartButton")->setVisible(true);
    }
    return true;
}

bool IntroState::frameEnded(const Ogre::FrameEvent& evt) {
    if (_exitGame)
        return false;

    return true;
}

void IntroState::keyPressed(const OIS::KeyEvent &e) {
    CEGUI::System::getSingleton().injectKeyDown(e.key);
    CEGUI::System::getSingleton().injectChar(e.text);
    // Espacio --> PlayState
    if (e.key == OIS::KC_SPACE && _frase > 0) {
        for (unsigned int i = 1; i <= 6; i++) {
            CEGUI::WindowManager::getSingleton().getWindow("Intro/P" + Ogre::StringConverter::toString(i))->setProperty("Alpha", "100");
        }
        _frase = 7;
    }
}

void IntroState::keyReleased(const OIS::KeyEvent &e) {
    CEGUI::System::getSingleton().injectKeyUp(e.key);
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
}

void IntroState::mouseMoved(const OIS::MouseEvent &e) {
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton IntroState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void IntroState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void IntroState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

IntroState* IntroState::getSingletonPtr() {
    return msSingleton;
}

IntroState& IntroState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void IntroState::createGUI() {

    CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout("Portada.layout");

    //Start button
    CEGUI::WindowManager::getSingleton().getWindow("Portada/PlayButton")->setFont("Scarface-48");

    CEGUI::WindowManager::getSingleton().getWindow("Portada/PlayButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::StartGame, this));
    //Start button
    CEGUI::WindowManager::getSingleton().getWindow("Portada/OtionsButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::Opciones, this));
    //Help button
    CEGUI::WindowManager::getSingleton().getWindow("Portada/HelpButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::Ayuda, this));
    //Credits button
    CEGUI::WindowManager::getSingleton().getWindow("Portada/CreditsButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::Creditos, this));
    //Scores button
    CEGUI::WindowManager::getSingleton().getWindow("Portada/ScoresButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::Puntuaciones, this));
    //Exit button
    CEGUI::WindowManager::getSingleton().getWindow("Portada/ExitButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::Salir, this));

    CEGUI::System::getSingleton().setGUISheet(sheet);
}

bool IntroState::StartGame(const CEGUI::EventArgs &e) {
    
    if (ConfigState::getSingletonPtr()->getFoundConf()) {
        Empezar(e);
    } else {
        CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout("Intro.layout");
        CEGUI::WindowManager::getSingleton().getWindow("Intro/NickEditBox")->setText(ConfigState::getSingletonPtr()->getPlayerName());

        for (unsigned int i = 1; i <= 5; i++) {
            CEGUI::WindowManager::getSingleton().getWindow("Intro/P" + Ogre::StringConverter::toString(i))->setProperty("TextColours", "FF800000");
            CEGUI::WindowManager::getSingleton().getWindow("Intro/P" + Ogre::StringConverter::toString(i))->setProperty("Alpha", "0.0");
        }
        CEGUI::WindowManager::getSingleton().getWindow("Intro/P6")->setProperty("Alpha", "0.0");

        CEGUI::WindowManager::getSingleton().getWindow("Intro/NickEditBox")->setVisible(false);

        //Start button
        CEGUI::WindowManager::getSingleton().getWindow("Intro/StartButton")->setVisible(false);
        CEGUI::WindowManager::getSingleton().getWindow("Intro/StartButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::Empezar, this));

        CEGUI::System::getSingleton().setGUISheet(sheet);

        _crono->reset();
        _frase = 1;
    }
    return true;
}

bool IntroState::Salir(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    _exitGame = true;
    return true;
}

bool IntroState::Empezar(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    std::string name;
    
    PlayState::getSingletonPtr()->setLevel(1, 3);
    
    if (ConfigState::getSingletonPtr()->getFoundConf()) {
        name = ConfigState::getSingletonPtr()->getPlayerName();
    }else{
        name = (CEGUI::WindowManager::getSingleton().getWindow("Intro/NickEditBox")->getText()).c_str();
        ConfigState::getSingletonPtr()->saveName(name);
    }
    
    PlayState::getSingletonPtr()->setName(name);
    changeState(PlayState::getSingletonPtr());
    return true;
}

bool IntroState::Opciones(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    pushState(ConfigState::getSingletonPtr());
    return true;
}

bool IntroState::Puntuaciones(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    pushState(ScoresState::getSingletonPtr());
    return true;
}

bool IntroState::Ayuda(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    pushState(HelpState::getSingletonPtr());
    return true;
}
bool IntroState::Creditos(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    pushState(CreditsState::getSingletonPtr());
    return true;
}

void IntroState::loadResources() {
    Ogre::ConfigFile cf;
    cf.load("resources.cfg");

    Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
    Ogre::String sectionstr, typestr, datastr;
    while (sI.hasMoreElements()) {
        sectionstr = sI.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i) {
            typestr = i->first;
            datastr = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation
                    (datastr, typestr, sectionstr);
        }
    }
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}
