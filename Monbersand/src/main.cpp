/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#define UNUSED_VARIABLE(x) (void)x

#include "GameManager.h"
#include "IntroState.h"
#include "PlayState.h"
#include "PauseState.h"
#include "FinishState.h"
#include "ConfigState.h"
#include "ScoresState.h"
#include "CreditsState.h"
#include "HelpState.h"

#include <iostream>

using namespace std;
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
int main(int argc, char *argv[])
#endif
{

    GameManager* game = new GameManager();
    IntroState* introState = new IntroState();
    PlayState* playState = new PlayState();
    PauseState* pauseState = new PauseState();
    FinishState* finishState = new FinishState();
    ConfigState* configState = new ConfigState();
    ScoresState* scoresState = new ScoresState();
    CreditsState* creditsState = new CreditsState();
    HelpState* helpState = new HelpState();

    UNUSED_VARIABLE(introState);
    UNUSED_VARIABLE(playState);
    UNUSED_VARIABLE(pauseState);
    UNUSED_VARIABLE(finishState);
    UNUSED_VARIABLE(configState);
    UNUSED_VARIABLE(scoresState);
    UNUSED_VARIABLE(creditsState);
    UNUSED_VARIABLE(helpState);

    try {
        // Inicializa el juego y transición al primer estado.
        game->start(IntroState::getSingletonPtr());
    } catch (Ogre::Exception& e) {
        std::cerr << "Excepción detectada: " << e.getFullDescription();
    }

    delete game;

    return 0;
}
