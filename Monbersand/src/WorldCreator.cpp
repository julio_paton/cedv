/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#include "WorldCreator.h"

using namespace std;
using namespace Ogre;

WorldCreator::WorldCreator() {
	_matrix = 0;
	_matrixESP = 0;
	_matrixWidth = 0;
	_matrixHeight = 0;
	_scnManager = nullptr;
	_nodeGateway = nullptr;
}

WorldCreator::~WorldCreator() {

	//free matrix memory
	for (int i = 0; i < _matrixHeight; i++)
		delete [] _matrix[i];
	delete [] _matrix;

	for (int i = 0; i< _matrixHeight * COEF; i++)
		delete [] _matrixESP[i];
	delete [] _matrixESP;

}

void WorldCreator::createNewWorld(std::string mapFile) {

	readMap(mapFile);

	_scnManager = Ogre::Root::getSingletonPtr()->getSceneManager(
			"SceneManager");
	//Create floor
	Ogre::MovablePlane *pl1 = new MovablePlane("FloorPlane");
	pl1->normal = Vector3::UNIT_Y;

	Ogre::MeshManager::getSingleton().createPlane("pl1",
			Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, *pl1,
			_matrixHeight * COEF, _matrixWidth * COEF, 20, 20, true, 1, 5,
			5, Ogre::Vector3::UNIT_Z);
	//Create roof
	Ogre::Plane pl2(Ogre::Vector3::UNIT_Y, 0);
	Ogre::MeshManager::getSingleton().createPlane("pl2",
			Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, pl2,
			_matrixHeight * COEF, _matrixWidth * COEF, 20, 20, true, 1, 5,
			5, Ogre::Vector3::UNIT_Z);

	Ogre::SceneNode* node1 = _scnManager->createSceneNode("Ground");
	Ogre::Entity* grEnt = _scnManager->createEntity("pEnt", "pl1");
	grEnt->setQueryFlags(GROUND);
	grEnt->setMaterialName("Ground");
	grEnt->setCastShadows(false);
	node1->attachObject(grEnt);
        
	Ogre::SceneNode* node2 = _scnManager->createSceneNode("ROOF");
	Ogre::Entity* rEnt = _scnManager->createEntity("pEnt2", "pl2");
	rEnt->setQueryFlags(ROOF);
	rEnt->setMaterialName("Roof");
	rEnt->setCastShadows(false);
	node2->pitch(Ogre::Radian(Ogre::Degree(180)));
	node2->attachObject(rEnt);
	node2->setPosition(0,10,0);

	_scnManager->getRootSceneNode()->addChild(node1);
	_scnManager->getRootSceneNode()->addChild(node2);

	//Lights and shadows
	_scnManager->setAmbientLight(Ogre::ColourValue(0.8,0.8,0.8));
	_scnManager->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_ADDITIVE);
	//Para que todas las luces produzcan sombras hay que crear una textura para cada una de ellas
	_scnManager->setShadowTextureSettings(1024,_lights.size());

	loadLights();

	loadWalls();
}

void WorldCreator::readMap(string mapFile) {

	std::ifstream infile;
	infile.open("media/maps/" + mapFile);
	if (infile) {
		//Read the size of the matrix;
		infile >> _matrixHeight >> _matrixWidth;
		//Allocate the matrix
		_matrix = new int*[_matrixHeight];
		for (int i = 0; i < _matrixHeight; i++) {
			_matrix[i] = new int[_matrixWidth];
		}

		//Read the elements
		char element;
		for (int i = 0; i < _matrixHeight; i++) {
			for (int j = 0; j < _matrixWidth; j++) {
				infile >> element;
				switch (element) {
				case M_PLAYER:
					_matrix[i][j] = T_PLAYER;
					_posStartPlayer = Vector3(
							i * COEF - _matrixHeight / 2 * COEF + COEF/2, 0,
							-j * COEF + _matrixWidth / 2 * COEF - COEF/2);
					break;
				case M_BOSS:
					_matrix[i][j] = T_ENEMY;
					_posBoss = Vector3(
							i * COEF - _matrixHeight / 2 * COEF + COEF/2, 0,
							-j * COEF + _matrixWidth / 2 * COEF - COEF/2);
					break;
				case M_WALL:
					_matrix[i][j] = T_WALL;
					_walls.push_back(Vector3(i, 0, j));
					break;
				case M_LIGHT:
					_matrix[i][j] = T_EMPTY;
					_lights.push_back(Vector3(i,0,j));
					break;
				case M_ENEMY:
					_matrix[i][j] = T_ENEMY;
					_enemiesPosition.push_back(Vector3(i * COEF - _matrixHeight / 2 * COEF + COEF/2, 0,
							-j * COEF + _matrixWidth / 2 * COEF - COEF/2));
					break;
				case M_GATEWAY:
					_matrix[i][j] = T_WALL;
					_posGateway = Vector3(i * COEF - _matrixHeight / 2 * COEF + COEF/2, 0,
							-j * COEF + _matrixWidth / 2 * COEF - COEF/2);
					break;
				default:
					_matrix[i][j] = T_EMPTY;
					break;
				}
			}
		}
		//Se crea la matriz que se usará para la búsqueda de caminos
		_matrixESP = new int*[_matrixHeight * COEF];
		for (int i = 0; i < _matrixHeight * COEF; i++) {
			_matrixESP[i] = new int[_matrixWidth * COEF];
		}
		int espI;
		int espJ;
		for (int i = 0; i < _matrixHeight * COEF; i++) {
			for (int j = 0; j < _matrixWidth * COEF; j++) {
				if (i <= COEF) {
					espI = 0;
				} else {
					espI = (int) (i / COEF);
				}
				if (j <= COEF) {
					espJ = 0;
				} else {
					espJ = (int) (j / COEF);
				}
				_matrixESP[i][j] = _matrix[espI][espJ];
			}
		}
	} else {
		std::cout << "Could not open map file!\n";
		exit(1);
	}
}

void WorldCreator::printMap() {
	for (int i = 0; i < _matrixWidth; i++) {
		for (int j = 0; j < _matrixHeight; j++) {
			cout << _matrix[i][j];
			if (j == _matrixHeight - 1)
				cout << endl;
		}
	}
}

void WorldCreator::printMapEsp() {
	for (int i = 0; i < _matrixWidth * COEF; i++) {
		for (int j = 0; j < _matrixHeight * COEF; j++) {
			cout << _matrixESP[i][j];
			if (j == _matrixHeight * COEF - 1)
				cout << endl;
		}
	}
}

void WorldCreator::loadWalls() {

	_wallsNodes.reserve(_walls.size());

	for (unsigned int k = 0; k < _walls.size(); k++) {
		int i = _walls[k].x;
		int j = _walls[k].z;
		Vector3 pos(i * COEF - _matrixHeight / 2 * COEF + COEF/2, 0,
				-j * COEF + _matrixWidth / 2 * COEF - COEF/2);
		Vector3 scale(1, 1, 1);
		Quaternion orientation = Ogre::Quaternion::IDENTITY;

        std::stringstream name;
        name << "Wall";
        name << k;
        Entity* obstacle = _scnManager->createEntity("Wall.mesh");
        obstacle->setQueryFlags(WALL);
    	obstacle->setCastShadows(false);

        _wallsNodes[k] = _scnManager->getRootSceneNode()->createChildSceneNode(name.str());
        name.str().clear();
        _wallsNodes[k]->attachObject(obstacle);
        _wallsNodes[k]->setOrientation(orientation);
        _wallsNodes[k]->setPosition(pos);
        _wallsNodes[k]->setScale(scale);
    }
    // Despues de poner las paredes, ponemos la puerta
    Vector3 scale(1,1, 1);
    Quaternion orientation = Ogre::Quaternion::IDENTITY;
    Entity* obstacle = _scnManager->createEntity("Gateway","Wall.mesh");
    obstacle->setQueryFlags(WALL);
    obstacle->setMaterialName("paredPuerta");
    obstacle->setCastShadows(false);
    _nodeGateway = _scnManager->getRootSceneNode()->createChildSceneNode("WallGateway");
    _nodeGateway->attachObject(obstacle);
    _nodeGateway->setOrientation(orientation);
    _nodeGateway->setPosition(_posGateway);
    _nodeGateway->setScale(scale);
 
}

void WorldCreator::loadLights() {
	for (unsigned int k = 0; k < _lights.size(); k++) {
		int i = _lights[k].x;
		int j = _lights[k].z;
		Vector3 pos(i * COEF - _matrixHeight / 2 * COEF + COEF/2, 10,
				-j * COEF + _matrixWidth / 2 * COEF - COEF/2);
		//cout << "i: " << i << " j: " << j << endl;
		//cout << "Light x: " << (i * COEF - _matrixHeight / 2 * COEF +COEF/2) << " z:"
		//		<< (-j * COEF + _matrixWidth / 2 * COEF - COEF/2) << endl;

		std::stringstream name;
		name << "Light";
		name << k;
		Ogre::Light* light = _scnManager->createLight(name.str());
		name.str().clear();

		light->setType(Ogre::Light::LT_SPOTLIGHT);
		light->setDiffuseColour(Ogre::ColourValue::White);
		light->setSpecularColour(Ogre::ColourValue::White);
		//Se resta en la altura para quedar por debajo de la caja
		light->setPosition(pos - Vector3(0,-0.5,0));
		light->setDirection(Vector3(0,-1,0));
		//Aquí se cambia el ángulo de apertura de la luz
	    light->setSpotlightRange(Ogre::Degree(110), Ogre::Degree(120));
	    light->setCastShadows(true);
//		setLightRange(light, 200);

		SceneNode* node = _scnManager->getRootSceneNode()->createChildSceneNode();
		Entity *ent = _scnManager->createEntity( "Farol.mesh" );
                ent->setVisibilityFlags(LIGHT);
		ent->setMaterialName("Material_farol");
	    ent->setCastShadows(false);
		node->attachObject( ent );
		node->setPosition(pos);
	}
}
