/*
 * DeathState.cpp
 *
 *  Created on: Jun 12, 2014
 *      Author: julio
 */

#include "DeathState.h"
#include "Enemy.h"
#include "PlayState.h"
#include <iostream>

using namespace Ogre;
using namespace std;

shared_ptr<DeathState> DeathState::GetInstance() {
	static std::shared_ptr<DeathState> instance =
			std::make_shared<DeathState>();
	return instance;
}

void DeathState::handleState(Enemy * enemyPtr, PlayState *playStatePtr) {

	//Para solo hacerlo la primera vez
	if (!enemyPtr->_deathHandled)
	{
		enemyPtr->changeFace("dead");
		enemyPtr->selectAnimation(Enemy::DEATH, false);
		enemyPtr->_deathHandled = true;
	}
}
