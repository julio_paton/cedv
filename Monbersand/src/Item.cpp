/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "Item.h"
#include "PlayState.h"
#include <Ogre.h>

using namespace Ogre;
using namespace std;

Item::Item(string id, string model, Ogre::Vector3 position, Ogre::Vector3 scale, string materialName, int type, int value) {
    _id = id;
    _posActual = position;
    _type = type;
    _value = value;
    _empty = false;
    
    Ogre::SceneManager *scnManager =
            Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
    _entidad = scnManager->createEntity(model);
    _entidad->setQueryFlags(ITEM);
    if (!materialName.empty())
        _entidad->setMaterialName(materialName);

    _nodeModel = scnManager->getRootSceneNode()->createChildSceneNode(_id);
    _nodeModel->setPosition(_posActual);
    _nodeModel->attachObject(_entidad);
    _nodeModel->setScale(scale);
    cout << "creado Item: " << _id << endl;
}

void Item::update(Ogre::Real pTime) {
    
    _nodeModel->yaw(Ogre::Degree(360)*pTime*0.5);
}

Item::~Item() {
    std::cout << "Item: " << _id << " destructor" << endl;

    Ogre::SceneManager *scnManager =
            Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
    scnManager->getRootSceneNode()->removeChild(_nodeModel);
}

