/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "Weapon.h"
#include "PlayState.h"
#include <Ogre.h>

using namespace Ogre;
using namespace std;

Weapon::Weapon(Ogre::SceneNode* node, string id, string model, Ogre::Vector3 position, Ogre::Vector3 escale,
        Ogre::Real damage, Ogre::Real fireRate, Ogre::Real reloadTime, unsigned int magazineSize, unsigned int munition, unsigned int maxMunition) {

    _id = id;
    _bullet = 0;
    _damage = damage;
    _fireRate = fireRate;
    _reloadTime = reloadTime;
    _inReload = false;
    _magazineSize = magazineSize;
    _munitionInMagazine = magazineSize;
    _munition = munition;
    _maxMunition = maxMunition;
    _estado = W_WAIT;
    _isShoot = false;

    _crono = new Crono();
    _crono->start();

    Ogre::SceneManager *scnManager =
            Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
    _entidad = scnManager->createEntity(model);
    _entidad->setQueryFlags(VOID);
    _entidad->setCastShadows(false);
    node->setScale(escale);
    node->setPosition(position);;
    node->attachObject(_entidad);
    
    Ogre::Entity *headEntity = scnManager->createEntity("cube.mesh");
    headEntity->setQueryFlags(PLAYER_ARMA);
    _nodeHead = node->createChildSceneNode("H" + id);
    _nodeHead->attachObject(headEntity);
    _nodeHead->setScale(Ogre::Vector3(0.2,0.2,0.2));
    _nodeHead->setVisible(false);
    _nodeHead->setPosition(position+Ogre::Vector3(-0.4,0.1,-2.2));
    _nodeHead->showBoundingBox(false);
    
    _explosionsMng = new ExplosionsManager();
    cout << "creado WEAPON: " << _id << endl;
}

void Weapon::setMunition(unsigned int munition) {
    if (munition <= (_munition - _maxMunition)) {
        _munition += munition;
    } else {
        _munition = _maxMunition;
    }
}

void Weapon::shoot() {

    if (_munitionInMagazine > 0) {
        _isShoot = true;
        GameManager::getSingletonPtr()->playSoundShoot();
        _explosionsMng->createExplosion(_nodeHead->_getDerivedPosition(),1.0);
        _munitionInMagazine--;
        _crono->reset();
        _estado = W_SHOOTING;
        // cout << " cargador: " << _munitionInMagazine << " total:" << _munition << endl;
    } else if (_munition > 0) {
        reloadAmmo();
    } else {
        // Sonido de arma vacia, por ahora el de muerte
        //GameManager::getSingletonPtr()->playSoundDead();
    }

}

void Weapon::update(Ogre::Real pTime, bool shooting) {
    //Gestión de las explosiones
    _explosionsMng->finishExplosions();
    
    _isShoot = false;
    switch (_estado) {
        case W_WAIT:
            if (shooting) {
                shoot();
            }
            break;
        case W_SHOOTING:
            if (_crono->isOver(_fireRate)) {
                _estado = W_WAIT;
            }
            break;
        case W_RELOADING:

            if (_crono->isOver(_reloadTime)) {
                _inReload = false;
                _estado = W_WAIT;
               // GameManager::getSingletonPtr()->playSoundReload();
                _entidad->setVisible(true);
            }
            break;
    }
}

void Weapon::reloadAmmo() {
    _estado = W_RELOADING;
    _inReload = true;
    _entidad->setVisible(false);
    GameManager::getSingletonPtr()->playSoundReload();
    _crono->reset();
    if (_munition > 0) {
        unsigned int mAux = _magazineSize - _munitionInMagazine;
        if (_munition >= mAux) {
            _munitionInMagazine = _magazineSize;
            _munition -= mAux;
        } else {
            _munitionInMagazine += _munition;
            _munition = 0;
        }
    }
}

Weapon::~Weapon() {
    delete _explosionsMng;
}

