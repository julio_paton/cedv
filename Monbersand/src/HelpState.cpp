/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "HelpState.h"

template<> HelpState* Ogre::Singleton<HelpState>::msSingleton = 0;

using namespace std;

void HelpState::enter() {
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->createCamera("HelpCamera");

    _viewport = _root->getAutoCreatedWindow()->getViewport(0);

    _exitGame = false;
    
    createGUI();

}

void HelpState::exit() {
    CEGUI::WindowManager::getSingleton().destroyWindow("HelpWindow");
    _sceneMgr->destroyCamera("HelpCamera");
}

void HelpState::pause() {
}

void HelpState::resume() {
}

bool HelpState::frameStarted(const Ogre::FrameEvent& evt) {
    return true;
}

bool HelpState::frameEnded(const Ogre::FrameEvent& evt) {
    if (_exitGame)
        return false;

    return true;
}

void HelpState::keyPressed(const OIS::KeyEvent &e) {
}

void HelpState::keyReleased(const OIS::KeyEvent &e) {
}

void HelpState::mouseMoved(const OIS::MouseEvent &e) {
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton HelpState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void HelpState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void HelpState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

HelpState* HelpState::getSingletonPtr() {
    return msSingleton;
}

HelpState& HelpState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void HelpState::createGUI() {

    //Mouse
    CEGUI::MouseCursor::getSingleton().setVisible(true);
    CEGUI::System::getSingleton().setDefaultFont("Scarface-36");
    CEGUI::Window* Help = CEGUI::WindowManager::getSingleton().loadWindowLayout(
            "Help.layout");
    (CEGUI::WindowManager::getSingleton().getWindow("Help/ExitButton"))->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&HelpState::Volver, this));
   
    CEGUI::System::getSingleton().getGUISheet()->addChildWindow(Help);
    
}

bool HelpState::Volver(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    popState();
    return true;
}