/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#include "Crono.h"

Crono::Crono() {
    _isRunning = false;
    _inicio = 0;
    _ptimer = new Ogre::Timer();
}

void Crono::start() {
    _ptimer->reset();
    _isRunning = true;
}

void Crono::pause() {
    if (_isRunning) {
        _inicio += _ptimer->getMilliseconds();
        _isRunning = false;
    }
}

void Crono::resume() {
    if (!_isRunning) {
        _ptimer->reset();
        _isRunning = true;
    }
}

void Crono::reset() {
    _inicio = 0;
    _ptimer->reset();
}

Ogre::Real Crono::getTime() {
    unsigned long time;
    if (_isRunning) {
        time = _ptimer->getMilliseconds() + _inicio;
    } else {
        time = _inicio;
    }
    return time;
}

bool Crono::isOver(Ogre::Real seconds) {
    return getTime() >= (seconds * 1000);
}
