/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "ScoresState.h"

template<> ScoresState* Ogre::Singleton<ScoresState>::msSingleton = 0;

using namespace std;

void ScoresState::enter() {
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->createCamera("ScoresCamera");

    _viewport = _root->getAutoCreatedWindow()->getViewport(0);

    _exitGame = false;
    
    createGUI();

}

void ScoresState::exit() {
    CEGUI::WindowManager::getSingleton().destroyWindow("ScoresWindow");
    _sceneMgr->destroyCamera("ScoresCamera");
}

void ScoresState::pause() {
}

void ScoresState::resume() {
}

bool ScoresState::frameStarted(const Ogre::FrameEvent& evt) {
    return true;
}

bool ScoresState::frameEnded(const Ogre::FrameEvent& evt) {
    if (_exitGame)
        return false;

    return true;
}

void ScoresState::keyPressed(const OIS::KeyEvent &e) {
}

void ScoresState::keyReleased(const OIS::KeyEvent &e) {
}

void ScoresState::mouseMoved(const OIS::MouseEvent &e) {
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton ScoresState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void ScoresState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void ScoresState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

ScoresState* ScoresState::getSingletonPtr() {
    return msSingleton;
}

ScoresState& ScoresState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void ScoresState::createGUI() {

    //Mouse
    CEGUI::MouseCursor::getSingleton().setVisible(true);
    CEGUI::System::getSingleton().setDefaultFont("Scarface-36");
    CEGUI::Window* scores = CEGUI::WindowManager::getSingleton().loadWindowLayout(
            "Scores.layout");
    (CEGUI::WindowManager::getSingleton().getWindow("Scores/ExitButton"))->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ScoresState::Volver, this));
    
        std::vector<ScoresData::pair_score> lScores = ScoresData::getInstance().getScores();
        std::string name;
        std::string level;
        std::stringstream aux;
        std::stringstream aux2;
        int sepPos;
        for (unsigned int i = 0; i < lScores.size() && i < 5; i++) {
            
            cout << "Nombre:" << lScores[i].first << " Score: " << lScores[i].second << endl;
            
            aux.str("");
            aux << "Scores/" << i + 1 << "/Pos";
            CEGUI::WindowManager::getSingleton().getWindow(aux.str())->setText(Ogre::StringConverter::toString(i + 1));
            aux.str("");
            sepPos = lScores[i].first.find('#');
            
            name = lScores[i].first.substr(0, sepPos);
            level = "Nivel " + lScores[i].first.substr(sepPos + 1);
            aux << "Scores/" << i + 1 << "/Nick";
            CEGUI::WindowManager::getSingleton().getWindow(aux.str())->setText(name);
            aux.str("");
            aux << "Scores/" << i + 1 << "/Level";
            CEGUI::WindowManager::getSingleton().getWindow(aux.str())->setText(level);
            aux.str("");
            aux << "Scores/" << i + 1 << "/Score";
            aux2.str("");
          //  aux2 << lScores[i].second << ".000";
            aux2 << lScores[i].second ;
            CEGUI::WindowManager::getSingleton().getWindow(aux.str())->setText(aux2.str());
        }
    CEGUI::System::getSingleton().getGUISheet()->addChildWindow(scores);
}

bool ScoresState::Volver(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    popState();
    return true;
}