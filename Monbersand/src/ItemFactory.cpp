/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "ItemFactory.h"
#include "Defines.h"

using namespace Ogre;

ItemPtr ItemFactory::makeItemHealt(Ogre::Vector3 position) {

    ItemPtr lItem = make_shared<Item>(
            "ItemSalud" + Ogre::StringConverter::toString(++_nextId), "MedicalKit.mesh",position,Ogre::Vector3(0.2,0.2,0.2),"medicalkit", I_HEALT,15);
    return lItem;

}

ItemPtr ItemFactory::makeItemMunition(Ogre::Vector3 position) {

    ItemPtr lItem = make_shared<Item>(
            "ItemMunition" + Ogre::StringConverter::toString(++_nextId), "Balas.mesh",position,Ogre::Vector3(0.3,0.3,0.3),"Balas", I_MUNITION,12);
    return lItem;

}
ItemPtr ItemFactory::makeItemKey(Ogre::Vector3 position) {

    ItemPtr lItem = make_shared<Item>(
            "ItemKey" + Ogre::StringConverter::toString(++_nextId), "Llave.mesh",position,Ogre::Vector3(1,1,1),"golden_key", I_KEY,50);
    return lItem;

}
