/*
 * ChaseState.cpp
 *
 *  Created on: Jun 9, 2014
 *      Author: julio
 */

#include "ChaseState.h"
#include "AttackState.h"
#include "Enemy.h"
#include "PlayState.h"
#include <iostream>

using namespace std;
using namespace Ogre;

shared_ptr<ChaseState> ChaseState::GetInstance() {
    static shared_ptr<ChaseState> instance = make_shared<ChaseState>();
    return instance;
}

void ChaseState::handleState(Enemy * enemyPtr, PlayState *playStatePtr) {

	//El enemigo se mueve entre dos posiciones
	if (enemyPtr->_chaseDirection == Vector3::ZERO) {
		if (enemyPtr->nextPatrolLocation()) {
			//Set Walk Animation
			enemyPtr->selectAnimation(Enemy::ANDAR, true);
		}
		else
		{
//			cout << "El enemigo " << enemyPtr->getId()
//	                    				<< " no tiene más destinos donde patrullar" << endl;
			enemyPtr->updateChasePath();
		}
	}
	else
	{
		Real move = enemyPtr->_chaseSpeed * playStatePtr->getDeltaT();
		enemyPtr->_chaseWalkDistance -= move;

		if (enemyPtr->_chaseWalkDistance <= 0.0f) {
			//Ya ha llegado al destino
			enemyPtr->getNode()->setPosition(enemyPtr->_chaseDestination);
			enemyPtr->_chaseDestination = Vector3::ZERO;

			/*Se actualiza el camino cada vez que se alcanza un destino del camino
			 * Sirve para evitar el choque entre entre enemigos
			*/
			enemyPtr->updateChasePath();

			if (enemyPtr->nextPatrolLocation())
			{
				//Se envía al enemigo hacia el próximo destino en caso de haber uno
				;//no_hacer_nada
			}
			else
			{
				//Set idle animation
				cout << "idle animation " << endl;
				enemyPtr->selectAnimation(Enemy::IDLE, true);
				enemyPtr->_chaseDirection = Vector3::ZERO;
			}
		} else {
			//Seguimos avanzando
			enemyPtr->getNode()->translate(enemyPtr->_chaseDirection * move);
		}
	}

    //Comprobar la distancia al jugador para cambiar de estado
    Real distance = enemyPtr->getNode()->getPosition().distance(
            playStatePtr->getPlayerPtr()->getNode()->getPosition());
    //Si el enemigo está dentro de su campo de visión y no hay otro enemigo atacando, empieza a atacarlo
    if (distance < enemyPtr->_attackDistance && !enemyPtr->anotherEnemyIsAttacking()) {
        enemyPtr->changeFace("angry");
        enemyPtr->changeState(AttackState::GetInstance());
        //Reset variables del estado
        enemyPtr->_chaseDirection = Vector3::ZERO;
        enemyPtr->_chaseWalkDistance = 0;
        enemyPtr->_chaseDestination = Vector3::ZERO;
        enemyPtr->_chaseWalkList.clear();
    }

}
