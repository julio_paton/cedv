/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "EnemyFactory.h"
#include "Defines.h"

using namespace Ogre;

EnemyPtr EnemyFactory::makeEnemyGolem(unsigned int mask, Ogre::Vector3 position) {

    EnemyPtr lEnemy = make_shared<Enemy>(
            "Enemy" + Ogre::StringConverter::toString(++_nextId), "Golem.mesh", 100,10,
            mask, position, 0.4, Ogre::Vector3(-0.2, 4.4, 1),Ogre::Vector3(1.4, 0.5, 1),
            "Golem_Stone",E_SOLDIER);
    return lEnemy;

}

EnemyPtr EnemyFactory::makeEnemyBoss(unsigned int mask, Ogre::Vector3 position) {

    EnemyPtr lEnemy = make_shared<Enemy>(
            "Enemy" + Ogre::StringConverter::toString(++_nextId), "Golem.mesh", 350,20,
            mask, position, 0.6, Ogre::Vector3(-0.2, 4.6, 1),Ogre::Vector3(1.5, 0.8, 1),
            "Golem_Fire",E_BOSS);
    return lEnemy;

}

void EnemyFactory::initialCreation(std::vector<EnemyPtr>& enemies)
{
	for (CreationZonePtr creationZone : _creationZonesPos)
	{
		if ( !creationZone->_hasAnActiveEnemy && creationZone->_enemiesCreated < MAX_ENEMIES_PER_ZONE){
			enemies.push_back( makeEnemyGolem(PLAYER_ENEMIGO, creationZone->_pos) );
			creationZone->_enemiesCreated++;
		}
	}
}

void EnemyFactory::checkNewEnemyCreation(std::vector<EnemyPtr>& enemies,
		const EnemyPtr& enemyDead)
{
	for (CreationZonePtr creationZone : _creationZonesPos)
	{
		if ( creationZone->_pos == enemyDead->getPosInicial())
		{
			if ( !creationZone->_hasAnActiveEnemy && creationZone->_enemiesCreated < MAX_ENEMIES_PER_ZONE){
				enemies.push_back( makeEnemyGolem(PLAYER_ENEMIGO, creationZone->_pos) );
				creationZone->_enemiesCreated++;
			}
		}
	}

}
