/*
 * AttackState.cpp
 *
 *  Created on: Jun 9, 2014
 *      Author: julio
 */

#include "AttackState.h"
#include "ChaseState.h"
#include "Enemy.h"
#include "PlayState.h"
#include <iostream>

using namespace std;
using namespace Ogre;

shared_ptr<AttackState> AttackState::GetInstance() {
	static shared_ptr<AttackState> instance =
			make_shared<AttackState>();
	return instance;
}

void AttackState::handleState(Enemy * enemyPtr, PlayState *playStatePtr) {

	if (!enemyPtr->_isAttacking )
	{
		// Atacamos
		//Se rota el enemigo hacia eljugador
		Vector3 direction = Ogre::Vector3::ZERO;
		direction.x = (playStatePtr->getPlayerPtr()->getNode()->getPosition().x
				- enemyPtr->getNode()->getPosition().x);
		direction.z = (playStatePtr->getPlayerPtr()->getNode()->getPosition().z
				- enemyPtr->getNode()->getPosition().z);
		direction.normalise();
		Vector3 src = enemyPtr->getNode()->getOrientation() * Vector3::UNIT_Z;
		Quaternion quat = src.getRotationTo(direction);
		enemyPtr->getNode()->rotate(quat);

		//Comienza la animación de atacar
		enemyPtr->selectAnimation(Enemy::FIRE, false);
		enemyPtr->_isAttacking = true;

	}
	else
	{
		//restar tiempo, si ha llegado el momento de golpeo al jugador se comprueba
		enemyPtr->_attackHitInstantLeft -= playStatePtr->getDeltaT();

		if (enemyPtr->_attackHitInstantLeft < 0 && !enemyPtr->_hitWasChecked)
		{
			enemyPtr->checkHit();
			enemyPtr->_hitWasChecked = true;
		}
		if (enemyPtr->_attackHasEnded){
			enemyPtr->changeFace("alert");
			enemyPtr->changeState(ChaseState::GetInstance());
			//Se resetean las variables del estadoww
			enemyPtr->_attackHitInstantLeft = enemyPtr->_attackHitInstant;
			enemyPtr->_isAttacking = false;
			enemyPtr->_attackHasEnded = false;
			enemyPtr->_hitWasChecked = false;
		}
	}
}
