/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "Enemy.h"
#include <Ogre.h>
#include "PatrolState.h"
#include "DeathState.h"
#include "PlayState.h"

using namespace Ogre;
using namespace std;

Enemy::Enemy(string id, string model, unsigned int vida, unsigned int contactDamage, unsigned int mask,
        Ogre::Vector3 position, float scale, Ogre::Vector3 positionHead, Ogre::Vector3 scaleHead,
        string material, unsigned int type) {

    _id = id;
    _mask = mask;
    _isDead = false;
    _isHurt = false;
    _showFace = false;
    _vida = vida;
    _contactDamage = contactDamage;
    _posInicial = position;
    _type = type;

    // Materiales
    _mapFaces["alert"] = Ogre::FloatRect(0.0, 0.0, 0.5, 0.5);
    _mapFaces["angry"] = Ogre::FloatRect(0.5, 0.0, 1.0, 0.5);
    _mapFaces["hurt"] = Ogre::FloatRect(0.0, 0.5, 0.5, 1.0);
    _mapFaces["dead"] = Ogre::FloatRect(0.5, 0.5, 1.0, 1.0);

    Ogre::SceneManager *scnManager =
            Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
    _bodyEntity = scnManager->createEntity(model);
    _bodyEntity->setQueryFlags(VOID);
    _bodyEntity->setCastShadows(true);

    _nodeModel = scnManager->getRootSceneNode()->createChildSceneNode("B" + _id);
    _nodeModel->setScale(scale, scale, scale);
    _nodeModel->setPosition(_posInicial);
    _nodeModel->attachObject(_bodyEntity);
    //    _nodeModel->showBoundingBox(true);

    _facesBillboardSet = scnManager->createBillboardSet("FaceBillboardSet" + _id, 1);

    _facesBillboardSet->setBillboardType(Ogre::BBT_POINT);
    _facesBillboardSet->setMaterialName("Faces");
    _facesBillboardSet->setDefaultDimensions(2, 2);
    _facesBillboardSet->setVisible(false);
    _face = _facesBillboardSet->createBillboard(Ogre::Vector3(2, 0, 0));

    Ogre::SceneNode* faceNode = _nodeModel->createChildSceneNode("FaceNode" + _id);
    faceNode->setPosition(0, 6, 0);
    faceNode->setVisible(false);
    faceNode->attachObject(_facesBillboardSet);

    //Nodo hijo solo mesh para ajustar colision
    Ogre::SceneNode *nodebox = _nodeModel->createChildSceneNode(_id);
    _entboxcol = scnManager->createEntity("cube.mesh");
    _entboxcol->setQueryFlags(PLAYER_ENEMIGO);
    nodebox->attachObject(_entboxcol);
    nodebox->setPosition(Ogre::Vector3(0, 2, 0));
    nodebox->setScale(2.4 , 1.8 , 2.4 );
    nodebox->setVisible(false);
    nodebox->showBoundingBox(false);

    _animations.push_back(_bodyEntity->getAnimationState("Idle"));
    _animations.push_back(_bodyEntity->getAnimationState("Fire"));
    _animations.push_back(_bodyEntity->getAnimationState("Death"));
    _animations.push_back(_bodyEntity->getAnimationState("Andar"));


    // Default behaviour
    disableAnimations();
    _activeAnimation = _animations.at(0);
    selectAnimation(IDLE, true);

    _nodeHead = _nodeModel->createChildSceneNode("H" + id);
    _headEntity = scnManager->createEntity("cube.mesh");
    _headEntity->setQueryFlags(PLAYER_ENEMIGO_HEAD);
    _nodeHead->attachObject(_headEntity);
    _nodeHead->setScale(scaleHead);
    _nodeHead->setVisible(false);
    _nodeHead->setPosition(positionHead);
    _nodeHead->showBoundingBox(false);

    if (!material.empty())
        _bodyEntity->setMaterialName(material);

    //IA:estado por defecto inicial
    _currentState = PatrolState::GetInstance();
    _playStatePtr = PlayState::getSingletonPtr();

    //Puntero al pathfinder
    _pathFinder = _playStatePtr->getPathFinder();

    //PatrolState
    _bossVisionDistance = BOSS_VISION_DISTANCE;
    _hasBeenHit = false;

    //ChaseState
    _chaseDirection = Ogre::Vector3::ZERO;
    _chaseDestination = Vector3::ZERO;
    _chaseWalkDistance = 0;
    _attackDistance = START_ATTACK_DISTANCE;
    _chaseSpeed = CHASE_SPEED;

    //AttackState
    _isAttacking = false;
    _attackHitInstant = ATTACK_HIT_INSTANT;
    _attackHitInstantLeft = _attackHitInstant;
    _attackHasEnded = false;
    _hitWasChecked = false;

    //DeathState
    _deathHandled = false;

}

Enemy::~Enemy() {
    std::cout << "Enemy: " << _id << " destructor" << endl;

    Ogre::SceneManager *scnManager =
            Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
    scnManager->getRootSceneNode()->removeChild(_nodeModel);
}

void Enemy::checkHit() {
    if (PlayState::getSingletonPtr()->isCollision(_nodeModel->getPosition(), _nodeModel->getOrientation()
            * Ogre::Vector3::UNIT_Z, PLAYER_AMIGO, hitDistance)) {
        PlayState::getSingletonPtr()->hitByEnemy(_contactDamage);
    }
}

void Enemy::selectAnimation(const unsigned int pIndex, bool pLoop) {
    _activeAnimation->setEnabled(false);

    if (pIndex < _animations.size()) {
        _activeAnimation = _animations.at(pIndex);
        _activeAnimation->setEnabled(true);
        _activeAnimation->setTimePosition(0.0);
        _activeAnimation->setLoop(pLoop);

    } else {
        // Animation doesn't exist
        // Default state
        _activeAnimation = _animations.at(0);
        _activeAnimation->setLoop(true);
    }

}

void Enemy::disableAnimations() {
    // Desactivate all animations
    itAnimation = _animations.begin();
    while (itAnimation != _animations.end()) {
        (*itAnimation)->setEnabled(false);
        itAnimation++;
    }

}

void Enemy::update(Ogre::Real pTime) {
    _currentState->handle(this, _playStatePtr);
    if (!_activeAnimation->hasEnded()) {
        _activeAnimation->addTime(pTime);
    }
    else
    {
        if (_activeAnimation->getAnimationName() == "Fire")
        {
        	_attackHasEnded = true;
        	selectAnimation(IDLE, true);
        }
        else if (_activeAnimation->getAnimationName() == "Death" && !_isDead)
        {
        	PlayState::getSingleton().anEnemyWasKilled(_id);
            _isDead = true;
        }
    }
    if (_isHurt) {//Simula el tiempo en llegar la bala
        _timeSinceLastHurt += pTime;
        if (_timeSinceLastHurt > 0.4) {
            _timeSinceLastHurt = 0;
            if(_isDead){
                GameManager::getSingletonPtr()->playSoundDead();
            }else{
                GameManager::getSingletonPtr()->playSoundHitEnemy();
                changeFace("hurt");
            }
            _isHurt = false;
        }
    }
    if (_showFace) {
        _timeSinceLastFace += pTime;
        if (_timeSinceLastFace > 0.5) {
            _timeSinceLastFace = 0;
            _showFace = false;
            _facesBillboardSet->setVisible(false);
        }
    }

}

void Enemy::changeFace(string faceNew) {
    _face->setTexcoordRect(_mapFaces[faceNew]);
    _facesBillboardSet->setVisible(true);
    _showFace = true;
}

void Enemy::changeState(std::shared_ptr<State> newState) {
    _currentState = newState;
}

bool Enemy::nextPatrolLocation() {
    if (_chaseWalkList.empty())
        return false;
    _chaseDestination = _chaseWalkList.front(); // this gets the front of the deque
    _chaseWalkList.pop_front(); // this removes the front of the deque
    _chaseDirection = _chaseDestination - _nodeModel->getPosition();
    _chaseWalkDistance = _chaseDirection.normalise();

    //Rotar modelo hacia el próximo destino
    Vector3 src = _nodeModel->getOrientation() * Vector3::UNIT_Z;
    if ((1.0f + src.dotProduct(_chaseDirection)) < 0.0001f) {
    	_nodeModel->yaw(Degree(180));
    } else {
    	Quaternion quat = src.getRotationTo(_chaseDirection);
    	_nodeModel->rotate(quat);
    }
    return true;
}

void Enemy::setHit(Ogre::Real damage) {
    _vida -= damage;
    _isHurt = true;
    _hasBeenHit = true;

    if (_vida <= 0) {
        _isHurt = false;
        _mask = PLAYER_ENEMIGO_MUERTO;
        _bodyEntity->setQueryFlags(_mask);
        _headEntity->setQueryFlags(_mask);
        _entboxcol->setQueryFlags(_mask);

        if (_type == E_BOSS)
            PlayState::getSingletonPtr()->dropItem(I_KEY, Ogre::Vector3(_nodeModel->getPosition().x, 0, _nodeModel->getPosition().z));
        else
            PlayState::getSingletonPtr()->dropItem(I_EMPTY, Ogre::Vector3(_nodeModel->getPosition().x, 0, _nodeModel->getPosition().z));

        changeState(DeathState::GetInstance());
        //Se resetea porque afecta al resto de enemigos
        _isAttacking = false;
    }
}

bool Enemy::anotherEnemyIsAttacking()
{
	std::vector<EnemyPtr> enemies = _playStatePtr->getEnemies();
	auto it = find_if(enemies.begin(), enemies.end(), [](EnemyPtr enemy) { return enemy->_isAttacking == true;});

	if (it != enemies.end())
	{
		return true;
	}
	return false;
}

void Enemy::setChaseWalkListFromVector(
		const std::vector<Ogre::Vector3>& patrolWalkList) {
	_chaseWalkList.clear();
	//Se fuerza a que actualize la próxima dirección
	_chaseDirection = Vector3::ZERO;
	if ( !patrolWalkList.empty() )
	{
		std::copy(patrolWalkList.begin(), patrolWalkList.end(), std::inserter(_chaseWalkList, _chaseWalkList.end()));
//		//Para evitar el efecto de que el enemigo vuelve hacia atrás
		_chaseWalkList.pop_front();
	}
}

void Enemy::updateChasePath() {
    //Calcular camino para patrullar hacia el player
	Vector3 posPlayer = _playStatePtr->getPlayerPtr()->getPos();

	//Establecer la posición inicial de la búsqueda
	_pathFinder->setStartPos( lround( _nodeModel->getPosition().x ),
			lround( _nodeModel->getPosition().z) );
	//Establecer la posición de todos los enemigos
	std::vector<EnemyPtr> restEnemies = _playStatePtr->getEnemies();
	string id = _id;
	auto it = find_if(restEnemies.begin(), restEnemies.end(), [id](EnemyPtr enemy) { return enemy->getId() == id;});

	if (it != restEnemies.end())
	{
		restEnemies.erase(it);
	}

	_pathFinder->setEnemiesPos( restEnemies );

	int result = _pathFinder->setDestinyPos(lround( posPlayer.x ), lround( posPlayer.z ));

	std::vector<Vector3> path;

	if (result == MicroPather::SOLVED) {
		_pathFinder->getComputedPath(path);
	}
	setChaseWalkListFromVector(path);

	_pathFinder->clearLastSearch();

    _lastPosUpdate = posPlayer;
}

