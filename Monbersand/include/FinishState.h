/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef FinishState_H
#define FinishState_H

#include <Ogre.h>
#include "CEGUI.h"
#include <OIS/OIS.h>

#include "GameState.h"
#include "ScoresData.h"

class FinishState : public Ogre::Singleton<FinishState>, public GameState
{
 public:
  FinishState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  bool RepeatLevel(const CEGUI::EventArgs &e);
  bool NextLevel(const CEGUI::EventArgs &e);
  bool Puntuaciones(const CEGUI::EventArgs &e);
  bool Portada(const CEGUI::EventArgs &e);
  bool Salir(const CEGUI::EventArgs &e);

  // Heredados de Ogre::Singleton.
  static FinishState& getSingleton ();
  static FinishState* getSingletonPtr ();

  // Miembros para widgets menu principal graficamente
  CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);
  void createGUI();

 protected:
  void saveScore();
  
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;

  bool _exitGame;
  bool _scoreSaved;
};

#endif
