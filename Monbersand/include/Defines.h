#ifndef DEFINES_H
#define DEFINES_H

#include <string>

//Mascaras para el ray query
#define GROUND 1 << 0    // Mascara para el SUELO
#define ROOF 1 << 0 //Mascara para el TECHO
#define VOID 1 << 1    // Mascara para lo que no cuenta
#define PLAYER_AMIGO 1 << 2    // Mascara para el PLAYER AMIGO
#define PLAYER_ENEMIGO 1 << 3  // Mascara para el PLAYER ENEMIGO
#define PLAYER_ENEMIGO_HEAD 1 << 4  // Mascara para el PLAYER ENEMIGO CABEZA
#define PLAYER_ENEMIGO_MUERTO 1 << 5  // Mascara para el PLAYER ENEMIGO MUERTO
#define PLAYER_ARMA 1 << 6  // Mascara para la ARMA DEL PLAYER
#define ITEM 1 << 7  // Mascara para los ITEMS
#define WALL 1 << 8  // Mascara para los MUROS
#define GATEWAY 1 << 9  // Mascara para la PUERTA
#define LIGHT 1 << 10  // Mascara para la PUERTA

// Para los items
#define I_EMPTY 0
#define I_HEALT 1
#define I_MUNITION 2
#define I_KEY 3

//Estado del ARMA
#define W_WAIT 4
#define W_SHOOTING 5
#define W_RELOADING 6

//Distancia de golpeo
const float hitDistance(3.5);

//Tipos de enemigo
#define E_SOLDIER 7
#define E_BOSS 8

// Acciones en el juego
#define A_EXIT 0
#define A_UP 1
#define A_DOWN 2
#define A_LEFT 3
#define A_RIGHT 4
#define A_TURN_LEFT 5
#define A_TURN_RIGHT 6
#define A_SHOOT 7
#define A_RELOAD 8
#define A_PAUSE 9

// Para el mapa del mundo
const int COEF = 10;

// Para las puntuaciones
const int PUNTUACION_BASE = 2000;
const int TIEMPO_MAXIMO = 600;
const int VELOCIDAD_JUGADOR = 6;

#define M_WALL 'X'
#define M_PLAYER 'P'
#define M_ENEMY 'E'
#define M_BOSS 'B'
#define M_GATEWAY 'G'
#define M_LIGHT 'L'

#define T_EMPTY 99
#define T_WALL 11
#define T_PLAYER 20
#define T_ENEMY 30

//Estados de los enemigos
//PatrolState
const int BOSS_VISION_DISTANCE(25);
//ChaseState
const float START_ATTACK_DISTANCE(3.5);
const float CHASE_SPEED(4.0f);
//AttackState
const float ATTACK_HIT_INSTANT(0.3f);

//Creación de enemigos
const int MAX_ENEMIES_PER_ZONE(5);


#endif // DEFINES_H
