/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef PLAYER_H
#define PLAYER_H

#include "Player.h"
#include "Weapon.h"
#include <Ogre.h>

using namespace std;

class Player {
public:

    Player(string id, std::string model, unsigned int mask, Ogre::Vector3 position, string materialName = "");
    ~Player();

    string getId() {
        return _id;
    }

    int getVida() {
        return _vida;
    }
    
    void setVida(int vida);
    void setMunition(unsigned int munition);

    Ogre::SceneNode* getNode() {
        return _nodeModel;
    }

    Ogre::SceneNode* getNodeWeapon() {
        return _nodeWeapon;
    }

    Ogre::Entity* getEntity() {
        return _entidad;
    }

    Ogre::Vector3 getPos() {
        return _nodeModel->getPosition();
    }

    WeaponPtr getWeapon() {
        return _selectedWeapon;
    }

    void update(Ogre::Real pTime, Ogre::Real _rotx, Ogre::Real _roty);

    void adelante(bool adelante) {
        _adelante = adelante;
    }

    void atras(bool atras) {
        _atras = atras;
    }

    void izquierda(bool izquierda) {
        _izquierda = izquierda;
    }

    void derecha(bool derecha) {
        _derecha = derecha;
    }

    void giroIzquierda(bool izquierda) {
        _giroIzquierda = izquierda;
    }

    void giroDerecha(bool derecha) {
        _giroDerecha = derecha;
    }

    void setShooting(bool shooting) {
        _shooting = shooting;
    }

    bool _isShooting() {
        return _shooting;
    }
    
    void addWeapon(WeaponPtr lWeapon);
    
    //Animaciones

    enum Animations {
        IDLE, ANDAR, NONE
    };
    
    void selectAnimation(const unsigned int pIndex, bool pLoop = false);
    void disableAnimations();

private:
    Ogre::Vector3 _posInicial;
    bool _adelante;
    bool _atras;
    bool _izquierda;
    bool _derecha;
    bool _giroIzquierda;
    bool _giroDerecha;
    bool _shooting;
    Ogre::Real _velocidad;

    unsigned int _mask;
    Ogre::SceneNode *_nodeModel;
    Ogre::SceneNode *_nodeWeapon;
    Ogre::Entity *_entidad;
    Ogre::Entity *_entidadModel;

    std::vector<WeaponPtr> _weapons;
    
    WeaponPtr _selectedWeapon;

    string _id;
    int _vida;
    int _maxVida;
    
    //Animaciones
    std::vector<Ogre::AnimationState*>::iterator itAnimation;
    std::vector<Ogre::AnimationState*> _animations;
    Ogre::AnimationState* _activeAnimation;

};

typedef shared_ptr<Player> PlayerPtr;

#endif
