/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef ENEMY_H
#define ENEMY_H

#include <Ogre.h>
#include "PatrolState.h"

class PathFinder;

using namespace std;

class Enemy {
public:

    Enemy(string id, std::string model, unsigned int vida, unsigned int contactDamage, unsigned int mask,
            Ogre::Vector3 position, Ogre::Real scale, Ogre::Vector3 positionHead, Ogre::Vector3 scaleHead,
            string material,unsigned int type);
    ~Enemy();

    string getId() {
        return _id;
    }

    int getVida() {
        return _vida;
    }

    Ogre::SceneNode* getNode() {
        return _nodeModel;
    }

    Ogre::Entity* getBodyEntity() {
        return _bodyEntity;
    }

    bool isDead() {
        return _isDead;
    }

    void setHit(Ogre::Real damage);

    unsigned int getContactDamage() {
        return _contactDamage;
    }
    
    unsigned int getType() {
        return _type;
    }

    //IA
    void update(Ogre::Real pTime);
    void changeState(std::shared_ptr<State> newState);
    void changeFace(string faceNew);
    bool nextPatrolLocation();
    void updateChasePath();

    //Búsqueda de caminos para los enemigos
    std::shared_ptr<PathFinder> _pathFinder;
    Ogre::Vector3 _lastPosUpdate; //Guarda la última posición del player en la que los caminos fueron actualizados

    //PatrolState
    Ogre::Real _bossVisionDistance;
    bool _hasBeenHit;

    //ChaseState
    Ogre::Real _attackDistance; //Distancia tras la cual el enemigo ve al jugador y lanza el ataque
    Ogre::Real _chaseWalkDistance; // The distance the enemy has left to travel
    Ogre::Vector3 _chaseDirection; // The direction the enemy is moving
    Ogre::Vector3 _chaseDestination; // The destination the enemy is moving towards
    Ogre::Real _chaseSpeed; // The speed at which the object is moving
    std::deque<Ogre::Vector3> _chaseWalkList; //Direcciones obtenidas en la búsqueda de caminos
        
    //AttackState
    bool _isAttacking;
    Ogre::Real _attackHitInstant; //Para calcular el momento en el que se golpea al jugador
    Ogre::Real _attackHitInstantLeft;
    bool _attackHasEnded;
    bool _hitWasChecked;
    bool anotherEnemyIsAttacking();
    //DeathState
    bool _deathHandled;

    //Billboard
    Ogre::BillboardSet* _facesBillboardSet;
    Ogre::Billboard* _face;

    //Animaciones

    enum Animations {
        IDLE, FIRE, DEATH, ANDAR, NONE
    };
    void checkHit();
    void selectAnimation(const unsigned int pIndex, bool pLoop = false);
    void disableAnimations();

	const std::deque<Ogre::Vector3>& getPatrolWalkList() const {
		return _chaseWalkList;
	}

	const Ogre::Vector3& getPosInicial() const
	{
		return _posInicial;
	}

private:
    Ogre::Vector3 _posInicial;

    unsigned int _mask;
    Ogre::SceneNode *_nodeModel;
    Ogre::SceneNode *_nodeHead;
    Ogre::Entity *_bodyEntity;
    Ogre::Entity *_entboxcol;
    Ogre::Entity *_headEntity;

    std::map<string, Ogre::FloatRect> _mapFaces;

    string _id;
    int _vida;
    unsigned int _contactDamage;
    unsigned int _type;

    bool _isDead;
    bool _isHurt;
    bool _showFace;
    
    Ogre::Real _timeSinceLastFace;
    Ogre::Real _timeSinceLastHurt;

    //IA
    std::shared_ptr<State> _currentState;
    PlayState *_playStatePtr;
    //ChaseState
    void setChaseWalkListFromVector(const std::vector<Ogre::Vector3>& patrolWalkList);

    //Animaciones
    std::vector<Ogre::AnimationState*>::iterator itAnimation;
    std::vector<Ogre::AnimationState*> _animations;
    Ogre::AnimationState* _activeAnimation;
};

typedef std::shared_ptr<Enemy> EnemyPtr;

#endif
