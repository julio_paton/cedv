/*
 * PatrolState.h
 *
 *  Created on: Jun 9, 2014
 *      Author: julio
 */

#ifndef PATROLSTATE_H_
#define PATROLSTATE_H_

#include "State.h"

class PatrolState: public State {

public:
	PatrolState() {
	}
	~PatrolState() {
	}
	static std::shared_ptr<PatrolState> GetInstance();
private:
	void handleState(
			Enemy * enemyPtr, PlayState *playStatePtr);
};

#endif /* PATROLSTATE_H_ */
