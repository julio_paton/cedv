/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef EXPLOSIONSMANAGER_H_
#define EXPLOSIONSMANAGER_H_

#include <Ogre.h>
#include <time.h>

class ExplosionsManager {

public:
	ExplosionsManager();
	~ExplosionsManager();

	void finishExplosions();
	void createExplosion(Ogre::Vector3 pos, Ogre::Real size=7);

private:
	// The set of all the billboards used for the explosions
	Ogre::BillboardSet* _explosionsBillSet;
	std::vector<std::pair<Ogre::Billboard*, time_t> > _explosionsBoards;
};

#endif /* EXPLOSIONSMANAGER_H_ */
