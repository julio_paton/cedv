/*
 * State.h
 *
 *  Created on: Jun 9, 2014
 *      Author: julio
 */

#ifndef STATE_H_
#define STATE_H_

#include <memory>
class Enemy;
class PlayState;

class State {
public:
	virtual ~State() {}
	void handle(Enemy * enemyPtr, PlayState *playStatePtr){ handleState(enemyPtr, playStatePtr); }

private:
	virtual void handleState(Enemy * enemyPtr, PlayState *playStatePtr) = 0;
};

#endif /* STATE_H_ */
