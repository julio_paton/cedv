/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#ifndef ENEMYFACTORY_H
#define ENEMYFACTORY_H

#include "Enemy.h"

class CreationZone
{
public:
	CreationZone(Ogre::Vector3 pos) : _pos(pos), _enemiesCreated(0), _hasAnActiveEnemy(false){}
	Ogre::Vector3 _pos;
	int _enemiesCreated;
	bool _hasAnActiveEnemy;
};

typedef std::shared_ptr<CreationZone> CreationZonePtr;

class EnemyFactory
{
public:
	EnemyFactory(const std::vector<Ogre::Vector3>& enemiesPositions) :
			_nextId(0)
	{
		for (Ogre::Vector3 pos : enemiesPositions)
			_creationZonesPos.push_back(std::make_shared<CreationZone>(pos));
	}
	EnemyPtr makeEnemyGolem(unsigned int mask, Ogre::Vector3 position);
	EnemyPtr makeEnemyBoss(unsigned int mask, Ogre::Vector3 position);

	void initialCreation(std::vector<EnemyPtr>& enemies);
	void checkNewEnemyCreation( std::vector<EnemyPtr>& enemies, const EnemyPtr& enemyDead);

private:
	int _nextId;
	std::vector<CreationZonePtr> _creationZonesPos;
};

typedef std::shared_ptr<EnemyFactory> EnemyFactoryPtr;



#endif // ENEMYFACTORY_H
