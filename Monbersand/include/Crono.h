/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef CRONO_H
#define CRONO_h

#include <Ogre.h>

using namespace std;

class Crono{
public:
    Crono();
    ~Crono();
    
    void start();
    void pause();
    void resume();
    void reset();
    bool isRunning(){
        return _isRunning;
    }
    Ogre::Real getTime();
    bool isOver (Ogre::Real secs);
private:
    Ogre::Timer* _ptimer;
    Ogre::Real _isRunning;
    unsigned long  _inicio;
    
};

#endif
