/*
 * AttackState.h
 *
 *  Created on: Jun 9, 2014
 *      Author: julio
 */

#ifndef ATTACKSTATE_H_
#define ATTACKSTATE_H_

#include "State.h"

class AttackState: public State {

public:
	AttackState() {
	}
	~AttackState() {
	}
	static std::shared_ptr<AttackState> GetInstance();
private:
	void handleState(Enemy * enemyPtr, PlayState *playStatePtr);
};

#endif /* ATTACKSTATE_H_ */
