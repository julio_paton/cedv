/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef ITEM_H
#define ITEM_H

#include "Item.h"
#include <Ogre.h>
#include <memory>

using namespace std;

class Item {
public:

    Item(string id, string model, Ogre::Vector3 position, Ogre::Vector3 scale, string materialName, int type, int value);
    ~Item();

    string getId() {
        return _id;
    }

    unsigned int getValue() {
        _empty = true;
        return _value;
    }

    Ogre::SceneNode* getNode() {
        return _nodeModel;
    }

    int getType() {
        return _type;
    }
    
    bool isEmpty(){
        return _empty;
    }

    Ogre::Vector3 getPos() {
        return _posActual;
    }
    
    void update(Ogre::Real pTime);

private:
    Ogre::Vector3 _posActual;
    
    Ogre::SceneNode *_nodeModel;
    Ogre::Entity *_entidad;
    
    string _id;
    bool _empty;
    unsigned int _value;
    int _type;

};
typedef shared_ptr<Item> ItemPtr;
#endif
