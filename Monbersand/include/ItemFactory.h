/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#ifndef ITEMFACTORY_H
#define ITEMFACTORY_H

#include "Item.h"

class ItemFactory
{
public:
    ItemFactory() : _nextId(0){}
    ItemPtr makeItemHealt(Ogre::Vector3 position);
    ItemPtr makeItemMunition(Ogre::Vector3 position);
    ItemPtr makeItemKey(Ogre::Vector3 position);

private:
    int _nextId;


};

#endif // ITEMFACTORY_H
