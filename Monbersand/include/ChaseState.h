/*
 * ChaseState.h
 *
 *  Created on: Jun 9, 2014
 *      Author: julio
 */

#ifndef CHASESTATE_H_
#define CHASESTATE_H_

#include "State.h"

class ChaseState: public State {

public:
	ChaseState() {
	}
	~ChaseState() {
	}
	static std::shared_ptr<ChaseState> GetInstance();
private:
	void handleState(Enemy * enemyPtr, PlayState *playStatePtr);

};

#endif /* CHASESTATE_H_ */
