/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/


#ifndef GameManager_H
#define GameManager_H

#include <stack>
#include <Ogre.h>
#include <OgreSingleton.h>
#include <OIS/OIS.h>

#include "InputManager.h"
#include "SoundFXManager.h"
#include "TrackManager.h"

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

class GameState;

class GameManager : public Ogre::FrameListener, public Ogre::Singleton<GameManager>, public OIS::KeyListener, public OIS::MouseListener {
public:
    GameManager();
    ~GameManager(); // Limpieza de todos los estados.

    // Para el estado inicial.
    void start(GameState* state);

    // Funcionalidad para transiciones de estados.
    void changeState(GameState* state);
    void pushState(GameState* state);
    void popState();

    InputManager* getInputManager() {
        return _inputMgr;
    }

    // Heredados de Ogre::Singleton.
    static GameManager& getSingleton();
    static GameManager* getSingletonPtr();

    bool initSDL();

    //Campos de configuracion

    SoundFXManager* getSoundFXManager() {
        return _pSoundFXManager;
    }

    TrackManager* getTrackManager() {
        return _pTrackManager;
    }

    void playSoundShoot();
    void playSoundHit();
    void playSoundHitPlayer();
    void playSoundClick();
    void playSoundHitEnemy();
    void playSoundPunch();
    void playSoundReload();
    void playSoundPicked();
    void playSoundKey();
    void playSoundDead();
    
    void playTrackGame();
    void pauseTrackGame();
    void stopTrackGame();
    
    void playTrackMenu();
    void pauseTrackMenu();
    void stopTrackMenu();
    
    void setVolumen(int valor);

protected:
    Ogre::Root* _root;
    Ogre::SceneManager* _sceneManager;
    Ogre::RenderWindow* _renderWindow;

    int _soundVolumen;

    // Funciones de configuración.
    void loadResources();
    void loadSounds();
    bool configure();

    // Heredados de FrameListener.
    bool frameStarted(const Ogre::FrameEvent& evt);
    bool frameEnded(const Ogre::FrameEvent& evt);

private:
    // Funciones para delegar eventos de teclado
    // y ratón en el estado actual.
    bool keyPressed(const OIS::KeyEvent &e);
    bool keyReleased(const OIS::KeyEvent &e);

    bool mouseMoved(const OIS::MouseEvent &e);
    bool mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);

    // Gestor de eventos de entrada.
    InputManager *_inputMgr;
    // Estados del juego.
    std::stack<GameState*> _states;
    // Manejadores del sonido.
    SoundFXManager* _pSoundFXManager;
    TrackManager* _pTrackManager;
    
    SoundFXPtr _soundHitEnemy;
    SoundFXPtr _soundClick;
    SoundFXPtr _soundHit;
    SoundFXPtr _soundHitPlayer;
    SoundFXPtr _soundPunch;
    SoundFXPtr _soundShoot;
    SoundFXPtr _soundReload;
    SoundFXPtr _soundPicked;
    SoundFXPtr _soundKey;
    SoundFXPtr _soundDead;
    
    TrackPtr _trackGame;
    TrackPtr _trackMenu;
};

#endif
