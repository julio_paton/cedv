/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef SCORESDATA_H_
#define SCORESDATA_H_

#include <OgreConfigFile.h>
#include <map>

class ScoresData {

public:
	static ScoresData &getInstance();

	typedef std::pair<std::string, int> pair_score;
	typedef std::vector<pair_score > vector_scores;

	struct sort_pred {
	    bool operator()(const std::pair<std::string,int> &left, const std::pair<std::string,int> &right) {
	        return left.second > right.second;
	    }
	};

	vector_scores getScores();
	void addScore(std::string name,unsigned int level, unsigned int score);

private:
	ScoresData();
	~ScoresData();
	ScoresData(const ScoresData &); //standard protection for singletons; prevent copy construction
	ScoresData& operator=(const ScoresData &); //standard protection for singletons; prevent assignment

	void extractName(std::string &name, size_t const &sepPos,
			const std::string &line) const;
	void extractScore(int& score, size_t const &sepPos,
			const std::string &line) const;
	void extractContents(const std::string &line);
	void parseLine(const std::string &line, size_t const lineNo);
	bool validLine(const std::string &line) const;

	vector_scores _scores;
	std::string _filename;
};

#endif /* SCORESDATA_H_ */
