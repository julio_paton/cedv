/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef CreditsState_H
#define CreditsState_H

#include <Ogre.h>
#include "CEGUI.h"
#include <OIS/OIS.h>

#include "GameState.h"

class CreditsState : public Ogre::Singleton<CreditsState>, public GameState {
public:

    CreditsState() {}

    void enter();
    void exit();
    void pause();
    void resume();

    void keyPressed(const OIS::KeyEvent &e);
    void keyReleased(const OIS::KeyEvent &e);

    void mouseMoved(const OIS::MouseEvent &e);
    void mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);

    bool frameStarted(const Ogre::FrameEvent& evt);
    bool frameEnded(const Ogre::FrameEvent& evt);

    bool Volver(const CEGUI::EventArgs &e);

    // Heredados de Ogre::Singleton.
    static CreditsState& getSingleton();
    static CreditsState* getSingletonPtr();

    // Miembros para widgets menu principal graficamente
    CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);
        
    void createGUI();

protected:
    Ogre::Root* _root;
    Ogre::SceneManager* _sceneMgr;
    Ogre::Viewport* _viewport;
    Ogre::Camera* _camera;

    bool _exitGame;
        
    std::string _filename;
    
};

#endif
