/*
 * DeathState.h
 *
 *  Created on: Jun 12, 2014
 *      Author: julio
 */

#ifndef DEATHSTATE_H_
#define DEATHSTATE_H_

#include "State.h"

class DeathState: public State {

public:
	DeathState() {
	}
	~DeathState() {
	}
	static std::shared_ptr<DeathState> GetInstance();
private:
	void handleState(Enemy * enemyPtr, PlayState *playStatePtr);

};

#endif /* DEATHSTATE_H_ */
