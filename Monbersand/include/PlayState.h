/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"
#include "Defines.h"
#include "Player.h"
#include "Enemy.h"
#include "EnemyFactory.h"

#include "Item.h"
#include "ItemFactory.h"

#include "Weapon.h"
#include "WeaponFactory.h"

#include "WorldCreator.h"
#include "PathFinder.h"

#include <CEGUI.h>
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>

class PlayState : public Ogre::Singleton<PlayState>, public GameState {
public:

	PlayState() : _cronoFX(nullptr), _cronoTXT(nullptr), _cronoPAUSE(nullptr), _cronoGAME(nullptr), _root(nullptr),
				_mInputDevice(nullptr), _sceneMgr(nullptr), _viewport(nullptr), _camera(nullptr), _cameraGod(nullptr), _cameraMap(nullptr),
				_raySceneQuery(nullptr), _raySceneQueryMov(nullptr), _CEGUIRenderer(nullptr), _intSceneQueryPlIt(nullptr), _cameraNode(nullptr),
				_cameraYawNode(nullptr), _cameraPitchNode(nullptr), _cameraRollNode(nullptr), _nodeCuerpo(nullptr),
				_enemiesDeads(0),  _score(0),_mousePosX(0), _mousePosY(0), _rotx(0), _roty(0), _playerOne(nullptr), _enemyDead(nullptr),_deltaT(0), _perHealt(0),
				_level(0), _lastLevel(0), _continues(0), _itemPicked(false), _keyFound(false), _blood(false),  _text(false),
				 _gameWin(false), _exitGame(false), _inPause(false) {}

    ~PlayState(){}

    void enter();
    void exit();
    void pause();
    void resume();

    void keyPressed(const OIS::KeyEvent &e);
    void keyReleased(const OIS::KeyEvent &e);

    void mouseMoved(const OIS::MouseEvent &e);
    void mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);
    CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);

    bool frameStarted(const Ogre::FrameEvent& evt);
    bool frameEnded(const Ogre::FrameEvent& evt);
    
    bool isCollision(const Ogre::Vector3& position, const Ogre::Vector3& direction, Ogre::uint32 mask, Ogre::Real distance);

    void moverCamara(Ogre::Vector3, Ogre::Radian, Ogre::Radian, Ogre::Radian);

    // Heredados de Ogre::Singleton.
    static PlayState& getSingleton();
    static PlayState* getSingletonPtr();

    void removeEnemy();
    void removeItem();
    
    void pickItem(int type,unsigned int value);
    void dropItem(int type,Ogre::Vector3 position);
    
    void anEnemyWasKilled(std::string id);

    // Cronos
    Crono *_cronoFX;
    Crono *_cronoTXT;
    Crono *_cronoPAUSE;
    Crono *_cronoGAME;
    
    std::map<int,OIS::KeyCode> _mapKeys;
    std::map<OIS::MouseButtonID, int> _mapMouse;

    //Getters&Setters

    PlayerPtr getPlayerPtr() {
        return _playerOne;
    }

    void newGame();

    Ogre::Real getDeltaT() const {
        return _deltaT;
    }

    const std::vector<EnemyPtr>& getEnemies() const {
        return _enemies;
    }

    const std::vector<ItemPtr>& getItems() const {
        return _items;
    }
    bool isGameWin(){
        return _gameWin;
    }
    Ogre::Real getGameTime(){
        return _cronoGAME->getTime();
    }
        
    int getScore(){
        return _score;
    }
    std::string getName(){
        return _name;
    }
    unsigned int getLevel(){
        return _level;
    }
    unsigned int getLastLevel(){
        return _lastLevel;
    }
    unsigned int getContinues(){
        return _continues;
    }
    void calculateScore();
    void setLevel(unsigned int level,unsigned int continues);
    void setScore(unsigned int score);
    void setName(std::string name);
    
    void hitByEnemy(unsigned int damage);

	const std::shared_ptr<PathFinder>& getPathFinder() const {
		return _pathFinder;
	}

protected:
    Ogre::Root* _root;
    InputManager* _mInputDevice;
    Ogre::SceneManager* _sceneMgr;
    Ogre::Viewport* _viewport;
    Ogre::Camera* _camera;
    Ogre::Camera* _cameraGod;
    Ogre::Camera* _cameraMap;
    Ogre::Vector3 _posInicialCamara;
    Ogre::RaySceneQuery *_raySceneQuery;
    Ogre::RaySceneQuery *_raySceneQueryMov;

    CEGUI::OgreRenderer* _CEGUIRenderer;
    Ogre::IntersectionSceneQuery *_intSceneQueryPlIt;

    Ogre::SceneNode *_cameraNode;
    Ogre::SceneNode *_cameraYawNode;
    Ogre::SceneNode *_cameraPitchNode;
    Ogre::SceneNode *_cameraRollNode;
    Ogre::SceneNode *_nodeCuerpo;

    int _enemiesDeads;
    int _score;
    int _mousePosX, _mousePosY;
    Ogre::Real _rotx;
    Ogre::Real _roty;
    
    string _levelMap;
    string _levelText;
    string _name;

    WorldCreator _worldCreator;
    //Búsqueda de caminos para los enemigos
    std::shared_ptr<PathFinder> _pathFinder;

    //Players & weapons
    PlayerPtr _playerOne;
    EnemyFactoryPtr _enemyFactory;
    ItemFactory _itemFactory;
    WeaponFactory _weaponFactory;

    EnemyPtr _enemyDead;

    std::vector<EnemyPtr> _enemies;
    std::vector<ItemPtr> _items;

    Ogre::Real _deltaT;
    void createScene();
    void createGUI();
    void initializeVars();
    void showText(string text);
    void enterInGateway();
    unsigned int calculateItem();
    // Porcentaje para la aparicion de items, vida o municion
    // a mayor porcentaje de vida, menos de municion;
    unsigned int _perHealt;
    
    unsigned int _level;
    unsigned int _lastLevel;
    unsigned int _continues;

    bool _itemPicked;
    bool _keyFound;
    bool _blood;
    bool _text;
    bool _gameWin;

    bool _exitGame;
    bool _inPause;

};

#endif
