/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef WEAPON_H
#define WEAPON_H

#include "Weapon.h"
#include <Crono.h>
#include "ExplosionsManager.h"
#include <Ogre.h>
#include <memory>

using namespace std;

class Weapon {
public:

    Weapon(Ogre::SceneNode* nodeCharacter, string id, std::string model, Ogre::Vector3 position, Ogre::Vector3 escale, Ogre::Real damage, Ogre::Real fireRate, Ogre::Real reloadTime, unsigned int magazineSize, unsigned int munition, unsigned int maxMunition);
    ~Weapon();

    string getId() {
        return _id;
    }

    Ogre::Real getDamage() {
        return _damage;
    }

    unsigned int getMunition() {
        return _munition;
    }

    unsigned int getMunitionMagazine() {
        return _munitionInMagazine;
    }
    
    void setMunition(unsigned int munition);

    bool isInReload() {
        return _inReload;
    }

    Ogre::Entity* getEntity() {
        return _entidad;
    }

    void shoot();
    
    bool isShoot(){
        return _isShoot;
    }

    void update(Ogre::Real pTime, bool shooting);

    void reloadAmmo();

private:

    int _estado;
    bool _inReload;
    bool _isShoot;
    Ogre::Real _damage;
    Ogre::Real _fireRate;
    Ogre::Real _reloadTime;

    unsigned int _magazineSize;
    unsigned int _munitionInMagazine;
    unsigned int _munition;
    unsigned int _maxMunition;
    unsigned int _bullet;

    Ogre::Entity *_entidad;
    Ogre::SceneNode *_nodeHead;
    
    ExplosionsManager *_explosionsMng;
    
    // Cronos
    Crono *_crono;

    string _id;

};
typedef shared_ptr<Weapon> WeaponPtr;
#endif
