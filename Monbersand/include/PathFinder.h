/*
 * PathFinder.h
 *
 *  Created on: Jun 25, 2014
 *      Author: julio
 */

#ifndef PATHFINDER_H_
#define PATHFINDER_H_

#include "micropather.h"
#include "Ogre.h"
#include "Enemy.h"

using namespace micropather;

class PathFinder: public Graph {
public:
	PathFinder(int mapWidth, int mapHeight, int **map);

	PathFinder(const PathFinder&);
	void operator=(const PathFinder&);
	~PathFinder();

	void clearLastSearch();
	int passable(int nx, int ny);
	int setDestinyPos(int i, int j);
	void setStartPos(int i, int j);

	void nodeToXY(void* node, int*x, int* y);

	void* XYToNode(int x, int y);

	virtual float LeastCostEstimate(void* nodeStart, void* nodeEnd);
	virtual void AdjacentCost(void* node, std::vector<StateCost> *neighbours);
	virtual void PrintStateInfo(void * node);

	void getComputedPath(std::vector<Ogre::Vector3>& computedPath);

	void setEnemiesPos(const std::vector<EnemyPtr>& enemies);

private:

	void ogrePosToMap(int i, int j, int& x, int& y);
	void mapPosToOgre(int x, int y, int& i, int& j);

	void findNewDestiny(int& nx, int& ny );
	bool isAnEnemyClose(int nx, int ny );

	std::vector<void*> _path;

	std::shared_ptr<MicroPather> _pather;

	int _mapWidth;
	int _mapHeight;
	int **_map;

	int _startX, _startY;
	std::vector<Ogre::Vector2> _enemiesPos;

};

#endif /* PATHFINDER_H_ */
