/*
 * WorldCreator.h
 *
 *  Created on: Jun 24, 2014
 *      Author: julio
 */

#ifndef WORLDCREATOR_H_
#define WORLDCREATOR_H_

#include <Ogre.h>
#include "Defines.h"

class WorldCreator {
public:
    WorldCreator();
    ~WorldCreator();

    void createNewWorld(std::string mapFile);

    void printMap();
    void printMapEsp();

    int getWordlHeight() const {
        return _matrixHeight * COEF;
    }

    int getWorldWidth() const {
        return _matrixWidth * COEF;
    }

    const Ogre::Vector3& getPosStartPlayer() const {
        return _posStartPlayer;
    }
    
    const Ogre::Vector3& getPosBoss() const {
        return _posBoss;
    }

    const Ogre::Vector3& getPosGateway() const {
        return _posGateway;
    }

    int** getMatrixEsp() const {
        return _matrixESP;
    }

    const std::vector<Ogre::Vector3>& getEnemiesPosition() const {
        return _enemiesPosition;
    }

private:
    void readMap(std::string mapFile);
    void loadWalls();
    void loadLights();

    int **_matrix;
    int **_matrixESP;
    int _matrixWidth;
    int _matrixHeight;

    Ogre::SceneManager *_scnManager;

    Ogre::Vector3 _posStartPlayer;
    Ogre::Vector3 _posGateway;
    Ogre::Vector3 _posBoss;
    Ogre::SceneNode *_nodeGateway;
    std::vector<Ogre::Vector3> _walls;
    std::vector<Ogre::Vector3> _enemiesPosition;
    std::vector<Ogre::SceneNode*> _wallsNodes;
    std::vector<Ogre::Vector3> _lights;
};

#endif /* WORLDCREATOR_H_ */
