/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#ifndef WEAPONFACTORY_H
#define WEAPONFACTORY_H

#include "Weapon.h"

class WeaponFactory
{
public:
    WeaponFactory() : _nextId(0){}
    WeaponPtr makeWeaponShotgun(Ogre::SceneNode* nodeCharacter);

private:
    int _nextId;


};

#endif // WEAPONFACTORY_H
