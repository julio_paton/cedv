/*********************************************************************
 * Juego Monbersand
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef ConfigState_H
#define ConfigState_H

#include <Ogre.h>
#include "CEGUI.h"
#include <OIS/OIS.h>

#include "GameState.h"

class ConfigState : public Ogre::Singleton<ConfigState>, public GameState {
public:

    ConfigState() : _cambioTecla(false), _exitGame(false), _foundConf(false) ,_playerName("Jugador") {}

    void enter();
    void exit();
    void pause();
    void resume();

    void keyPressed(const OIS::KeyEvent &e);
    void keyReleased(const OIS::KeyEvent &e);

    void mouseMoved(const OIS::MouseEvent &e);
    void mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);

    bool frameStarted(const Ogre::FrameEvent& evt);
    bool frameEnded(const Ogre::FrameEvent& evt);

    bool Volver(const CEGUI::EventArgs &e);
    bool Restablecer(const CEGUI::EventArgs &e);
    bool Guardar(const CEGUI::EventArgs &e);
    
    bool teclaValida(const OIS::KeyEvent &e);
    
    bool getFoundConf(){
        return _foundConf;
    }
    std::string getPlayerName(){
        return _playerName;
    }

    // Heredados de Ogre::Singleton.
    static ConfigState& getSingleton();
    static ConfigState* getSingletonPtr();

    // Miembros para widgets menu principal graficamente
    CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);
    
    int _accionActiva;
    int _volumen;
    bool _cambioTecla;
    
    void createGUI();
    
    void loadConf();
    void valorDefecto();
    void saveName(std::string name);

protected:
    Ogre::Root* _root;
    Ogre::SceneManager* _sceneMgr;
    Ogre::Viewport* _viewport;
    Ogre::Camera* _camera;

    bool _exitGame;
    bool _foundConf;
    
    std::map<OIS::KeyCode,std::string> _mapKeyName;
    
    std::string textoTeclaAccion(int action);
    
    std::string _filename;
    std::string _playerName;
    
    void extractName(const std::string &line);
    void loadKeys(const std::string &line);
    
    bool cambiarAdelante(const CEGUI::EventArgs &e);
    bool cambiarAtras(const CEGUI::EventArgs &e);
    bool cambiarIzquierda(const CEGUI::EventArgs &e);
    bool cambiarDerecha(const CEGUI::EventArgs &e);
    bool cambiarGiroIzq(const CEGUI::EventArgs &e);
    bool cambiarGiroDcha(const CEGUI::EventArgs &e);
    bool cambiarReload(const CEGUI::EventArgs &e);
    bool Volumen(const CEGUI::EventArgs &e);
};

#endif
