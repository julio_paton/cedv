#include "FinishState.h"
#include "PlayState.h"
#include "IntroState.h"

template<> FinishState* Ogre::Singleton<FinishState>::msSingleton = 0;

void FinishState::enter() {

    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->createCamera("FinishCamera");

    _root->getAutoCreatedWindow()->removeAllViewports();
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    GameManager::getSingletonPtr()->playMenusTrack();

    createCEGUI();

    _exitGame = false;
}

void FinishState::exit() {
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    _sceneMgr->destroyAllCameras();
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    GameManager::getSingletonPtr()->stopMenusTrack();
}

void FinishState::pause() {
}

void FinishState::resume() {
}

bool FinishState::frameStarted(const Ogre::FrameEvent& evt) {
    return true;
}

bool FinishState::frameEnded(const Ogre::FrameEvent& evt) {
    if (_exitGame)
        return false;

    return true;
}

void FinishState::keyPressed(const OIS::KeyEvent &e) {
    // Transición al siguiente estado.
    // Espacio --> PlayState
    //if (e.key == OIS::KC_SPACE) {
    //  changeState(PlayState::getSingletonPtr());
    //}
}

void FinishState::keyReleased(const OIS::KeyEvent &e) {
    // if (e.key == OIS::KC_ESCAPE) {
    //   _exitGame = true;
    //}
}

void FinishState::mouseMoved(const OIS::MouseEvent &e) {

    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton FinishState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void FinishState::mousePressed(const OIS::MouseEvent &e,
        OIS::MouseButtonID id) {

    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void FinishState::mouseReleased(const OIS::MouseEvent &e,
        OIS::MouseButtonID id) {

    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

FinishState*
FinishState::getSingletonPtr() {
    return msSingleton;
}

FinishState&
FinishState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void FinishState::createCEGUI() {

    _CEGUIRenderer = IntroState::getSingletonPtr()->getCEGUIRenderer();
    CEGUI::System::getSingleton().setDefaultMouseCursor("SpaceLand",
            "Raton");

    //Aqui va para agregar la puntuacion obtenida en el juego.
    // float puntuacion=PlayState::getSingletonPtr()->getPuntuacion();


    CEGUI::Window *guiRoot = CEGUI::WindowManager::getSingleton().loadWindowLayout("Fin.layout");

    CEGUI::Window *botonSalir = CEGUI::WindowManager::getSingleton().getWindow("Root/FondoFin/BotonSalir");
    botonSalir->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&FinishState::continuar, this));
    botonSalir->setAlwaysOnTop(true);
    
    CEGUI::Window *botonContinuar = CEGUI::WindowManager::getSingleton().getWindow("Root/FondoFin/Botonnuevapartida");
    botonContinuar->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&FinishState::continuar, this));
    botonContinuar->setAlwaysOnTop(true);
    //Informamos de la puntuacion
    CEGUI::Window *txtPuntuacion = CEGUI::WindowManager::getSingleton().getWindow("Root/FondoFin/puntuacion");
    //JULIO, aqui debe de dejarte de dar el error en cuanto habilites la otra
    txtPuntuacion->setText(Ogre::StringConverter::toString(PlayState::getSingleton().getPuntuacion()));
    txtPuntuacion->setAlwaysOnTop(true);

    //Al darle al boton de guardar, guardamos el nick que aparecera el estado records del principio.
    CEGUI::System::getSingleton().setGUISheet(guiRoot);

}

bool FinishState::salir(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundButtonClick();

    _exitGame = true;
    return true;
}

bool FinishState::archivar(const CEGUI::EventArgs &e) {

	GameManager::getSingletonPtr()->playSoundButtonClick();

    CEGUI::Editbox *editbox = static_cast<CEGUI::Editbox*>(CEGUI::WindowManager::getSingleton().getWindow("Root/FondoFin/Nickbox"));
    CEGUI::String nick = editbox->getText();
    std::stringstream aux;
    aux << nick.c_str();
    _nick=aux.str();

    pushState(PlayState::getSingletonPtr());

    return true;
}

bool FinishState::continuar(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundButtonClick();

    changeState(IntroState::getSingletonPtr());
    return true;
}


