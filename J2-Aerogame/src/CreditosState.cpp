/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "CreditosState.h"
#include "PlayState.h"
#include "IntroState.h"
#include "InstruccionesState.h"
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

template<> CreditosState* Ogre::Singleton<CreditosState>::msSingleton = 0;

void CreditosState::enter() {
	_root = Ogre::Root::getSingletonPtr();

	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");
	_viewport = _root->getAutoCreatedWindow()->getViewport(0);

	createCEGUI();
	_exitGame = false;
	_pag = 1;

}

void CreditosState::exit() {

	CEGUI::WindowManager::getSingleton().destroyAllWindows();

}

void CreditosState::pause() {
}

void CreditosState::resume() {

}

bool CreditosState::frameStarted(const Ogre::FrameEvent& evt) {
	return true;
}

bool CreditosState::frameEnded(const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;
	return true;
}

void CreditosState::keyPressed(const OIS::KeyEvent &e) {
}

void CreditosState::keyReleased(const OIS::KeyEvent &e) {
}

void CreditosState::mouseMoved(const OIS::MouseEvent &e) {
	CEGUI::System &sys = CEGUI::System::getSingleton();
	sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
	// Scroll wheel.
	if (e.state.Z.rel)
		sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton CreditosState::convertButton(OIS::MouseButtonID buttonID) {
	switch (buttonID) {
	case OIS::MB_Left:
		return CEGUI::LeftButton;

	case OIS::MB_Right:
		return CEGUI::RightButton;

	case OIS::MB_Middle:
		return CEGUI::MiddleButton;

	default:
		return CEGUI::LeftButton;
	}
}

void CreditosState::mousePressed(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {
	CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void CreditosState::mouseReleased(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {
	CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

CreditosState*
CreditosState::getSingletonPtr() {
	return msSingleton;
}

CreditosState&
CreditosState::getSingleton() {
	assert(msSingleton);
	return *msSingleton;
}

void CreditosState::createCEGUI() {
	_CEGUIRenderer = IntroState::getSingletonPtr()->getCEGUIRenderer();

	CEGUI::System::getSingleton().setDefaultMouseCursor("SpaceLandCreditos",
			"Raton");
	CEGUI::Window *guiRoot =
			CEGUI::WindowManager::getSingleton().loadWindowLayout(
					"Creditos.layout");

	CEGUI::Window *botonVolver = CEGUI::WindowManager::getSingleton().getWindow(
			"Root/Fondocreditos/BotonVolver");
	botonVolver->subscribeEvent(CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&CreditosState::Volver, this));
	botonVolver->setAlwaysOnTop(true);

	CEGUI::Window *botonSalir = CEGUI::WindowManager::getSingleton().getWindow(
			"Root/Fondocreditos/BotonExit");
	botonSalir->subscribeEvent(CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&CreditosState::Salir, this));

	CEGUI::Window *botonSiguiente =
			CEGUI::WindowManager::getSingleton().getWindow(
					"Root/Fondocreditos/Derecha");
	botonSiguiente->subscribeEvent(CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&CreditosState::siguiente, this));

	CEGUI::Window *botonAnterior =
			CEGUI::WindowManager::getSingleton().getWindow(
					"Root/Fondocreditos/Izquierda");
	botonAnterior->subscribeEvent(CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&CreditosState::anterior, this));

	CEGUI::Window *botonInstrucciones =
			CEGUI::WindowManager::getSingleton().getWindow(
					"Root/Fondocreditos/BotonHowtoplay");
	botonInstrucciones->subscribeEvent(CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&CreditosState::instrucciones, this));

	CEGUI::System::getSingleton().setGUISheet(guiRoot);

}

bool CreditosState::siguiente(const CEGUI::EventArgs &e) {
	if (++_pag > 3)
		_pag = 1;
	std::stringstream aux;
	aux << "set:SpaceLandCreditos image:Creditos" << _pag;
	CEGUI::DefaultWindow* staticImage =
			static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow(
					"Root/Fondocreditos/Creditos"));
	staticImage->setProperty("Image", aux.str());
	return true;

}

bool CreditosState::anterior(const CEGUI::EventArgs &e) {
	if (--_pag < 1)
		_pag = 3;
	std::stringstream aux;
	aux << "set:SpaceLandCreditos image:Creditos" << _pag;
	CEGUI::DefaultWindow* staticImage =
			static_cast<CEGUI::DefaultWindow*>(CEGUI::WindowManager::getSingleton().getWindow(
					"Root/Fondocreditos/Creditos"));
	staticImage->setProperty("Image", aux.str());
	return true;
}

bool CreditosState::instrucciones(const CEGUI::EventArgs &e) {
	pushState(InstruccionesState::getSingletonPtr());
	return true;
}

bool CreditosState::contacto(const CEGUI::EventArgs &e) {
	//pushState(SupportState::getSingletonPtr());
	return true;
}

bool CreditosState::Volver(const CEGUI::EventArgs &e) {
	GameManager::getSingletonPtr()->playSoundButtonClick();
	popState();

	return true;
}

bool CreditosState::Salir(const CEGUI::EventArgs &e) {
	GameManager::getSingletonPtr()->playSoundButtonClick();
	_exitGame = true;
	return true;
}
