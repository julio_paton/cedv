/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "Nave.h"
#include "Misil.h"
#include "Enemy.h"
#include "PlayState.h"

static const int rotorRotateSpeed = 1500;

Enemy::Enemy(int id) {

    _id = NumberToString(id);
    std::stringstream saux;
    saux << "nave" << _id;
    _id = saux.str();
    _estado = N_INICIAL;
    _maskInicial = NAVE_ENEMIGA;

    _ultimoMisil = 0;
    _cantidadMisiles = 0;
    _resistencia = 0;
    _ratioDisparo = 0.0;

    _nodeRotor1 = NULL;
    _nodeRotor2 = NULL;
    _nodeBody = NULL;

    _moveAnimation = NULL;

    std::stringstream name;
    name << "Enemy";
    name << _id;

    _name = name.str();

}

Enemy::~Enemy() {

    //	Ogre::LogManager::getSingleton().logMessage("destroy");

    //Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->getRootSceneNode()->removeAndDestroyChild(_nodeBody->getName());
    Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->destroySceneNode(_nodeBody);
    Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->destroyAnimation(
            _name);
    Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->destroyAnimationState(
            _name);


}

void Enemy::setResistencia(int resistencia) {
    _resistencia = resistencia;
}

void Enemy::setPuntos(int puntos) {
    _puntos = puntos;
}

void Enemy::setCantidadMisiles(int cantidadMisiles) {
    _cantidadMisiles = cantidadMisiles;
}

void Enemy::setRatioDisparo(Ogre::Real ratioDisparo) {
    _ratioDisparo = ratioDisparo;
}

void Enemy::cargarMisiles() {
    _misiles.reserve(_cantidadMisiles);
    std::stringstream saux;
    for (int i = 0; i < _cantidadMisiles; i++) {
        saux.str("");
        saux << _id << "m" << i;
        _misiles.push_back(Misil(_nodeBody->getPosition() + (Ogre::Vector3(0, -2, 0)), Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager"), "Misil", saux.str(), MISIL_ENEMIGO, -1, 25.0,MISIL_DIRIGIDO));
    }
}

void Enemy::paintNave(Ogre::Real deltaT) {


    _deltaT = deltaT;
    switch (_estado) {
        case N_INICIAL:
            _mask = _maskInicial;
            break;
        case N_DISPARANDO:
            _mask = _maskInicial;
            _timeSinceLastHit += deltaT;
            if (_timeSinceLastHit > _ratioDisparo) {
                _timeSinceLastHit = 0;
                dispararMisil();
            }

            break;
        case N_TOCADO:
            _mask = NAVE_INACTIVA;
            _timeSinceLastTocado += deltaT;
            if (_timeSinceLastTocado > 0.2) {
                _timeSinceLastTocado = 0;
                //cout << " en espera  " << _timeSinceLastTocado << endl;
                _estado = N_DISPARANDO;
            }
            // _entidad->getSubEntity(0)->getTechnique()->setDiffuse(1.0, 0.0, 0.0, 1.0);
            break;
        case N_MUERTA:
            // cout << " ¡¡ M U E R T O ! !  " << endl;
            _mask = NAVE_INACTIVA;
            _nodeBody->setVisible(false, true);
            for (int i = 0; i < _cantidadMisiles; i++) {
                _misiles[i].changeEstado(M_INACTIVO);
            }
            break;
    }
    for (unsigned int i = 0; i < _misiles.size(); i++) {
        _misiles[i].paintMisil(_deltaT);
    }
    _entidad->setQueryFlags(_mask);
}

void Enemy::changeEstado(int state) {
    _estado = state;
}

void Enemy::tocado(int fuerzaMisil) {
    //cout << "Nave " << _name << " Me han dado !" << endl;
    //_entidad->setQueryFlags(MISIL_INACTIVO);
    _entidad->setQueryFlags(NAVE_INACTIVA);
    //_nodeBody->get
    _resistencia -= fuerzaMisil;
    if (fuerzaMisil == -1 || _resistencia <= 0) {
        _estado = N_MUERTA;
        PlayState::getSingletonPtr()->explosion(_nodeBody->getPosition(),_puntos);
        //cout << " ¡¡ M U E R T O ! !  " << endl;
    } else {
        _estado = N_TOCADO;
    }
}

void Enemy::marcarMisil(string name) {
    for (unsigned int i = 0; i < _misiles.size(); i++) {
        if (name == _misiles[i].getId()) {
            _misiles[i].changeEstado(M_IMPACTADO);
        }
    }
}

void Enemy::dispararMisil() {
    unsigned int mActual = _ultimoMisil + 1;
    if (mActual >= _misiles.size()) {
        mActual = 0;
    }
    
    _misiles[mActual].lanzamiento(_nodeBody->getPosition() + (Ogre::Vector3(0, -3, 0)), PlayState::getSingletonPtr()->getPosNaveJugador());
    _ultimoMisil = mActual;
    PlayState::getSingletonPtr()->disparo();
}

void Enemy::setBody(std::string body) {

    Ogre::SceneManager *scnManager =
            Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");

    _entidad = scnManager->createEntity(body);
    _entidad->setQueryFlags(_mask);
    _nodeBody = scnManager->getRootSceneNode()->createChildSceneNode(_id);
    _nodeBody->attachObject(_entidad);

}

void Enemy::setRotor1(std::string rotor1, Ogre::Real x, Ogre::Real y,
        Ogre::Real z, Ogre::Vector3 rotor1Rotation) {

    Ogre::SceneManager *scnManager =
            Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");

    _rotor1Rotation = rotor1Rotation;

    Ogre::Entity* lEntityRotor1 = scnManager->createEntity(
            "Biplane_Rotor.mesh");
    lEntityRotor1->setQueryFlags(MISIL_INACTIVO);
    _nodeRotor1 = _nodeBody->createChildSceneNode();

    _nodeRotor1->attachObject(lEntityRotor1);
    _nodeRotor1->translate(x, y, z);
}

void Enemy::setRotor2(std::string rotor2, Ogre::Real x, Ogre::Real y,
        Ogre::Real z, Ogre::Vector3 rotor2Rotation) {
    Ogre::SceneManager *scnManager =
            Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");

    _rotor2Rotation = rotor2Rotation;

    Ogre::Entity* lEntityRotor2 = scnManager->createEntity(rotor2);
    lEntityRotor2->setQueryFlags(MISIL_INACTIVO);
    _nodeRotor2 = _nodeBody->createChildSceneNode();

    _nodeRotor2->attachObject(lEntityRotor2);
    _nodeRotor2->translate(x, y, z);
}

void Enemy::setAnimation(Ogre::Vector3 posIni, Ogre::Vector3 posFin, int rotIni,
        int rotFin, int time) {

    Ogre::SceneManager *scnManager =
            Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");

    Ogre::Animation *animation = scnManager->createAnimation(_name, time);
    animation->setRotationInterpolationMode(Ogre::Animation::RIM_SPHERICAL);

    Ogre::NodeAnimationTrack *animationTrack = animation->createNodeTrack(1,
            _nodeBody);

    Ogre::TransformKeyFrame *MyKeyFrame = animationTrack->createNodeKeyFrame(0);
    MyKeyFrame->setTranslate(posIni);
    MyKeyFrame->setRotation(
            Ogre::Quaternion(Ogre::Degree(rotIni), Ogre::Vector3::UNIT_Y) * Ogre::Quaternion(Ogre::Degree(rotIni), Ogre::Vector3::UNIT_Z));

    MyKeyFrame = animationTrack->createNodeKeyFrame(time);
    MyKeyFrame->setRotation(
            Ogre::Quaternion(Ogre::Degree(rotFin), Ogre::Vector3::UNIT_Y) * Ogre::Quaternion(Ogre::Degree(rotFin), Ogre::Vector3::UNIT_Z));

    MyKeyFrame->setTranslate(posFin);

    _moveAnimation = scnManager->createAnimationState(_name);
    _moveAnimation->setEnabled(true);
    _moveAnimation->setLoop(false);
}

void Enemy::animate(float deltaT) {

    //Move plane
    _moveAnimation->addTime(deltaT);
    //Move rotors
    if (_nodeRotor1) {
        _nodeRotor1->rotate(_rotor1Rotation,
                Ogre::Radian(Ogre::Degree(rotorRotateSpeed * deltaT)),
                Ogre::Node::TS_LOCAL);
    }
    if (_nodeRotor2) {
        _nodeRotor2->rotate(_rotor2Rotation,
                Ogre::Radian(Ogre::Degree(rotorRotateSpeed * deltaT)),
                Ogre::Node::TS_LOCAL);
    }
}

//Check if the plan has to disappear

bool Enemy::dispose() {
    bool dispose = false;
    //If the movement has finished (out of screen)
    if ((_moveAnimation->getTimePosition() == _moveAnimation->getLength()) || _estado == N_MUERTA) {
        _moveAnimation->setEnabled(false);
        _moveAnimation->setTimePosition(0);
        dispose = true;
    }

    //Check left power

    return dispose;
}
