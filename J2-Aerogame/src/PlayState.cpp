/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#include "PlayState.h"
#include <cstdlib>
#include <ctime>
#include <string>
#include <iostream>

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

static const int CREATION_TIME_BIPLANE = 7;
static const int CREATION_TIME_APACHE = 40;

using namespace Ogre;
using namespace std;

void PlayState::enter() {
    _root = Ogre::Root::getSingletonPtr();

    //Elimina la cámara antigua y usa la nueva
    _root->getAutoCreatedWindow()->removeViewport(0);
    _mInputDevice = GameManager::getSingletonPtr()->getInputManager();
    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");

    _camera = _sceneMgr->createCamera("PlayCamera");
    _camera->setPosition(Vector3(0, 0, 80));
    _camera->lookAt(Vector3(0, 0, 0));
    _camera->setNearClipDistance(5);
    _camera->setFarClipDistance(10000);

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _camera->setAspectRatio(
            Ogre::Real(
            _viewport->getActualWidth()
            / _viewport->getActualHeight()));

    //Creamos la escena
//    cout << "creando escena" << endl;
    createScene();

    //Inicializamos variables
//    cout << "Inicializamos variables" << endl;
    initializeVars();

    //CEGUI
	CEGUI::MouseCursor::getSingleton().hide();

    newGame();
}

void PlayState::exit() {

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    CEGUI::WindowManager::getSingleton().destroyAllWindows();

    delete _enemyFactory;
    delete _explosionsMng;
    _enemies.clear();

    _nivelTrack->stop();
	CEGUI::MouseCursor::getSingleton().show();
}

void PlayState::pause() {
    _root->getAutoCreatedWindow()->removeViewport(0);
    _root->getAutoCreatedWindow()->addViewport(
            _sceneMgr->getCamera("IntroCamera"));

    _nivelTrack->pause();
	CEGUI::MouseCursor::getSingleton().show();

}

void PlayState::resume() {

    _root->getAutoCreatedWindow()->removeViewport(0);
    _root->getAutoCreatedWindow()->addViewport(
            _sceneMgr->getCamera("PlayCamera"));

    _nivelTrack->play();
	CEGUI::MouseCursor::getSingleton().hide();

}

bool PlayState::frameStarted(const Ogre::FrameEvent& evt) {

    _deltaT = evt.timeSinceLastFrame;
    _timeSinceLastMetros += _deltaT;
    if (_timeSinceLastMetros > 0.1) {
        _timeSinceLastMetros = 0;
        _metrosRecorridos += 1;
     //   cout << "Metros: " << _metrosRecorridos << endl;
        if((_metrosRecorridos%_metrosEntreMundo)==0){
//            cout << "Metros: " << _metrosRecorridos << endl;
            cambiarMundo();
        }
    }
    if (_naveJugador != NULL) {

        _naveJugador->setDelta(_deltaT);
        if (_mInputDevice->getKeyboard()->isKeyDown(OIS::KC_UP))
            _naveJugador->adelante();
        if (_mInputDevice->getKeyboard()->isKeyDown(OIS::KC_DOWN))
            _naveJugador->atras();
        if (_mInputDevice->getKeyboard()->isKeyDown(OIS::KC_LEFT))
            _naveJugador->izquierda();
        if (_mInputDevice->getKeyboard()->isKeyDown(OIS::KC_RIGHT))
            _naveJugador->derecha();
        _naveJugador->paintNave(_deltaT);

    }

    createEnemies();
    //Gestion de los enemigos
    for (unsigned int i = 0; i < _enemies.size(); i++) {
        _enemies[i]->paintNave(_deltaT);
        _enemies[i]->animate(_deltaT);

        if (_enemies[i]->dispose()) {
            delete _enemies[i];
            _enemies.erase(_enemies.begin() + i);
        }
    }

    //Gestión de las explosiones
    _explosionsMng->finishExplosions();

    // Empezamos con las querys de colisiones
    SceneQueryMovableIntersectionList ilist;
    SceneQueryMovableIntersectionList::iterator it;
    // Primero vemos si alguna nave ha colisionado con la nuestra
    IntersectionSceneQueryResult &result = _intSceneQueryNavNe->execute();
    ilist = result.movables2movables;
    it = ilist.begin();

    if (it != ilist.end()) {
        Ogre::MovableObject* mov1 = it->first;
        Ogre::MovableObject* mov2 = it->second;
        // cout << "Chocamos " << mov1->getParentSceneNode()->getName() << " con " << mov2->getParentSceneNode()->getName() << endl;

        if (mov1->getParentSceneNode()->getName() == _naveJugador->getId()) {
            _naveJugador->tocado();
            for (unsigned int i = 0; i < _enemies.size(); i++) {
                if (mov2->getParentSceneNode()->getName()
                        == _enemies[i]->getId()) {
                    _enemies[i]->tocado(-1);
                }
            }
        } else {
            if (mov2->getParentSceneNode()->getName() == _naveJugador->getId()) {
                _naveJugador->tocado();
                for (unsigned int i = 0; i < _enemies.size(); i++) {
                    if (mov1->getParentSceneNode()->getName()
                            == _enemies[i]->getId()) {
                        _enemies[i]->tocado(-1);
                    }
                }
            }
        }
    }
    //    /////////////////////////////////////////////////////////////////////////////////
    //    // Ahora si algun misil nuestro ha impactado en alguna nave

    result = _intSceneQueryMavNe->execute();
    ilist = result.movables2movables;
    it = ilist.begin();

    if (it != ilist.end()) {
        Ogre::MovableObject* mov1 = it->first;
        Ogre::MovableObject* mov2 = it->second;
        string nombreNave = "";
        string nombreMisil = "";
        int fuerzaMisil;
        // cout << "Chocamos " << mov1->getParentSceneNode()->getName() << " con " << mov2->getParentSceneNode()->getName() << endl;
        if ((mov1->getParentSceneNode()->getName().find("nave") != std::string::npos)) {
            nombreNave = mov1->getParentSceneNode()->getName();
            nombreMisil = mov2->getParentSceneNode()->getName();
        } else {
            if ((mov2->getParentSceneNode()->getName().find("nave") != std::string::npos)) {
                nombreNave = mov1->getParentSceneNode()->getName();
                nombreMisil = mov2->getParentSceneNode()->getName();
            }
        }

        //Avisamos a la nave

        if (nombreNave != "" && nombreMisil != "") {
            //Buscamos el misil
            fuerzaMisil = _naveJugador->marcarMisil(nombreMisil);
            //Avisamos a la nave
            for (unsigned int i = 0; i < _enemies.size(); i++) {
                if (nombreNave == _enemies[i]->getId()) {
                    _enemies[i]->tocado(fuerzaMisil);
                }
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////////////
    // Por ultimo si algun misil ha impactado en nosotros
    result = _intSceneQueryNavMe->execute();
    ilist = result.movables2movables;
    it = ilist.begin();

    if (it != ilist.end()) {
        Ogre::MovableObject* mov1 = it->first;
        Ogre::MovableObject* mov2 = it->second;
        // cout << "Chocamos " << mov1->getParentSceneNode()->getName() << " con " << mov2->getParentSceneNode()->getName() << endl;
        string nombreMisil = "";
        if (mov1->getParentSceneNode()->getName() == _naveJugador->getId()) {
            nombreMisil = mov2->getParentSceneNode()->getName();
        } else {
            if (mov2->getParentSceneNode()->getName() == _naveJugador->getId()) {
                nombreMisil = mov1->getParentSceneNode()->getName();
            }
        }
        if (nombreMisil != "") {
            unsigned pos = nombreMisil.find_first_of("m");
            std::string str2 = nombreMisil.substr(0, pos);

            for (unsigned int i = 0; i < _enemies.size(); i++) {
                if (str2 == _enemies[i]->getId()) {
                    _enemies[i]->marcarMisil(nombreMisil);
                }
            }
            _naveJugador->tocado();
        }
    }
    return true;
}

bool PlayState::frameEnded(const Ogre::FrameEvent& evt) {

    if (_exitGame)
        return false;

    if (_naveJugador->isDead())
        changeState(FinishState::getSingletonPtr());
    return true;
}

void PlayState::keyPressed(const OIS::KeyEvent &e) {

    if (e.key == OIS::KC_P) {
        pushState(PauseState::getSingletonPtr());
    }

    if (_naveJugador != NULL) {
        if (e.key == OIS::KC_LEFT) {
            _naveJugador->ladeo(Ogre::Degree(-45));
        }
        if (e.key == OIS::KC_RIGHT) {
            _naveJugador->ladeo(Ogre::Degree(45));
        }
    }

}

void PlayState::keyReleased(const OIS::KeyEvent &e) {

    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
    if (_naveJugador != NULL) {
        if (e.key == OIS::KC_LEFT) {
            _naveJugador->ladeo(Ogre::Degree(45));
        }
        if (e.key == OIS::KC_RIGHT) {
            _naveJugador->ladeo(Ogre::Degree(-45));
        }
    }

}

void PlayState::mouseMoved(const OIS::MouseEvent &e) {

    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMousePosition(e.state.X.abs, e.state.Y.abs);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

void PlayState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void PlayState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {

}

PlayState*
PlayState::getSingletonPtr() {
    return msSingleton;
}

PlayState&
PlayState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void PlayState::createScene() {

    _intSceneQueryNavNe = _sceneMgr->createIntersectionQuery(NAVE_ENEMIGA | NAVE_AMIGA);
    _intSceneQueryMavNe = _sceneMgr->createIntersectionQuery(NAVE_ENEMIGA | MISIL_AMIGO);
    _intSceneQueryNavMe = _sceneMgr->createIntersectionQuery(NAVE_AMIGA | MISIL_ENEMIGO);

    Entity* groundEnt = _sceneMgr->createEntity("Mundo", "Mundo.mesh");
    SceneNode* node1 = _sceneMgr->createSceneNode();
    node1->setPosition(Vector3(0.0, 0.0, 0.0));
    node1->scale(4, 4, 1);
    groundEnt->setQueryFlags(MUNDO);
    groundEnt->setMaterialName("Tierra");
    groundEnt->setCastShadows(false);
    node1->attachObject(groundEnt);
    node1->showBoundingBox(true);

    _mundoNode = node1;

    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_MODULATIVE);
    _sceneMgr->setShadowColour(Ogre::ColourValue(0.5, 0.5, 0.5));
    _sceneMgr->setAmbientLight(Ogre::ColourValue(1, 1, 1));

    _sceneMgr->setShadowTextureCount(1);
    _sceneMgr->setShadowTextureSize(1024);

    Ogre::Light* light = _sceneMgr->createLight("Light");
    light->setPosition(10, 10, 80);
    light->setType(Ogre::Light::LT_SPOTLIGHT);
    light->setDirection(Ogre::Vector3(0, 0, -1));
    light->setSpotlightInnerAngle(Ogre::Degree(45.0f));
    light->setSpotlightOuterAngle(Ogre::Degree(60.0f));
    light->setSpotlightFalloff(5.0f);
    light->setCastShadows(true);

    _sceneMgr->getRootSceneNode()->addChild(node1);
}

void PlayState::initializeVars() {

    _naveJugador = NULL;
    _naves.clear();
    _metrosRecorridos = 0;
    _mundo=1;
    _puntuacion = 0;
    _metrosEntreMundo = 1000;
    _enemyFactory = new EnemyFactory();

    //Explosions
    _explosionsMng = new ExplosionsManager();

    //To generate random positions
    srand(time(NULL));

    //Carga de efectos de sonido
    _soundExplosion = GameManager::getSingletonPtr()->getSoundFXManager()->load(
            "explosion.ogg");
    _disparoMisil = GameManager::getSingletonPtr()->getSoundFXManager()->load(
            "disparo.ogg");
//    _disparoMisil = GameManager::getSingletonPtr()->getSoundFXManager()->load(
//            "disparo.mp3");
    _nivelTrack = GameManager::getSingletonPtr()->getTrackManager()->load(
            "airplane-ambience.ogg");
    //	_nivelTrack->setVolume(50);
    //Se reproduce y se indica que se repita
    _nivelTrack->play(5);

    _exitGame = false;

}

void PlayState::newGame() {
    _naves.push_back(Nave(Vector3(0, -28, 0), _sceneMgr, "Prota", "-1", NAVE_AMIGA));
    _naveJugador = &_naves[0];
    _mundoNode->roll(Ogre::Degree(-90));
    _mundoNode->setPosition(Ogre::Vector3(0, 0, -10));
//    Entity *ent = static_cast<Entity *> (_mundoNode->getAttachedObject("Mundo"));
//    ent->setMaterialName("Mundo1");
    cambiarMundo();
    _naveJugador->changeEstado(N_DISPARANDO);
}

void PlayState::cambiarMundo(){
    
    if(_mundo>3){
        _mundo=1;
    }
    std::stringstream saux;
    saux << "Mundo" << _mundo;
    Entity *ent = static_cast<Entity *> (_mundoNode->getAttachedObject("Mundo"));
    ent->setMaterialName(saux.str());
    _mundo++;
}

void PlayState::createEnemies() {

    //cout << "creando enemigos" << endl;
    //Biplane
    _timeSinceLastBiplane += _deltaT;
    if (_timeSinceLastBiplane > CREATION_TIME_BIPLANE) {
        _timeSinceLastBiplane = 0;
        _enemies.push_back(_enemyFactory->makeBiplane());
    }

    //Apache
    _timeSinceLastApache += _deltaT;
    if (_timeSinceLastApache > CREATION_TIME_APACHE) {
        _timeSinceLastApache = 0;
        _enemies.push_back(_enemyFactory->makeApache());
    }

}
void PlayState::disparo() {
    _disparoMisil->play();
}
Ogre::Vector3 PlayState::getPosNaveJugador() {
    if(_naveJugador!=NULL){
       return _naveJugador->getPos();
    }else{
        return Ogre::Vector3(0,0,0);
    }
}
void PlayState::explosion(const Ogre::Vector3 &v1, int puntos) {
    _puntuacion += puntos;
    _explosionsMng->createExplosion(v1);
    _soundExplosion->play();
//    cout << "\n*************************************\nHas conseguido " << puntos << " pts. \nTotal: "
//            << _puntuacion << "\nMetros Recorridos: " << _metrosRecorridos << "\n******************************" << endl;
}
