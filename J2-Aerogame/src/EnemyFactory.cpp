/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "EnemyFactory.h"

//Limits of the screen
static const int maxPos = 30;
static const int minPos = -30;

Enemy* EnemyFactory::makeBiplane() {

	Enemy *lEnemy = new Enemy(_nextId++);

	lEnemy->setBody("Biplane.mesh");
	lEnemy->setRotor1("Biplane_Rotor.mesh", 1.60125, -0.22221, -0.01413,
			Ogre::Vector3(1, 0, 0));
	int initPos = rand() % (maxPos - minPos) + minPos;
        
        lEnemy->setCantidadMisiles(4);
        lEnemy->setRatioDisparo(2.5);
        lEnemy->setResistencia(50);
        lEnemy->setPuntos(50);
        lEnemy->cargarMisiles();

	lEnemy->setAnimation(Ogre::Vector3(initPos,40 , 0),
			Ogre::Vector3(initPos,-40, 0), -90, -90, 20);
        lEnemy->changeEstado(N_DISPARANDO);
	return lEnemy;
}

Enemy* EnemyFactory::makeApache() {

	Enemy *lEnemy = new Enemy(_nextId++);

	lEnemy->setBody("Apache_Body.mesh");
	lEnemy->setRotor1("Apache_MainRotor.mesh", 0.34952, 2.76534, 0.17003,
			Ogre::Vector3(0, 1, 0));
	lEnemy->setRotor2("Apache_TailRotor.mesh", -9.00083, 2.26745, -0.19381,
				Ogre::Vector3(0, 0, 1));
	int initPos = rand() % (maxPos - minPos) + minPos;
        
        lEnemy->setCantidadMisiles(4);
        lEnemy->setRatioDisparo(3);
        lEnemy->setResistencia(200);
        lEnemy->setPuntos(500);
        lEnemy->cargarMisiles();

	lEnemy->setAnimation(Ogre::Vector3(initPos,40 , 0),
				Ogre::Vector3(initPos,-40, 0), -90, -90, 50);
        lEnemy->changeEstado(N_DISPARANDO);

	return lEnemy;
}
