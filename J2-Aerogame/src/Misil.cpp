/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "Misil.h"
#include <Ogre.h>

Misil::Misil(const Ogre::Vector3 &v1, Ogre::SceneManager *sceneMngr, string name, string id, unsigned int mask, int orientacion,Ogre::Real velocidad,int tipo) {
    _posInicial = v1;
    _posActual = _posInicial;
    _name = name;
    _id = id;
    _tipo = tipo;
    _estado = M_INACTIVO;
    _mask = MISIL_INACTIVO;
    _maskDisparado = mask;
    _velocidad = velocidad;
    _distanciaMaxima = 50;
    _fuerzaMisil = 20;
    _orientacion = orientacion;
    if(mask==MISIL_AMIGO){
        _entidad = sceneMngr->createEntity("Bullet1.mesh");
    }else{
        _entidad = sceneMngr->createEntity("Bullet2.mesh");
    }

    _entidad->setQueryFlags(_mask);
    _entidad->setCastShadows(false);
    _nodo = sceneMngr->createSceneNode(id);
    _nodo->setPosition(_posInicial);
    _nodo->scale(0.5, 0.5, 0.5);
//    _nodo->scale(1.5, 1.5, 1);
//    _nodo->pitch(Ogre::Degree(90));
    _nodo->attachObject(_entidad);
    _nodo->showBoundingBox(false);
    sceneMngr->getRootSceneNode()->addChild(_nodo);
    _nodo->setVisible(false);


}

void Misil::paintMisil(Ogre::Real deltaT) {
    switch (_estado) { 
        case M_INACTIVO:
            _mask = MISIL_INACTIVO;
            _nodo->setVisible(false);
            _posActual = _posInicial;
            break;
        case M_DISPARADO:
            _nodo->setVisible(true);
            _posInicial = _posActual;
            _estado = M_AVANZANDO;
            break;
        case M_AVANZANDO:
            if(_tipo==MISIL_NORMAL){
            _posActual += Ogre::Vector3(0, deltaT * _velocidad *_orientacion, 0);
            }else{
                if(_tipo==MISIL_DIRIGIDO){
                   _posActual += Ogre::Vector3(_vecDirigido[0]*deltaT,_vecDirigido[1]*deltaT,0); 
                }
            }
            _mask = _maskDisparado;
            break;
        case M_IMPACTADO:
            //cout << "Bien he impactado " << endl;
            _nodo->setVisible(false);
            _mask = MISIL_INACTIVO;
            _estado = M_INACTIVO;
            break;
    }
    _nodo->setPosition(_posActual);
    _entidad->setQueryFlags(_mask);
}

Misil::~Misil() {
 // Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->getRootSceneNode()->removeAndDestroyChild(_nodo->getName());
//    if(_nodo!=NULL){
//        _nodo->setVisible(false);
//        _entidad->setQueryFlags(MISIL_INACTIVO);
//    }
}

void Misil::lanzamiento(const Ogre::Vector3 &v1, const Ogre::Vector3 &vE) {
    _posActual = v1;
    if(_tipo==MISIL_DIRIGIDO){
        _vecDirigido=vE-v1;
     }
    changeEstado(M_DISPARADO);
}

void Misil::changeEstado(int state) {
    _estado = state;
}

string Misil::getName() {
    return _name;
}

string Misil::getId() {
    return _id;
}

int Misil::getFuerza() {
    return _fuerzaMisil;
}

void Misil::setFuerza(int fuerza) {
    _fuerzaMisil = fuerza;
}

Ogre::SceneNode* Misil::getNode() {
    return _nodo;
}

void Misil::setPos(const Ogre::Vector3 &v1) {
    _posActual = v1;
}


