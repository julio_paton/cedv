/*********************************************************************
 * Juego memory
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#include "IntroState.h"
#include "PlayState.h"
#include "CreditosState.h"
#include "InstruccionesState.h"
#include "SupportState.h"

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

void IntroState::enter() {
	_root = Ogre::Root::getSingletonPtr();

	if (!_alreadyInitialized) {

		_sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
		_camera = _sceneMgr->createCamera("IntroCamera");

		_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

		//llamada a los miembros para los widgets (menu principal -> entrada al juego)

		_CEGUIRenderer = &CEGUI::OgreRenderer::bootstrapSystem();

		CEGUI::Imageset::setDefaultResourceGroup("Imagesets");
		CEGUI::Font::setDefaultResourceGroup("Fonts");
		CEGUI::Scheme::setDefaultResourceGroup("Schemes");
		CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");
		CEGUI::WindowManager::setDefaultResourceGroup("Layouts");

		_alreadyInitialized = true;
	}

	else {
		_sceneMgr = _root->getSceneManager("SceneManager");
		_camera = _sceneMgr->createCamera("IntroCamera");
		_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
	}

	createCEGUI();
	GameManager::getSingletonPtr()->playMenusTrack();

	_exitGame = false;
}

void IntroState::exit() {
	_sceneMgr->clearScene();
	_root->getAutoCreatedWindow()->removeAllViewports();
	CEGUI::WindowManager::getSingleton().destroyAllWindows();
}

void IntroState::pause() {
	CEGUI::WindowManager::getSingleton().destroyAllWindows();
}

void IntroState::resume() {
	createCEGUI();
}

bool IntroState::frameStarted(const Ogre::FrameEvent& evt) {
	return true;
}

bool IntroState::frameEnded(const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void IntroState::keyPressed(const OIS::KeyEvent &e) {
	// Transición al siguiente estado.
	// Espacio --> PlayState
	// if (e.key == OIS::KC_SPACE) {
	// changeState(PlayState::getSingletonPtr());
	// }
}

void IntroState::keyReleased(const OIS::KeyEvent &e) {
	if (e.key == OIS::KC_ESCAPE) {
		_exitGame = true;
	}
}

void IntroState::mouseMoved(const OIS::MouseEvent &e) {
	CEGUI::System &sys = CEGUI::System::getSingleton();
	sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
	// Scroll wheel.
	if (e.state.Z.rel)
		sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton IntroState::convertButton(OIS::MouseButtonID buttonID) {
	switch (buttonID) {
	case OIS::MB_Left:
		return CEGUI::LeftButton;

	case OIS::MB_Right:
		return CEGUI::RightButton;

	case OIS::MB_Middle:
		return CEGUI::MiddleButton;

	default:
		return CEGUI::LeftButton;
	}
}

void IntroState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
	CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void IntroState::mouseReleased(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {
	CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

IntroState*
IntroState::getSingletonPtr() {
	return msSingleton;
}

IntroState&
IntroState::getSingleton() {
	assert(msSingleton);
	return *msSingleton;
}

void IntroState::createCEGUI() {

	CEGUI::SchemeManager::getSingleton().create("TaharezLook.scheme");
	CEGUI::SchemeManager::getSingleton().create("SpaceLand.scheme");
	CEGUI::SchemeManager::getSingleton().create("SpaceLandPause.scheme");
	CEGUI::SchemeManager::getSingleton().create("SpaceLandCreditos.scheme");
	CEGUI::SchemeManager::getSingleton().create(
			"SpaceLandInstrucciones.scheme");

	CEGUI::ImagesetManager::getSingleton().create("SpaceLand.imageset");
	CEGUI::ImagesetManager::getSingleton().create("SpaceLandPause.imageset");
	CEGUI::ImagesetManager::getSingleton().create("SpaceLandCreditos.imageset");
	CEGUI::ImagesetManager::getSingleton().create(
			"SpaceLandInstrucciones.imageset");
	CEGUI::ImagesetManager::getSingleton().create("SpaceLandSupport.imageset");

	CEGUI::ImagesetManager::getSingleton().create("SpaceLandFondo.imageset");
	CEGUI::ImagesetManager::getSingleton().create("SpaceLandFonPause.imageset");
	CEGUI::ImagesetManager::getSingleton().create("SpaceLandFonFin.imageset");
	CEGUI::ImagesetManager::getSingleton().create(
			"SpaceLandFonCreditos.imageset");
	CEGUI::ImagesetManager::getSingleton().create(
			"SpaceLandFonInstrucciones.imageset");
	CEGUI::ImagesetManager::getSingleton().create(
			"SpaceLandFonSupport.imageset");

	CEGUI::System::getSingleton().setDefaultFont("28DaysLater-15");
	CEGUI::System::getSingleton().setDefaultFont("28DaysLater-30");
	CEGUI::System::getSingleton().setDefaultMouseCursor("SpaceLand", "Raton");
	CEGUI::Window *guiRoot =
			CEGUI::WindowManager::getSingleton().loadWindowLayout(
					"Menu.layout");

	CEGUI::Window *botonPlay = CEGUI::WindowManager::getSingleton().getWindow(
			"Root/Fondo/BotonPlay");
	botonPlay->subscribeEvent(CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&IntroState::Jugar, this));
	botonPlay->setAlwaysOnTop(true);

	CEGUI::Window *botonSalir = CEGUI::WindowManager::getSingleton().getWindow(
			"Root/Fondo/BotonExit");
	botonSalir->subscribeEvent(CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&IntroState::Salir, this));
	botonSalir->setAlwaysOnTop(true);

	CEGUI::Window *botonCreditos =
			CEGUI::WindowManager::getSingleton().getWindow(
					"Root/Fondo/BotonCredits");
	botonCreditos->subscribeEvent(CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&IntroState::Creditos, this));
	botonCreditos->setAlwaysOnTop(true);

	CEGUI::Window *botonRecords =
			CEGUI::WindowManager::getSingleton().getWindow(
					"Root/Fondo/BotonRecords");
	botonRecords->subscribeEvent(CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&IntroState::Instrucciones, this));
	botonRecords->setAlwaysOnTop(true);

	CEGUI::Window *botonInstrucciones =
			CEGUI::WindowManager::getSingleton().getWindow(
					"Root/Fondo/BotonHowtoplay");
	botonInstrucciones->subscribeEvent(CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&IntroState::Instrucciones, this));
	botonInstrucciones->setAlwaysOnTop(true);

	CEGUI::System::getSingleton().setGUISheet(guiRoot);

}
//Implementacion logica de los widgets de inicio

bool IntroState::Jugar(const CEGUI::EventArgs &e) {
	GameManager::getSingletonPtr()->playSoundButtonClick();

	pushState(PlayState::getSingletonPtr());
	return true;
}

bool IntroState::Salir(const CEGUI::EventArgs &e) {
	GameManager::getSingletonPtr()->playSoundButtonClick();
	_exitGame = true;
	return true;
}

bool IntroState::Creditos(const CEGUI::EventArgs &e) {
	GameManager::getSingletonPtr()->playSoundButtonClick();
	pushState(CreditosState::getSingletonPtr());
	return true;
}

bool IntroState::Instrucciones(const CEGUI::EventArgs &e) {
	GameManager::getSingletonPtr()->playSoundButtonClick();
	pushState(InstruccionesState::getSingletonPtr());
	return true;
}

