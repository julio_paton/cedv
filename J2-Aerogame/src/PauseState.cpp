/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "PauseState.h"
#include "IntroState.h"
#include "PlayState.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

void PauseState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");
  _viewport = _root->getAutoCreatedWindow()->getViewport(0);

 createCEGUI();

  _exitGame = false;
}

void
PauseState::exit ()
{
	CEGUI::WindowManager::getSingleton().destroyAllWindows();
}

void
PauseState::pause ()
{
}

void
PauseState::resume ()
{
}

bool
PauseState::frameStarted
(const Ogre::FrameEvent& evt)
{
  return true;
}

bool
PauseState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
PauseState::keyPressed
(const OIS::KeyEvent &e) {
  // Tecla p --> Estado anterior.
  if (e.key == OIS::KC_P) {
	  popState();
	  //pushState(PauseState::getSingletonPtr());
  }
}

void
PauseState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
PauseState::mouseMoved
(const OIS::MouseEvent &e)
{
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel) sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton PauseState::convertButton(OIS::MouseButtonID buttonID)
{
    switch (buttonID)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}

void
PauseState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void
PauseState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

PauseState*
PauseState::getSingletonPtr ()
{
return msSingleton;
}

PauseState&
PauseState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}

//Definicion/implementacion de los widgets graficamente
void PauseState::createCEGUI()
{
    _CEGUIRenderer = IntroState::getSingletonPtr()->getCEGUIRenderer();

    CEGUI::System::getSingleton().setDefaultMouseCursor("SpaceLandPause", "Raton");
    CEGUI::Window *guiRoot = CEGUI::WindowManager::getSingleton().loadWindowLayout("Pause.layout");

    CEGUI::Window *botonSalirPause = CEGUI::WindowManager::getSingleton().getWindow("Root/Fondopausa/BotonExit");
    botonSalirPause->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PauseState::salir, this));
    botonSalirPause->setAlwaysOnTop(true);

    CEGUI::Window *botonVolver = CEGUI::WindowManager::getSingleton().getWindow("Root/Fondopausa/BotonVolver");
   botonVolver->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PauseState::volver, this));
   botonVolver->setAlwaysOnTop(true);

    CEGUI::System::getSingleton().setGUISheet(guiRoot);

}

bool PauseState::salir(const CEGUI::EventArgs &e)
{
    PlayState::getSingletonPtr()->salir();
    popState();
    return true;
}

bool PauseState::volver(const CEGUI::EventArgs &e)
{
    popState();
    return true;
}

