/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "SupportState.h"
#include "PlayState.h"
#include "IntroState.h"
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>


template<> SupportState* Ogre::Singleton<SupportState>::msSingleton = 0;

void
SupportState::enter ()
{
    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);

    createCEGUI();
    _exitGame=false;


}

void
SupportState::exit ()
{

    CEGUI::WindowManager::getSingleton().destroyAllWindows();

}

void
SupportState::pause()
{
  }

void
SupportState::resume()
{

}

bool
SupportState::frameStarted
(const Ogre::FrameEvent& evt)
{
    return true;
}

bool
SupportState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;
    return true;
}

void
SupportState::keyPressed
(const OIS::KeyEvent &e)
{
}

void
SupportState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
SupportState::mouseMoved
(const OIS::MouseEvent &e)
{
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel) sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton SupportState::convertButton(OIS::MouseButtonID buttonID)
{
    switch (buttonID)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}

void
SupportState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}


void
SupportState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

SupportState*
SupportState::getSingletonPtr ()
{
    return msSingleton;
}

SupportState&
SupportState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}

void SupportState::createCEGUI()
{
	_CEGUIRenderer = IntroState::getSingletonPtr()->getCEGUIRenderer();

	CEGUI::System::getSingleton().setDefaultMouseCursor("SpaceLandSupport", "Raton");
    CEGUI::Window *guiRoot = CEGUI::WindowManager::getSingleton().loadWindowLayout("Support.layout");

    CEGUI::Window *botonVolver = CEGUI::WindowManager::getSingleton().getWindow("Root/FondoSupport/BotonVolver");
    botonVolver->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&SupportState::Volver, this));

    CEGUI::Window *botonSalir = CEGUI::WindowManager::getSingleton().getWindow("Root/FondoSupport/BotonExit");
    botonSalir->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&SupportState::salir, this));


    CEGUI::System::getSingleton().setGUISheet(guiRoot);

}

bool SupportState::Volver(const CEGUI::EventArgs &e)
{
	GameManager::getSingletonPtr()->playSoundButtonClick();
	popState();

    return true;
}

bool SupportState::salir(const CEGUI::EventArgs &e)
{
	_exitGame = true;
	return true;
}
