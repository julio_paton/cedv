/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "Nave.h"
#include "Misil.h"
#include "PlayState.h"
#include <Ogre.h>

Nave::Nave(const Ogre::Vector3 &v1, Ogre::SceneManager *sceneMngr,
        string name, string id, unsigned int mask) {
    _posInicial = v1;
    _posActual = _posInicial;
    _name = name;
    std::stringstream saux;
    saux << "nave" << id;
    _id = saux.str();
    _estado = N_DISPARANDO;
    _maskInicial = NAVE_AMIGA;
    _mask = _maskInicial;
    _ultimoMisil = 0;
    _cantidadMisiles = 9;
    _vidas = 4;
    _velocidad = 20.0;
    _ratioDisparo = 0.5;
    _fuerzaMisil = 20;
//modo viejo
 //   _entidad = sceneMngr->createEntity("Biplane.mesh");
    
    //modo nuevo
    _entidad = sceneMngr->createEntity("SpaceFrigateBody", "SpaceFrigate.mesh");

    _entidad->setQueryFlags(_mask);
    _entidad->setCastShadows(true);
    _nodo = sceneMngr->createSceneNode(saux.str());
    _nodo->setPosition(v1);
    // modo viejo
    //_nodo->scale(1, 1, 1);
    //modo nuevo
    _nodo->scale(0.5, 0.5, 0.5);
    _nodo->roll(Ogre::Degree(90));
    _nodo->pitch(Ogre::Degree(90));
    _nodo->attachObject(_entidad);
    sceneMngr->getRootSceneNode()->addChild(_nodo);

    _misiles.reserve(_cantidadMisiles);
    for (int i = 0; i < _cantidadMisiles; i++) {
        saux.str("");
        saux << _id << "m" << i;
        _misiles.push_back(Misil(_posActual + (Ogre::Vector3(0, 1, 0)), sceneMngr, "Misil", saux.str(), MISIL_AMIGO, 1, 25.0,MISIL_NORMAL));
    }
}

void Nave::setPos(const Ogre::Vector3 &v1) {
    // cout << "Me cambian de posicion" << endl;
    _posActual = v1;
}

Ogre::Vector3 Nave::getPos() {
    // cout << "Me cambian de posicion" << endl;
    return _posActual;
}

void Nave::setDelta(Ogre::Real deltaT) {
    _deltaT = deltaT;
    _velocidadParaMisil = 0.0;
}

void Nave::adelante() {
    if (_posActual.y < 30) {
        _velocidadParaMisil = _velocidad * _deltaT;
        _posActual += Ogre::Vector3(0, 1 * _deltaT * _velocidad, 0);
    }
}

void Nave::atras() {
    if (_posActual.y > -30) {
        _posActual += Ogre::Vector3(0, -1 * _deltaT * _velocidad, 0);
    }
}

void Nave::izquierda() {
    if (_posActual.x > -34) {
        _posActual += Ogre::Vector3(-1 * _deltaT * _velocidad, 0, 0);
    }
}

void Nave::derecha() {
    if (_posActual.x < 34) {
        _posActual += Ogre::Vector3(1 * _deltaT * _velocidad, 0, 0);
    }
}

void Nave::paintNave(Ogre::Real deltaT) {
    switch (_estado) {
        case N_INICIAL:
            _mask = _maskInicial;
            _nodo->setVisible(true);
            _posActual = _posInicial;
            break;
        case N_DISPARANDO:
            _nodo->setVisible(true);
            _mask = _maskInicial;
            _timeSinceLastHit += deltaT;
            if (_timeSinceLastHit > _ratioDisparo) {
                _timeSinceLastHit = 0;
                dispararMisil();
            }
            break;
        case N_TOCADO:
            _mask = NAVE_INACTIVA;
            _timeDeInactividad += deltaT;
            _timeSinceLastTocado += deltaT;
            
            if (_timeDeInactividad > 1.5) { // TIEMPO EN EL QUE ESTA INACTIVO
                _timeDeInactividad = 0;
                _estado = N_DISPARANDO;
            }
            if (_timeSinceLastTocado > 0.3) {
                _timeSinceLastTocado = 0;
                _nodo->flipVisibility(true);
            }

            break;
        case N_MUERTA:
            // cout << " ¡¡ M U E R T O ! !  " << endl;
            _mask = NAVE_INACTIVA;
            _nodo->setVisible(false);
            _posActual = _posInicial;
            break;
    }
    _nodo->setPosition(_posActual);
    _entidad->setQueryFlags(_mask);

    for (unsigned int i = 0; i < _misiles.size(); i++) {
        _misiles[i].paintMisil(_deltaT);
    }
}

void Nave::ladeo(Ogre::Degree r) {
    _nodo->pitch(r);
}

Nave::~Nave() {
}

void Nave::changeEstado(int state) {
    _estado = state;
}

void Nave::tocado() {
    //cout << "Nave " << _name << " Me han dado !" << endl;
    _entidad->setQueryFlags(NAVE_INACTIVA);
    _vidas--;
        PlayState::getSingletonPtr()->explosion(_posActual,0);
    if (_vidas <= 0) {
        _estado = N_MUERTA;
//        cout << " ¡¡ M U E R T O ! !  " << _vidas << endl;
    } else {
        _posActual = _posInicial;
        _estado = N_TOCADO;
    }
}

int Nave::marcarMisil(string name) {
    int valor = 0;
    for (unsigned int i = 0; i < _misiles.size(); i++) {
        _misiles[i].paintMisil(_deltaT);
        if (name == _misiles[i].getId()) {
            _misiles[i].changeEstado(M_IMPACTADO);
            valor = _misiles[i].getFuerza();
        }
    }
    return valor;
}

void Nave::dispararMisil() {
    unsigned int mActual = _ultimoMisil + 1;
    if (mActual >= _misiles.size()) {
        mActual = 0;
    }
    _misiles[mActual].lanzamiento(_posActual + (Ogre::Vector3(0, 1 + _velocidadParaMisil, 0)), Ogre::Vector3(0, 0, 0));
    _ultimoMisil = mActual;
    PlayState::getSingletonPtr()->disparo();
}

string Nave::getName() {
    return _name;
}

string Nave::getId() {
    return _id;
}

Ogre::SceneNode* Nave::getNode() {
    return _nodo;
}

bool Nave::isDead() {
	return _estado == N_MUERTA;
}
