/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#ifndef MISIL_H
#define MISIL_H

//Estado de una nave
#define M_INACTIVO 0
#define M_DISPARADO 1
#define M_AVANZANDO 2
#define M_IMPACTADO 3
#define MISIL_NORMAL 8
#define MISIL_DIRIGIDO 9
//Mascaras para el ray query
#define MISIL_INACTIVO 1 << 4  // Mascara para las MISILS
#define MISIL_AMIGO 1 << 5  // Mascara para las MISILS 
#define MISIL_ENEMIGO 1 << 6  // Mascara para las MISILS 

#include "Misil.h"
#include "Ogre.h"

using namespace std;

class Misil {
public:
	Misil(const Ogre::Vector3 &v1, Ogre::SceneManager *sceneMngr, string name,
			string id,unsigned int mask,int orientacion,Ogre::Real velocidad,int tipo);
	~Misil();

	void paintMisil(Ogre::Real deltaT);
        void lanzamiento(const Ogre::Vector3 &v1,const Ogre::Vector3 &vE);
	string getName();
	string getId();
	int getFuerza();
	void setFuerza(int fuerza);
	void changeEstado(int state);
	void setPos(const Ogre::Vector3 &v1);
	Ogre::SceneNode* getNode();

private:
	Ogre::Vector3 _posInicial;
	Ogre::Vector3 _posActual;
	Ogre::Vector3 _vecDirigido;
	int _movAvance;
	int _estado;
        int _tipo;
	Ogre::SceneNode *_nodo;
	Ogre::Entity *_entidad;
	string _id; 
	string _name;
	unsigned int _mask;
	unsigned int _maskDisparado;
        Ogre::Real _velocidad;
        int _orientacion;
        int _distanciaMaxima;
        int _fuerzaMisil;

};

#endif
