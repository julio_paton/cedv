/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#ifndef CreditosState_H
#define CreditosState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <vector>
#include "GameState.h"


using namespace Ogre;
using namespace std;


class CreditosState : public Ogre::Singleton<CreditosState>,
    public GameState
{
public:
    CreditosState () {}

    void enter ();
    void exit ();
    void pause ();
    void resume ();

    void keyPressed (const OIS::KeyEvent &e);
    void keyReleased (const OIS::KeyEvent &e);

    void mouseMoved (const OIS::MouseEvent &e);
    void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);

    bool frameStarted (const Ogre::FrameEvent& evt);
    bool frameEnded (const Ogre::FrameEvent& evt);

    // Heredados de Ogre::Singleton.
    static CreditosState& getSingleton ();
    static CreditosState* getSingletonPtr ();

    void createCEGUI();
    bool Salir (const CEGUI::EventArgs &e);
    bool Volver(const CEGUI::EventArgs &e);
    bool siguiente(const CEGUI::EventArgs &e);
    bool anterior(const CEGUI::EventArgs &e);
    bool instrucciones (const CEGUI::EventArgs &e);
    bool contacto (const CEGUI::EventArgs &e);

private:

    //AnimationState * _animState;
    //CEGUI::OgreRenderer*_CEGUIRenderer;
    std::vector<bool> _teclado;
    int _pag;

protected:
    Ogre::Root* _root;
    Ogre::SceneManager* _sceneMgr;
    Ogre::Viewport* _viewport;
    Ogre::Camera* _camera;
    Ogre::OverlayManager* _overlayManager;
    CEGUI::OgreRenderer*_CEGUIRenderer;

    bool _exitGame;
};

#endif
