/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef ENEMY_H_
#define ENEMY_H_

#include "Nave.h"
#include "Misil.h"
#include "Ogre.h"
#include "Utils.h"

class Enemy {

public:

	Enemy(int id);
	~Enemy();

	void setBody(std::string body);

	void setRotor1(std::string rotor1, Ogre::Real x, Ogre::Real y, Ogre::Real z, Ogre::Vector3 rotor1Rotation);
	void setRotor2(std::string rotor2, Ogre::Real x, Ogre::Real y, Ogre::Real z, Ogre::Vector3 rotor2Rotation);

	void setAnimation( Ogre::Vector3 posIni,
				Ogre::Vector3 posFin, int rotIni, int rotFin, int time);
        
	void paintNave(Ogre::Real deltaT);
	void changeEstado(int state);
	void dispararMisil();
        void tocado(int fuerza);
        
        void setResistencia(int resistencia);
        void setPuntos(int puntos);
        void setCantidadMisiles(int cantidadMisiles);
        void setRatioDisparo(Ogre::Real ratioDisparo);
        void cargarMisiles();
        void marcarMisil(string name);
        
	void animate(float deltaT);
	std::string getId() {
		return _id;
	}
	bool dispose();

private:

	Ogre::SceneNode *_nodeBody;
	Ogre::SceneNode *_nodeRotor1;
	Ogre::SceneNode *_nodeRotor2;

	Ogre::Vector3 _rotor1Rotation;
	Ogre::Vector3 _rotor2Rotation;
        
        
	int _estado;
        int _puntos;
        std::vector<Misil> _misiles;
	unsigned int _mask;
	unsigned int _maskInicial;
        int _ultimoMisil;
        int _cantidadMisiles;
        int _resistencia;
        Ogre::Real _ratioDisparo;
        Ogre::Real _timeSinceLastHit;
        Ogre::Real _timeSinceLastTocado;
        Ogre::Real _deltaT;
	Ogre::Entity *_entidad;

	std::string _id;
	std::string _name;
	Ogre::AnimationState *_moveAnimation;


};

#endif /* ENEMY_H_ */
