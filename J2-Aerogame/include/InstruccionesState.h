/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#ifndef InstruccionesState_H
#define InstruccionesState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"
#include "CEGUI.h"
#include "RendererModules/Ogre/CEGUIOgreRenderer.h"

using namespace Ogre;

class InstruccionesState : public Ogre::Singleton<InstruccionesState>, public GameState
{
 public:
  InstruccionesState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static InstruccionesState& getSingleton ();
  static InstruccionesState* getSingletonPtr ();

  //Miembros para la implementacion de los widgets graficamente
   void createCEGUI();
  //Miembros para el funcionamiento logico de lo widgets

    bool Volver(const CEGUI::EventArgs &e);
    bool Salir (const CEGUI::EventArgs &e);
    bool siguiente(const CEGUI::EventArgs &e);
    bool anterior(const CEGUI::EventArgs &e);

private:

    //AnimationState * _animState;
    //CEGUI::OgreRenderer*_CEGUIRenderer;
    std::vector<bool> _teclado;
    int _pag;

protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::OverlayManager* _overlayManager;

  bool _exitGame;
};

#endif
