/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef ENEMYFACTORY_H_
#define ENEMYFACTORY_H_

#include "Enemy.h"

class EnemyFactory {
public:

	EnemyFactory() : _nextId(2) {}

	Enemy* makeBiplane();
	Enemy* makeApache();

private:
	int _nextId;
};

#endif /* ENEMYFACTORY_H_ */
