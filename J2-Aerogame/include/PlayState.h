/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"
#include "FinishState.h"
#include "PauseState.h"
#include "EnemyFactory.h"
#include "ExplosionsManager.h"
#include "Nave.h"
#include "Misil.h"
#include <CEGUI.h>
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>

#define MUNDO 1 << 0  // Mascara para el MUNDO

class PlayState: public Ogre::Singleton<PlayState>, public GameState {
public:
	PlayState() {
	}

	void enter();
	void exit();
	void pause();
	void resume();

	void keyPressed(const OIS::KeyEvent &e);
	void keyReleased(const OIS::KeyEvent &e);

	void mouseMoved(const OIS::MouseEvent &e);
	void mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
	void mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);

	bool frameStarted(const Ogre::FrameEvent& evt);
	bool frameEnded(const Ogre::FrameEvent& evt);

	// Heredados de Ogre::Singleton.
	static PlayState& getSingleton();
	static PlayState* getSingletonPtr();

	void newGame();
	void cambiarMundo();
	void disparo();
	void explosion(const Ogre::Vector3 &v1, int puntos);

	Ogre::Vector3 getPosNaveJugador();

	int getPuntuacion(){ return _puntuacion;}

	//void createCEGUI();
	void salir() {
		_exitGame = true;
	}
private:
	InputManager* _mInputDevice;
protected:
	Ogre::Root* _root;
	Ogre::SceneManager* _sceneMgr;
	Ogre::Viewport* _viewport;
	Ogre::Camera* _camera;
	Ogre::IntersectionSceneQuery *_intSceneQueryNavNe;
	Ogre::IntersectionSceneQuery *_intSceneQueryNavMe;
	Ogre::IntersectionSceneQuery *_intSceneQueryMavNe;
	Ogre::SceneNode *_mundoNode;
	Ogre::OverlayManager* _overlayManager;
	std::vector<Nave> _naves;

	Nave *_naveJugador;

	bool _exitGame;

	std::vector<Enemy*> _enemies;
	EnemyFactory *_enemyFactory;

	ExplosionsManager *_explosionsMng;
	float _deltaT;

	//Enemies creation times
	float _timeSinceLastBiplane;
	float _timeSinceLastApache;

	//Puntuacion
	float _timeSinceLastMetros;
	int _metrosRecorridos;
	int _puntuacion;
	int _mundo;
	int _metrosEntreMundo;

	//Sonidos
	SoundFXPtr _soundExplosion;
	SoundFXPtr _disparoMisil;
	TrackPtr _nivelTrack;

	void createScene();
	void createEnemies();
	void initializeVars();

};

#endif
