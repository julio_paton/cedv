/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#ifndef NAVE_H
#define NAVE_H

//Estado de una nave
#define N_INICIAL 0
#define N_DISPARANDO 1
#define N_TOCADO 2
#define N_MUERTA 3
//Mascaras para el ray query
#define NAVE_AMIGA 1 << 1  // Mascara para las NAVES enemigas
#define NAVE_ENEMIGA 1 << 2  // Mascara para las NAVES amigas
#define NAVE_INACTIVA 1 << 3  // Mascara para las NAVES amigas

#include "Nave.h"
#include "Misil.h"
#include <Ogre.h>

using namespace std;

class Nave {
public:
	Nave(const Ogre::Vector3 &v1, Ogre::SceneManager *sceneMngr, string name,
			string id,unsigned int mask);
	~Nave();
        
	void setDelta(Ogre::Real deltaT);
	void paintNave(Ogre::Real deltaT);
	void setVelocidad(Ogre::Real velocidads);
	void setPos(const Ogre::Vector3 &v1);
	void ladeo(Ogre::Degree r);
	string getName();
	string getId();
	void changeEstado(int state);
	void dispararMisil();
        void tocado();
        void adelante();
        void atras();
        void izquierda();
        void derecha();
	Ogre::SceneNode* getNode();
	Ogre::Vector3 getPos();
        int marcarMisil(string name);

        bool isDead();

	//Animaciones
//	Ogre::AnimationState* getFaceUpState();
//	Ogre::AnimationState* getFaceDownState();
//	Ogre::AnimationState* getGoToCenterState();


private:
	Ogre::Vector3 _posInicial;
	Ogre::Vector3 _posActual;
	int _estado;
	Ogre::SceneNode *_nodo;
	Ogre::Entity *_entidad;
	string _id; //0-buena 1-mala
	string _name;
        std::vector<Misil> _misiles;
	unsigned int _mask;
	unsigned int _maskInicial;
        int _vidas;
        int _ultimoMisil;
        int _cantidadMisiles;
        int _resistencia;
        int _fuerzaMisil;
        Ogre::Real _velocidad;
        Ogre::Real _velocidadParaMisil;
        Ogre::Real _ratioDisparo;
        Ogre::Real _timeSinceLastHit;
        Ogre::Real _timeSinceLastTocado;
        Ogre::Real _timeDeInactividad;
        Ogre::Real _deltaT;

//		void createAnimations();
//	Ogre::AnimationState* configureAnimation(string name,Ogre::Vector3 posIni, Ogre::Vector3 posFin, int rotIni, int rotFin);
//
//	Ogre::AnimationState *_faceUpState;
//	Ogre::AnimationState *_faceDownState;
//	Ogre::AnimationState *_goToCenterState;
};

#endif
