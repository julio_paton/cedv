/*********************************************************************
 * Juego Aerogame
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef PauseState_H
#define PauseState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

using namespace Ogre;

class PauseState : public Ogre::Singleton<PauseState>, public GameState
{
 public:
  PauseState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static PauseState& getSingleton ();
  static PauseState* getSingletonPtr ();

  //Miembros para la implementacion de los widgets graficamente
   void createCEGUI();
  //Miembros para el funcionamiento logico de lo widgets
  
    // bool volumen(const CEGUI::EventArgs &e);
    bool salir(const CEGUI::EventArgs &e);
    bool volver (const CEGUI::EventArgs &e);

private:

    //AnimationState * _animState;
    //CEGUI::OgreRenderer*_CEGUIRenderer;
    std::vector<bool> _teclado;

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;
  Ogre::OverlayManager* _overlayManager;
  CEGUI::OgreRenderer*_CEGUIRenderer;

  bool _exitGame;
};

#endif
