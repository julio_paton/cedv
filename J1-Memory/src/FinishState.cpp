#include "FinishState.h"
#include "PlayState.h"
#include "IntroState.h"

template<> FinishState* Ogre::Singleton<FinishState>::msSingleton = 0;

void FinishState::enter() {

    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->createCamera("FinishCamera");

    _root->getAutoCreatedWindow()->removeAllViewports();
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    GameManager::getSingletonPtr()->playMenusTrack();

    createCEGUI();

    _exitGame = false;
}

void FinishState::exit() {
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    _sceneMgr->destroyAllCameras();
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    GameManager::getSingletonPtr()->stopMenusTrack();
}

void FinishState::pause() {
}

void FinishState::resume() {
}

bool FinishState::frameStarted(const Ogre::FrameEvent& evt) {
    return true;
}

bool FinishState::frameEnded(const Ogre::FrameEvent& evt) {
    if (_exitGame)
        return false;

    return true;
}

void FinishState::keyPressed(const OIS::KeyEvent &e) {
    // Transición al siguiente estado.
    // Espacio --> PlayState
    //if (e.key == OIS::KC_SPACE) {
    //  changeState(PlayState::getSingletonPtr());
    //}
}

void FinishState::keyReleased(const OIS::KeyEvent &e) {
    // if (e.key == OIS::KC_ESCAPE) {
    //   _exitGame = true;
    //}
}

void FinishState::mouseMoved(const OIS::MouseEvent &e) {

    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton FinishState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void FinishState::mousePressed(const OIS::MouseEvent &e,
        OIS::MouseButtonID id) {

    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void FinishState::mouseReleased(const OIS::MouseEvent &e,
        OIS::MouseButtonID id) {

    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

FinishState*
FinishState::getSingletonPtr() {
    return msSingleton;
}

FinishState&
FinishState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void FinishState::createCEGUI() {
    _CEGUIRenderer = IntroState::getSingletonPtr()->getCEGUIRenderer();
    CEGUI::System::getSingleton().setDefaultMouseCursor("VaultDefense",
            "Raton");

    float puntuacion=PlayState::getSingletonPtr()->getPuntuacion();
    int movimientos=PlayState::getSingletonPtr()->getMovimientos();
    int tiempo=PlayState::getSingletonPtr()->getTiempo();
    
    
    CEGUI::Window *guiRoot =
            CEGUI::WindowManager::getSingleton().loadWindowLayout("Fin.layout");
    CEGUI::Window *botonSalir = CEGUI::WindowManager::getSingleton().getWindow(
            "Fin/Menu/Salir");
    botonSalir->subscribeEvent(CEGUI::PushButton::EventClicked,
            CEGUI::Event::Subscriber(&FinishState::salir, this));
    //Informamos puntuacion
    CEGUI::Window *txtTiempo = CEGUI::WindowManager::getSingleton().getWindow(
            "Fin/Menu/Tiempo");
    CEGUI::Window *txtMovimientos = CEGUI::WindowManager::getSingleton().getWindow(
            "Fin/Menu/Movimientos");
    CEGUI::Window *txtPuntuacion = CEGUI::WindowManager::getSingleton().getWindow(
            "Fin/Menu/Puntuacion");
    
    txtPuntuacion->setText(Ogre::StringConverter::toString(puntuacion));
    txtMovimientos->setText(Ogre::StringConverter::toString(movimientos));
    txtTiempo->setText(Ogre::StringConverter::toString(tiempo));
    
    //////////////////////
    CEGUI::Window *botonVolver = CEGUI::WindowManager::getSingleton().getWindow("Fin/Menu/VolverJugar");
    botonVolver->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&FinishState::volverJugar, this));

    CEGUI::Window *botonDificil = CEGUI::WindowManager::getSingleton().getWindow("Fin/Menu/Dificil");
    botonDificil->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&FinishState::juegoDificil, this));

    CEGUI::Window *botonMedio = CEGUI::WindowManager::getSingleton().getWindow("Fin/Menu/Medio");
    botonMedio->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&FinishState::juegoMedio, this));

    CEGUI::Window *botonFacil = CEGUI::WindowManager::getSingleton().getWindow("Fin/Menu/Facil");
    botonFacil->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&FinishState::juegoMedio, this));



    CEGUI::System::getSingleton().setGUISheet(guiRoot);

}

bool FinishState::salir(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundButtonClick();

    _exitGame = true;
    return true;
}

//
//

bool FinishState::juegoMedio(const CEGUI::EventArgs &e) {
    PlayState::getSingletonPtr()->setNivel(MEDIO);
    return volverJugar(e);
}
//
//

bool FinishState::juegoFacil(const CEGUI::EventArgs &e) {
    PlayState::getSingletonPtr()->setNivel(FACIL);

    return volverJugar(e);
}
//

bool FinishState::juegoDificil(const CEGUI::EventArgs &e) {
    PlayState::getSingletonPtr()->setNivel(DIFICIL);
    return volverJugar(e);
}

bool FinishState::volverJugar(const CEGUI::EventArgs &e) {
    changeState(PlayState::getSingletonPtr());
    return true;

}


