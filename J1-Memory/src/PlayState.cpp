/*********************************************************************
 * Juego memory
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "PlayState.h"
#include "PauseState.h"
#include "FinishState.h"
#include "Ficha.h"
#include <cstdlib>
#include <ctime>

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

using namespace Ogre;
using namespace std;
std::time_t t_inicio = 0;
void PlayState::enter() {

    _root = Ogre::Root::getSingletonPtr();

    //Elimina la cámara antigua y usa la nueva
    _root->getAutoCreatedWindow()->removeViewport(0);

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");

    _camera = _sceneMgr->createCamera("PlayCamera");
    _camera->setPosition(Vector3(0, 0, 24));
    _camera->lookAt(Vector3(0, 0, 0));
    _camera->setNearClipDistance(5);
    _camera->setFarClipDistance(10000);

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    //Inicializa rayQuery
    _raySceneQuery = _sceneMgr->createRayQuery(Ray());

    //Carga de efectos de sonido
    _soundError = GameManager::getSingletonPtr()->getSoundFXManager()->load(
            "Error.wav");
    _soundAcierto = GameManager::getSingletonPtr()->getSoundFXManager()->load(
            "Acierto.wav");
    if (_nivel == NULL) {
        _nivel = FACIL;
    }

    newGame();

    _exitGame = false;
}

void PlayState::exit() {
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    _nivelTrack->stop();
}

void PlayState::pause() {

    _root->getAutoCreatedWindow()->removeViewport(0);
    _root->getAutoCreatedWindow()->addViewport(
            _sceneMgr->getCamera("IntroCamera"));

    _nivelTrack->pause();
}

void PlayState::resume() {
    _root->getAutoCreatedWindow()->removeViewport(0);
    _root->getAutoCreatedWindow()->addViewport(
            _sceneMgr->getCamera("PlayCamera"));

    _nivelTrack->play();
}

bool PlayState::frameStarted(const Ogre::FrameEvent& evt) {

    _deltaT = evt.timeSinceLastFrame;

    //Gestion de las animaciones
    for (unsigned int i = 0; i < _animations.size(); i++) {

        _animations[i]->addTime(_deltaT);

        //Cuando termina una animación
        if (_animations[i]->getTimePosition() == _animations[i]->getLength()) {
            _animations[i]->setEnabled(false);
            _animations[i]->setTimePosition(0);
            _animations.erase(_animations.begin() + i);

            // Control para parejas
            parejas();
        }
    }

    return true;
}

bool PlayState::frameEnded(const Ogre::FrameEvent& evt) {

    if (_parejasCorrectas == _parejasTotales && !_animations.size()) {
        _tiempoFinal = difftime(std::time(NULL),t_inicio);
        _puntuacion= std::floor(((100000*_nivel)/_tiempoFinal)+(600-(_nivel/_movimientos)));
        changeState(FinishState::getSingletonPtr());
    }
    if (_exitGame)
        return false;

    return true;
}

void PlayState::keyPressed(const OIS::KeyEvent &e) {

    Ogre::Vector3 vt(0, 0, 0);
    switch (e.key) {

        case OIS::KC_1:
            setNivel(FACIL);
            newGame();
            break;

        case OIS::KC_2:
            setNivel(MEDIO);
            newGame();
            break;

        case OIS::KC_3:
            setNivel(DIFICIL);
            newGame();
            break;

        case OIS::KC_UP:
            vt += Ogre::Vector3(0, 0, -1);
            break;
        case OIS::KC_DOWN:
            vt += Ogre::Vector3(0, 0, 1);
            break;
        case OIS::KC_LEFT:
            vt += Ogre::Vector3(-1, 0, 0);
            break;
        case OIS::KC_RIGHT:
            vt += Ogre::Vector3(1, 0, 0);
            break;
        case OIS::KC_P:
            pushState(PauseState::getSingletonPtr());
            break;
        default:
            break;
    }
    _camera->moveRelative(vt * _deltaT * tSpeed);
}

void PlayState::keyReleased(const OIS::KeyEvent &e) {

    switch (e.key) {
        case OIS::KC_ESCAPE:
            _exitGame = true;
            break;
        default:
            break;
    }

}

CEGUI::MouseButton PlayState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void PlayState::mouseMoved(const OIS::MouseEvent &e) {
    _mousePosX = e.state.X.abs;
    _mousePosY = e.state.Y.abs;

    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMousePosition(e.state.X.abs, e.state.Y.abs);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

void PlayState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void PlayState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
    switch (id) {
            uint32 mask;
        case OIS::MB_Left:
        {

            //Si hay animaciones en marcha no se seleccionan nuevas fichas
            if (!_animations.size()) {

                mask = FICHA_SEL; // Solo a las fichas seleccionables
                setRayQuery(_mousePosX, _mousePosY, mask);

                Ogre::RaySceneQueryResult &result = _raySceneQuery->execute();
                Ogre::RaySceneQueryResult::iterator it;
                it = result.begin();

                if (it != result.end()) {
                    //Actualizamos movimientos
                    _movimientos++;

                    int pos = getFicha((it->movable->getParentSceneNode())->getName());
                    if (pos != -1) {
                        _fichaSeleccionada = &_fichas[pos];
                        _fichaSeleccionada->changeEstado(F_SELECCIONADA);

                        //Comienza la animación
                        _fichaSeleccionada->getFaceUpState()->setEnabled(true);
                        _animations.push_back(_fichaSeleccionada->getFaceUpState());
                    }

                    if (_fichaAnterior != NULL) {
                        if ((_fichaAnterior->getName()).compare(
                                _fichaSeleccionada->getName()) == 0) {
                            _isPareja = true;
                            _fichaAnterior->changeEstado(F_EMPAREJADA);
                            _fichaSeleccionada->changeEstado(F_EMPAREJADA);
                        } else {
                            _isPareja = false;
                            _fichaAnterior->changeEstado(F_ERROR);
                            _fichaSeleccionada->changeEstado(F_ERROR);
                        }

                    } else {
                        _fichaAnterior = _fichaSeleccionada;
                    }
                }
            }
        }

            break;
        case OIS::MB_Right:

            break;
        default:
            break;
    }

}

PlayState*
PlayState::getSingletonPtr() {
    return msSingleton;
}

PlayState&
PlayState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void PlayState::setRayQuery(int posx, int posy, unsigned int mask) {

    Ogre::Ray rayMouse = _camera->getCameraToViewportRay(
            posx / float(_root->getAutoCreatedWindow()->getWidth()),
            posy / float(_root->getAutoCreatedWindow()->getHeight()));

    _raySceneQuery->setRay(rayMouse);
    _raySceneQuery->setSortByDistance(true);
    _raySceneQuery->setQueryMask(mask);

}

void PlayState::createScene() {

    Entity* groundEnt = _sceneMgr->createEntity("Mesa.mesh");
    SceneNode* node1 = _sceneMgr->createSceneNode();
    node1->setPosition(Vector3(0.0, 0.0, 0.0));
    groundEnt->setQueryFlags(MESA);
    groundEnt->setMaterialName("Madera");
    groundEnt->setCastShadows(false);
    node1->attachObject(groundEnt);

    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_MODULATIVE);
    _sceneMgr->setShadowColour(Ogre::ColourValue(0.5, 0.5, 0.5));
    _sceneMgr->setAmbientLight(Ogre::ColourValue(1, 1, 1));

    _sceneMgr->setShadowTextureCount(1);
    _sceneMgr->setShadowTextureSize(1024);
    //    _sceneMgr->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE);
    //    Light* light = _sceneMgr->createLight("Light1");
    //    light->setType(Light::LT_DIRECTIONAL);
    //    light->setDirection(Vector3(1, -1, 0));
    //    node1->attachObject(light);
    //
    Ogre::Light* light = _sceneMgr->createLight("Light");
    light->setPosition(0, 15, 20);
    light->setType(Ogre::Light::LT_SPOTLIGHT);
    light->setDirection(Ogre::Vector3(0, -1, -1));
    light->setSpotlightInnerAngle(Ogre::Degree(45.0f));
    light->setSpotlightOuterAngle(Ogre::Degree(60.0f));
    light->setSpotlightFalloff(5.0f);
    light->setCastShadows(true);

    _sceneMgr->getRootSceneNode()->addChild(node1);
}

void PlayState::newGame() {
    //Cambiamos el nivel
    //setNivel(nivel);
    //Inicializamos variables
    initializeVars();
    //Generamos las posiciones dependiendo del nivel
    fillPositions();
    //Obtenermos un acceso aleatorio a esas posiciones
    int raiz = _nivel * _nivel;
    std::vector<int> vPos;
    vPos.reserve(raiz);
    int n;
    srand(time(NULL));
    for (int i = 1; i <= raiz; i++) {
        do {
            n = rand() % raiz;
        } while (checkrep(n, vPos));
        vPos.push_back(n);
    }
    //Creamos y colocamos las fichas
    paintTablero(vPos);
    t_inicio = std::time(NULL);
}

void PlayState::setNivel(int nivel) {
    _nivel = nivel;
}

void PlayState::initializeVars() {
    //Limpiamos la escena
    _sceneMgr->clearScene();
    //Creamos la escena
    createScene();
    _parejasTotales = 0;
    _parejasCorrectas = 0;
    _movimientos = 0;
    _puntuacion = 0;
    _tiempoFinal = 0;
    _isPareja = false;
    _fichaAnterior = NULL;
    _fichaSeleccionada = NULL;
    _waitingToHide = false;
    _parejasCorrectas = 0;
    _parejasTotales = _nivel * _nivel / 2;
    _movimientos = 0;
    _tablero.clear();
    _fichas.clear();
    _animations.clear();

    switch (_nivel) {

        case FACIL:
            _nivelTrack = GameManager::getSingletonPtr()->getTrackManager()->load(
                    "NivelFacilTrack.mp3");
            break;
        case MEDIO:
            _nivelTrack = GameManager::getSingletonPtr()->getTrackManager()->load(
                    "NivelMedioTrack.mp3");
            break;
        case DIFICIL:
            _nivelTrack = GameManager::getSingletonPtr()->getTrackManager()->load(
                    "NivelDificilTrack.mp3");
    }

    _nivelTrack->setVolume(50);
    //Se reproduce y se indica que se repita
    _nivelTrack->play(2);

}

void PlayState::fillPositions() {
    _tablero.reserve(_nivel * _nivel);
    float x = 1.5;
    float y = 1.5;
    for (int i = 0; i < _nivel / 2; i++) {
        for (int j = 0; j < _nivel / 2; j++) {
            _tablero.push_back(Vector3(x, y, 0.2));
            _tablero.push_back(Vector3(x, -y, 0.2));
            _tablero.push_back(Vector3(-x, y, 0.2));
            _tablero.push_back(Vector3(-x, -y, 0.2));
            x += 3;
        }
        x = 1.5;
        y += 3;
    }
}

void PlayState::paintTablero(std::vector<int> vPos) {
    std::stringstream name;
    std::stringstream id;
    int aux = 0;
    for (unsigned int i = 0; i < vPos.size(); i++) {
        name.str("");
        id.str(""); 
        name << "fruta" << aux;
        id << "f" << i;
        _fichas.push_back(
                Ficha(_tablero[vPos[i]], _sceneMgr, name.str(), id.str()));
        aux++;
        if (aux == (_nivel * _nivel / 2)) {
            aux = 0;
        }
    }
}

bool PlayState::checkrep(int n, std::vector<int> v) {
    for (unsigned int i = 0; i < v.size(); i++)
        if (n == v[i])
            return true;
    return false;
}

int PlayState::getFicha(Ogre::String name) {
    for (unsigned int i = 0; i < _fichas.size(); i++) {
        if (_fichas[i].getId().compare(name) == 0) {
            return i;
        }
    }
    return -1;
}

void PlayState::parejas() {

    if (_fichaSeleccionada != NULL && _fichaAnterior != NULL
            && _fichaSeleccionada != _fichaAnterior) {
        //Ha terminado la animacion de emparejamiento correcto
        if (_waitingToHide) {
            _fichaAnterior->changeEstado(F_INVISIBLE);
            _fichaSeleccionada->changeEstado(F_INVISIBLE);
            _fichaSeleccionada = NULL;
            _fichaAnterior = NULL;
            _waitingToHide = false;

        } else if (_isPareja) {
            //Comienza la animacion de emparejamiento correcto
            _parejasCorrectas++;
            _waitingToHide = true;
            _fichaSeleccionada->getGoToCenterState()->setEnabled(true);
            _animations.push_back(_fichaSeleccionada->getGoToCenterState());
            _fichaAnterior->getGoToCenterState()->setEnabled(true);
            _animations.push_back(_fichaAnterior->getGoToCenterState());

            _soundAcierto->play();

        } else {
            //Comienza la animacion de emparejamiento incorrecto
            _waitingToHide = false;
            _fichaAnterior->changeEstado(F_INICIAL);
            _fichaSeleccionada->changeEstado(F_INICIAL);
            _fichaSeleccionada->getFaceDownState()->setEnabled(true);
            _animations.push_back(_fichaSeleccionada->getFaceDownState());
            _fichaAnterior->getFaceDownState()->setEnabled(true);
            _animations.push_back(_fichaAnterior->getFaceDownState());

            _soundError->play();

            _fichaSeleccionada = NULL;
            _fichaAnterior = NULL;
        }

    }
}

float PlayState::getPuntuacion() {
    return _puntuacion;
}

int PlayState::getMovimientos() {
        return _movimientos;
}

int PlayState::getTiempo() {
        return _tiempoFinal;
}

