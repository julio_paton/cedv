/*********************************************************************
 * Juego memory
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#define UNUSED_VARIABLE(x) (void)x

#include "GameManager.h"
#include "IntroState.h"
#include "PlayState.h"
#include "PauseState.h"
#include "FinishState.h"
#include "CreditosState.h"
#include "InstruccionesState.h"
#include "IntermedialState.h"

#include <iostream>

using namespace std;

int main () {

  GameManager* game = new GameManager();
  IntroState* introState = new IntroState();
  PlayState* playState = new PlayState();
  PauseState* pauseState = new PauseState();
  FinishState* finishState = new FinishState();
  IntermedialState* intermedialState = new IntermedialState();
  CreditosState* creditosState= new CreditosState();
  InstruccionesState* instruccionesState = new InstruccionesState();



  UNUSED_VARIABLE(introState);
  UNUSED_VARIABLE(playState);
  UNUSED_VARIABLE(pauseState);
  UNUSED_VARIABLE(finishState);
  UNUSED_VARIABLE(creditosState);
  UNUSED_VARIABLE(instruccionesState);
  UNUSED_VARIABLE(intermedialState);
    
  try
    {
      // Inicializa el juego y transición al primer estado.
      game->start(IntroState::getSingletonPtr());
    }
  catch (Ogre::Exception& e)
    {
      std::cerr << "Excepción detectada: " << e.getFullDescription();
    }
  
  delete game;
  
  return 0;
}
