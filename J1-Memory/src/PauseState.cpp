/*********************************************************************
 * Juego memory
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "PauseState.h"
#include "IntroState.h"
#include "PlayState.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

void PauseState::enter() {
	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");
	_viewport = _root->getAutoCreatedWindow()->getViewport(0);
	
	//llamamos al miembro de CEGUI para la interfaz (definido alfinal de la pagina)
	createCEGUI();
	// Nuevo background colour.
	//_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 1.0, 0.0));

	_exitGame = false;
}

void PauseState::exit() {
CEGUI::WindowManager::getSingleton().destroyAllWindows();
}

void PauseState::pause() {
}

void PauseState::resume() {
}

bool PauseState::frameStarted(const Ogre::FrameEvent& evt) {
	return true;
}

bool PauseState::frameEnded(const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void PauseState::keyPressed(const OIS::KeyEvent &e) {
	// Tecla p --> Estado anterior.
	if (e.key == OIS::KC_P) {
		popState();
	}
}

void PauseState::keyReleased(const OIS::KeyEvent &e) {

}

void PauseState::mouseMoved(const OIS::MouseEvent &e) {

    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel) sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton PauseState::convertButton(OIS::MouseButtonID buttonID)
{
    switch (buttonID)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}

void PauseState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {

    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void PauseState::mouseReleased(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {

    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

PauseState*
PauseState::getSingletonPtr() {
	return msSingleton;
}

PauseState&
PauseState::getSingleton() {
	assert(msSingleton);
	return *msSingleton;
}

//Definicion/implementacion de los widgets graficamente
void PauseState::createCEGUI()
{
    _CEGUIRenderer = IntroState::getSingletonPtr()->getCEGUIRenderer();
    CEGUI::System::getSingleton().setDefaultMouseCursor("VaultDefense", "Raton");

    CEGUI::Window *guiRoot = CEGUI::WindowManager::getSingleton().loadWindowLayout("Pause.layout");
    CEGUI::Window *botonSalir = CEGUI::WindowManager::getSingleton().getWindow("Pause/Menu/Salir");
    botonSalir->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PauseState::salir, this));
    CEGUI::Window *botonContinuar = CEGUI::WindowManager::getSingleton().getWindow("Pause/Menu/Continuar");
   botonContinuar->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PauseState::continuar, this));


    CEGUI::System::getSingleton().setGUISheet(guiRoot);

}

bool PauseState::salir(const CEGUI::EventArgs &e)
{
    PlayState::getSingletonPtr()->salir();
    popState();
    return true;
}

bool PauseState::continuar(const CEGUI::EventArgs &e)
{
    popState();
    return true;
}

