#include "IntermedialState.h"
#include "PlayState.h"
#include "IntroState.h"

template<> IntermedialState* Ogre::Singleton<IntermedialState>::msSingleton = 0;

void IntermedialState::enter() {

    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->createCamera("FinishCamera");

    _root->getAutoCreatedWindow()->removeAllViewports();
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    createCEGUI();

    _exitGame = false;
}

void IntermedialState::exit() {
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    _sceneMgr->destroyCamera(_sceneMgr->getCamera("FinishCamera"));
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    GameManager::getSingletonPtr()->stopMenusTrack();
}

void IntermedialState::pause() {
}

void IntermedialState::resume() {
}

bool IntermedialState::frameStarted(const Ogre::FrameEvent& evt) {
    return true;
}

bool IntermedialState::frameEnded(const Ogre::FrameEvent& evt) {
    if (_exitGame)
        return false;

    return true;
}

void IntermedialState::keyPressed(const OIS::KeyEvent &e) {

}

void IntermedialState::keyReleased(const OIS::KeyEvent &e) {
     if (e.key == OIS::KC_ESCAPE) {
       _exitGame = true;
    }
}

void IntermedialState::mouseMoved(const OIS::MouseEvent &e) {

    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton IntermedialState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void IntermedialState::mousePressed(const OIS::MouseEvent &e,
        OIS::MouseButtonID id) {

    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void IntermedialState::mouseReleased(const OIS::MouseEvent &e,
        OIS::MouseButtonID id) {

    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

IntermedialState*
IntermedialState::getSingletonPtr() {
    return msSingleton;
}

IntermedialState&
IntermedialState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void IntermedialState::createCEGUI() {
    _CEGUIRenderer = IntroState::getSingletonPtr()->getCEGUIRenderer();
    CEGUI::System::getSingleton().setDefaultMouseCursor("VaultDefense",
            "Raton");

    CEGUI::Window *guiRoot =
            CEGUI::WindowManager::getSingleton().loadWindowLayout("Intermedial.layout");
    
    //CEGUI::Window *botonVolver = CEGUI::WindowManager::getSingleton().getWindow("Intermedial/Menu/VolverJugar");
    //botonVolver->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntermedialState::volverJugar, this));

    CEGUI::Window *botonDificil = CEGUI::WindowManager::getSingleton().getWindow("Intermedial/Menu/Dificil");
    botonDificil->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntermedialState::juegoDificil, this));

    CEGUI::Window *botonMedio = CEGUI::WindowManager::getSingleton().getWindow("Intermedial/Menu/Medio");
    botonMedio->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntermedialState::juegoMedio, this));

    CEGUI::Window *botonFacil = CEGUI::WindowManager::getSingleton().getWindow("Intermedial/Menu/Facil");
    botonFacil->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntermedialState::juegoFacil, this));



    CEGUI::System::getSingleton().setGUISheet(guiRoot);

}

bool IntermedialState::salir(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundButtonClick();

    _exitGame = true;
    return true;
}

bool IntermedialState::continuar(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundButtonClick();

    changeState(PlayState::getSingletonPtr());
    return true;

}

//
//

bool IntermedialState::juegoMedio(const CEGUI::EventArgs &e) {
    PlayState::getSingletonPtr()->setNivel(MEDIO);
    return volverJugar(e);
}
//
//

bool IntermedialState::juegoFacil(const CEGUI::EventArgs &e) {
    PlayState::getSingletonPtr()->setNivel(FACIL);

    return volverJugar(e);
}
//

bool IntermedialState::juegoDificil(const CEGUI::EventArgs &e) {
    PlayState::getSingletonPtr()->setNivel(DIFICIL);
    return volverJugar(e);
}

bool IntermedialState::volverJugar(const CEGUI::EventArgs &e) {
    changeState(PlayState::getSingletonPtr());
    return true;

}


