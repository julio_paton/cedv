/*********************************************************************
 * Juego memory
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "InstruccionesState.h"
#include "IntroState.h"
#include "PlayState.h"

template<> InstruccionesState* Ogre::Singleton<InstruccionesState>::msSingleton = 0;

void InstruccionesState::enter() {

	_root = Ogre::Root::getSingletonPtr();
	// Se recupera el gestor de escena
	_sceneMgr = _root->getSceneManager("SceneManager");
	
	//llamamos al miembro de CEGUI para la interfaz (definido alfinal de la pagina)
	createCEGUI();

	_exitGame = false;
	_pag=1;
}

void InstruccionesState::exit() {

	_sceneMgr->clearScene();

	CEGUI::WindowManager::getSingleton().destroyAllWindows();
}

void InstruccionesState::pause() {

    CEGUI::WindowManager::getSingleton().destroyAllWindows();
}

void InstruccionesState::resume() {

    popState();
}

bool InstruccionesState::frameStarted(const Ogre::FrameEvent& evt) {
	return true;
}

bool InstruccionesState::frameEnded(const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void InstruccionesState::keyPressed(const OIS::KeyEvent &e) {
    CEGUI::System::getSingleton().injectKeyDown(e.key);
    CEGUI::System::getSingleton().injectChar(e.text);
	
}

void InstruccionesState::keyReleased(const OIS::KeyEvent &e) {
    CEGUI::System::getSingleton().injectKeyUp(e.key);
    if(e.key==OIS::KC_Q) pushState(PlayState::getSingletonPtr());

}

void InstruccionesState::mouseMoved(const OIS::MouseEvent &e) {

    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel) sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton InstruccionesState::convertButton(OIS::MouseButtonID buttonID)
{
    switch (buttonID)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}

void InstruccionesState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {

    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void InstruccionesState::mouseReleased(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {

    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

InstruccionesState*
InstruccionesState::getSingletonPtr() {
	return msSingleton;
}

InstruccionesState&
InstruccionesState::getSingleton() {
	assert(msSingleton);
	return *msSingleton;
}

void InstruccionesState::createCEGUI()
{

    CEGUI::Window *guiRoot = CEGUI::WindowManager::getSingleton().loadWindowLayout("instrucciones.layout");
    CEGUI::Window *botonVolver = CEGUI::WindowManager::getSingleton().getWindow("Instrucciones/Boton/volver");
    botonVolver->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&InstruccionesState::Volver, this));

    CEGUI::System::getSingleton().setGUISheet(guiRoot);

}


bool InstruccionesState::Volver(const CEGUI::EventArgs &e)
{
	GameManager::getSingletonPtr()->playSoundButtonClick();
    popState();

    return true;
}



