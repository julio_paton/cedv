/*********************************************************************
 * Juego memory
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "Ficha.h"
#include <Ogre.h>

Ficha::Ficha(const Ogre::Vector3 &v1, Ogre::SceneManager *sceneMngr,
		string name, string id) {
	_posInicial = v1;
	_posActual = _posInicial;
	_name = name;
	_id = id;
	_estado = F_INICIAL;
	_mask = FICHA_SEL;
	_entidad = sceneMngr->createEntity("ficha_frutas.mesh");
	_entidad->setQueryFlags(_mask);
	_entidad->setCastShadows(true);
	_entidad->getSubEntity(1)->setMaterialName(name);
	_nodo = sceneMngr->createSceneNode(id);
	_nodo->setPosition(v1);
	_nodo->attachObject(_entidad);
	sceneMngr->getRootSceneNode()->addChild(_nodo);

	createAnimations();
}

void Ficha::setPos(const Ogre::Vector3 &v1) {
	_posActual = v1;
}

void Ficha::paintFicha() {
	_nodo->setPosition(_posActual);
}

Ficha::~Ficha() {
}

void Ficha::changeEstado(int state) {
	_estado = state;
	_mask = FICHA_NOSEL; //Mask no seleccionable por defecto
    string material="Select"; //Por defecto
	switch (state) {
	case F_INICIAL:
		_mask = FICHA_SEL;
            material="Select";
		break;
        case F_SELECCIONADA:
		_mask = FICHA_NOSEL;
            material="Select";
		break;
	case F_EMPAREJADA:
            material="Pareja";
		break;
	case F_ERROR:
            material="NoPareja";
		break;
	case F_INVISIBLE:
		_nodo->setVisible(false);
		break;
	}
    _entidad->getSubEntity(2)->setMaterialName(material);
	_entidad->setQueryFlags(_mask);
}

string Ficha::getName() {
	return _name;
}

string Ficha::getId() {
	return _id;
}

Ogre::SceneNode* Ficha::getNode() {
	return _nodo;
}

Ogre::AnimationState* Ficha::getFaceUpState() {
	return _faceUpState;
}

Ogre::AnimationState* Ficha::getFaceDownState() {
	return _faceDownState;
}

Ogre::AnimationState* Ficha::getGoToCenterState() {
	return _goToCenterState;
}

void Ficha::createAnimations() {

	//Dar la vuelta hacia arriba
	std::stringstream faceUpName;
	faceUpName << "FaceUp";
	faceUpName << _id;

	_faceUpState = configureAnimation(faceUpName.str(),
			_posInicial + Ogre::Vector3(0, 0, 2), _posInicial, 0, 180);

	//Dar la vuelta hacia abajo
	std::stringstream faceDownName;
	faceDownName << "FaceDown";
	faceDownName << _id;

	_faceDownState = configureAnimation(faceDownName.str(),
			_posInicial + Ogre::Vector3(0, 0, 2), _posInicial, 180, 0);

	//Ir hacia el centro
	std::stringstream goToCenterName;
	goToCenterName << "GoToCenter";
	goToCenterName << _id;

	_goToCenterState = configureAnimation(goToCenterName.str(),
				_posInicial + Ogre::Vector3(0, 0, 3), Ogre::Vector3(0, 0, 3), 180, 180);
}

Ogre::AnimationState* Ficha::configureAnimation(string name, Ogre::Vector3 posIni,
		Ogre::Vector3 posFin, int rotIni, int rotFin) {

	Ogre::SceneManager *sceneMgr =
			Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");

	Ogre::Animation *animation = sceneMgr->createAnimation(name, 1);
	animation->setRotationInterpolationMode(Ogre::Animation::RIM_SPHERICAL);

	Ogre::NodeAnimationTrack *animationTrack = animation->createNodeTrack(1,
			_nodo);

	Ogre::TransformKeyFrame *MyKeyFrame = animationTrack->createNodeKeyFrame(0);
	MyKeyFrame->setTranslate(posIni);
	MyKeyFrame->setRotation(
			Ogre::Quaternion(Ogre::Degree(rotIni), Ogre::Vector3::UNIT_Y));

	MyKeyFrame = animationTrack->createNodeKeyFrame(1);
	MyKeyFrame->setRotation(
			Ogre::Quaternion(Ogre::Degree(rotFin), Ogre::Vector3::UNIT_Y));
	MyKeyFrame->setTranslate(posFin);

	Ogre::AnimationState *animState = sceneMgr->createAnimationState(name);
	animState->setEnabled(false);
	animState->setLoop(false);

	return animState;
}

