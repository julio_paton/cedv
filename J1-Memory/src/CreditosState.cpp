#include "CreditosState.h"
#include "PlayState.h"
#include "IntroState.h"
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>


template<> CreditosState* Ogre::Singleton<CreditosState>::msSingleton = 0;

void
CreditosState::enter ()
{
    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->getSceneManager("SceneManager");

    createCEGUI();
    _exitGame=false;

}

void
CreditosState::exit ()
{
    _sceneMgr->clearScene();

    CEGUI::WindowManager::getSingleton().destroyAllWindows();

}

void
CreditosState::pause()
{
    CEGUI::WindowManager::getSingleton().destroyAllWindows();

}

void
CreditosState::resume()
{
    popState();
}

bool
CreditosState::frameStarted
(const Ogre::FrameEvent& evt)
{
    return true;
}

bool
CreditosState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;
    return true;
}

void
CreditosState::keyPressed
(const OIS::KeyEvent &e)
{
//	changeState(PlayState::getSingletonPtr());
    CEGUI::System::getSingleton().injectKeyDown(e.key);
    CEGUI::System::getSingleton().injectChar(e.text);
}

void
CreditosState::keyReleased
(const OIS::KeyEvent &e)
{
    CEGUI::System::getSingleton().injectKeyUp(e.key);
    if(e.key==OIS::KC_Q) pushState(PlayState::getSingletonPtr());
}

void
CreditosState::mouseMoved
(const OIS::MouseEvent &e)
{
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel) sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton CreditosState::convertButton(OIS::MouseButtonID buttonID)
{
    switch (buttonID)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;

    case OIS::MB_Right:
        return CEGUI::RightButton;

    case OIS::MB_Middle:
        return CEGUI::MiddleButton;

    default:
        return CEGUI::LeftButton;
    }
}

void
CreditosState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}


void
CreditosState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

CreditosState*
CreditosState::getSingletonPtr ()
{
    return msSingleton;
}

CreditosState&
CreditosState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}

void CreditosState::createCEGUI()
{

    CEGUI::Window *guiRoot = CEGUI::WindowManager::getSingleton().loadWindowLayout("creditos.layout");
    CEGUI::Window *botonVolver = CEGUI::WindowManager::getSingleton().getWindow("Creditos/Boton/volver");
    botonVolver->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&CreditosState::Volver, this));

    CEGUI::System::getSingleton().setGUISheet(guiRoot);

}

bool CreditosState::Volver(const CEGUI::EventArgs &e)
{
	GameManager::getSingletonPtr()->playSoundButtonClick();
    popState();

    return true;
}
