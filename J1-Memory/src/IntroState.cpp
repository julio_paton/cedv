/*********************************************************************
 * Juego memory
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "IntroState.h"
#include "PlayState.h"
#include "InstruccionesState.h"
#include "CreditosState.h"
#include "IntermedialState.h"

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

void IntroState::enter() {
	_root = Ogre::Root::getSingletonPtr();

	_sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
	_camera = _sceneMgr->createCamera("IntroCamera");
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

	//llamada a los miembros para los widgets (menu principal -> entrada al juego)

	_CEGUIRenderer = &CEGUI::OgreRenderer::bootstrapSystem();

	CEGUI::Imageset::setDefaultResourceGroup("Imagesets");
	CEGUI::Font::setDefaultResourceGroup("Fonts");
	CEGUI::Scheme::setDefaultResourceGroup("Schemes");
	CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");
	CEGUI::WindowManager::setDefaultResourceGroup("Layouts");

	createCEGUI();
	GameManager::getSingletonPtr()->playMenusTrack();

	//Insercion simple para introduccion
	//_viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 1.0));

	_exitGame = false;
}

void IntroState::exit() {
	_sceneMgr->clearScene();
	_root->getAutoCreatedWindow()->removeAllViewports();
}

void IntroState::pause() {
	CEGUI::WindowManager::getSingleton().destroyAllWindows();
}

void IntroState::resume() {
	createCEGUI();
}

bool IntroState::frameStarted(const Ogre::FrameEvent& evt) {
	return true;
}

bool IntroState::frameEnded(const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void IntroState::keyPressed(const OIS::KeyEvent &e) {
	// Transición al siguiente estado.
	// Espacio --> PlayState
//  if (e.key == OIS::KC_SPACE) {
//    changeState(PlayState::getSingletonPtr());
//  }
}

void IntroState::keyReleased(const OIS::KeyEvent &e) {
	if (e.key == OIS::KC_ESCAPE) {
		_exitGame = true;
	}
}

void IntroState::mouseMoved(const OIS::MouseEvent &e) {
	CEGUI::System &sys = CEGUI::System::getSingleton();
	sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
	// Scroll wheel.
	if (e.state.Z.rel)
		sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton IntroState::convertButton(OIS::MouseButtonID buttonID) {
	switch (buttonID) {
	case OIS::MB_Left:
		return CEGUI::LeftButton;

	case OIS::MB_Right:
		return CEGUI::RightButton;

	case OIS::MB_Middle:
		return CEGUI::MiddleButton;

	default:
		return CEGUI::LeftButton;
	}
}

void IntroState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
	CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void IntroState::mouseReleased(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {
	CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

IntroState*
IntroState::getSingletonPtr() {
	return msSingleton;
}

IntroState&
IntroState::getSingleton() {
	assert(msSingleton);
	return *msSingleton;
}

void IntroState::createCEGUI() {

	CEGUI::SchemeManager::getSingleton().create("TaharezLook.scheme");
	CEGUI::SchemeManager::getSingleton().create("VaultDefense.scheme");

	CEGUI::ImagesetManager::getSingleton().create("VaultDefense.imageset");
	CEGUI::ImagesetManager::getSingleton().create(
			"VaultDefenseInstrucciones.imageset");
	CEGUI::ImagesetManager::getSingleton().create(
			"VaultDefenseCreditos.imageset");
	CEGUI::ImagesetManager::getSingleton().create("VaultDefenseFondo.imageset");
	CEGUI::ImagesetManager::getSingleton().create("VaultDefenseAyuda.imageset");
	//CEGUI::System::getSingleton().setDefaultFont("DejaVuSans-10");
	CEGUI::System::getSingleton().setDefaultFont("VaultDefense-15");
	CEGUI::System::getSingleton().setDefaultFont("VaultDefense-30");
	CEGUI::System::getSingleton().setDefaultMouseCursor("VaultDefense",
			"Raton");
	CEGUI::Window *guiRoot =
			CEGUI::WindowManager::getSingleton().loadWindowLayout(
					"Menu.layout");

	CEGUI::Window *botonSinglePlayer =
			CEGUI::WindowManager::getSingleton().getWindow(
					"Root/Fondo/BotonSinglePlayer");
	botonSinglePlayer->subscribeEvent(CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&IntroState::configSinglePlayer, this));

	CEGUI::Window *botonSalir = CEGUI::WindowManager::getSingleton().getWindow(
			"Root/Fondo/BotonSalir");
	botonSalir->subscribeEvent(CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&IntroState::Salir, this));

	CEGUI::Window *botonCreditos =
			CEGUI::WindowManager::getSingleton().getWindow(
					"Root/Fondo/BotonCreditos");
	botonCreditos->subscribeEvent(CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&IntroState::Creditos, this));

	CEGUI::Window *botonInstrucciones =
			CEGUI::WindowManager::getSingleton().getWindow(
					"Root/Fondo/BotonInstrucciones");
	botonInstrucciones->subscribeEvent(CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&IntroState::Instrucciones, this));

	CEGUI::System::getSingleton().setGUISheet(guiRoot);

}
//Implementacion logica de los widgets de inicio

bool IntroState::configSinglePlayer(const CEGUI::EventArgs &e) {
	GameManager::getSingletonPtr()->playSoundButtonClick();
	pushState(IntermedialState::getSingletonPtr());
	return true;
}

bool IntroState::Creditos(const CEGUI::EventArgs &e) {
	GameManager::getSingletonPtr()->playSoundButtonClick();
	pushState(CreditosState::getSingletonPtr());
	return true;
}

bool IntroState::Instrucciones(const CEGUI::EventArgs &e) {
	GameManager::getSingletonPtr()->playSoundButtonClick();
	pushState(InstruccionesState::getSingletonPtr());
	return true;
}

bool IntroState::Salir(const CEGUI::EventArgs &e) {
	GameManager::getSingletonPtr()->playSoundButtonClick();
	_exitGame = true;
	return true;
}

