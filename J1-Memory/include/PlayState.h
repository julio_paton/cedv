/*********************************************************************
 * Juego memory
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"
#include "Ficha.h"
#include <CEGUI.h>
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>

#define MESA 1 << 0  // Mascara para la mesa
//NIVELES
#define FACIL 2
#define MEDIO 4
#define DIFICIL 6 

class PlayState : public Ogre::Singleton<PlayState>, public GameState
{
 public:
  PlayState () {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static PlayState& getSingleton ();
  static PlayState* getSingletonPtr ();
  
  void newGame();
  void setNivel(int nivel);
  float getPuntuacion();
  int getMovimientos();
  int getTiempo();
  void createCEGUI();

  void salir()
  {
      _exitGame=true;
  }

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;
  Ogre::RaySceneQuery *_raySceneQuery;
  Ogre::SceneNode *_selectedNode;
  Ogre::OverlayManager* _overlayManager;
  std::vector<Ficha> _fichas;
  std::vector<Ogre::Vector3> _tablero;
  int _nivel;
  int _parejasTotales;
  int _parejasCorrectas;
  int _movimientos;
  int _tiempoFinal;
  float _puntuacion;
  Ficha *_fichaSeleccionada;
  Ficha *_fichaAnterior;

  static const Ogre::Real tSpeed = 10.0;
  Ogre::Real _deltaT;
  int _mousePosX, _mousePosY;

  void setRayQuery(int posx, int posy, unsigned int mask);
  void createOverlay();

  void initializeVars();


  void fillPositions();
  void paintTablero(std::vector<int>vPos);
  void createScene();
  void parejas();
  bool checkrep(int n, std::vector<int> vPos);
  int getFicha(Ogre::String name);

  //Animaciones
  std::vector<Ogre::AnimationState*> _animations;

  //Sonidos
  SoundFXPtr _soundError;
  SoundFXPtr _soundAcierto;

  TrackPtr _nivelTrack;

  bool _exitGame;
  bool _isPareja;
  bool _waitingToHide;

};

#endif
