/*********************************************************************
 * Juego memory
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef IntroState_H
#define IntroState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include "CEGUI.h"
#include "RendererModules/Ogre/CEGUIOgreRenderer.h"
#include "GameState.h"

class IntroState : public Ogre::Singleton<IntroState>, public GameState
{
 public:
  IntroState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static IntroState& getSingleton ();
  static IntroState* getSingletonPtr ();

  CEGUI::OgreRenderer* getCEGUIRenderer() {
        return _CEGUIRenderer;
    }

  // Miembros para widgets menu principal graficamente
  void createCEGUI();
  
  // Miembros para widgets menu pricipal logicamente
  bool configSinglePlayer(const CEGUI::EventArgs &e);
  bool Salir(const CEGUI::EventArgs &e);
  bool Creditos(const CEGUI::EventArgs &e);
  bool Instrucciones(const CEGUI::EventArgs &e);
  bool Records(const CEGUI::EventArgs &e);
 
 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;
  Ogre::OverlayManager* _overlayManager;
  CEGUI::OgreRenderer* _CEGUIRenderer;
  bool _exitGame;
};

#endif
