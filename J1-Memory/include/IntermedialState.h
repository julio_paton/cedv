/*********************************************************************
 * Juego memory
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef IntermedialState_H
#define IntermedialState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"
#include "PlayState.h"
#include <iostream>

using namespace Ogre;
using namespace std;

class IntermedialState : public Ogre::Singleton<IntermedialState>, public GameState
{
 public:
  IntermedialState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
   CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);


  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);




  // Heredados de Ogre::Singleton.
  static IntermedialState& getSingleton ();
  static IntermedialState* getSingletonPtr ();

    void createCEGUI();
    bool salir(const CEGUI::EventArgs &e);
    bool continuar(const CEGUI::EventArgs &e);
    bool volverJugar(const CEGUI::EventArgs &e);
    bool juegoDificil(const CEGUI::EventArgs &e);
    bool juegoMedio(const CEGUI::EventArgs &e);
    bool juegoFacil(const CEGUI::EventArgs &e);



private:

    //AnimationState * _animState;
    //CEGUI::OgreRenderer*_CEGUIRenderer;
    std::vector<bool> _teclado;

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;
    Ogre::OverlayManager* _overlayManager;
    CEGUI::OgreRenderer*_CEGUIRenderer;


  bool _exitGame;
};

#endif
