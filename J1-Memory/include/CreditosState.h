#ifndef CreditosState_H
#define CreditosState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <vector>
#include "GameState.h"


using namespace Ogre;
using namespace std;


class CreditosState : public Ogre::Singleton<CreditosState>,
    public GameState
{
public:
    CreditosState () {}

    void enter ();
    void exit ();
    void pause ();
    void resume ();

    void keyPressed (const OIS::KeyEvent &e);
    void keyReleased (const OIS::KeyEvent &e);

    void mouseMoved (const OIS::MouseEvent &e);
    void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);

    bool frameStarted (const Ogre::FrameEvent& evt);
    bool frameEnded (const Ogre::FrameEvent& evt);

    // Heredados de Ogre::Singleton.
    static CreditosState& getSingleton ();
    static CreditosState* getSingletonPtr ();

    void createCEGUI();
    bool configSinglePlayer(const CEGUI::EventArgs &e);
    bool Volver(const CEGUI::EventArgs &e);
private:

    //AnimationState * _animState;
    //CEGUI::OgreRenderer*_CEGUIRenderer;
    std::vector<bool> _teclado;

protected:
    Ogre::Root* _root;
    Ogre::SceneManager* _sceneMgr;
    Ogre::OverlayManager* _overlayManager;

    bool _exitGame;
};

#endif
