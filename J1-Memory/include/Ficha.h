/*********************************************************************
 * Juego memory
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#ifndef FICHA_H
#define FICHA_H

//Estado de una ficha
#define F_INICIAL 0
#define F_EMPAREJADA 1
#define F_ERROR 2
#define F_INVISIBLE 3
#define F_SELECCIONADA 4
//Mascaras para el ray query
#define FICHA_SEL 1 << 1  // Mascara para las fichas que si
#define FICHA_NOSEL 1 << 2  // Mascara para las fichas que no se pueden seleccionar

#include "Ficha.h"
#include <Ogre.h>

using namespace std;

class Ficha {
public:
	Ficha(const Ogre::Vector3 &v1, Ogre::SceneManager *sceneMngr, string name,
			string id);
	~Ficha();

	void paintFicha();
	void setPos(const Ogre::Vector3 &v1);
	string getName();
	string getId();
	void changeEstado(int state);
	Ogre::SceneNode* getNode();

	//Animaciones
	Ogre::AnimationState* getFaceUpState();
	Ogre::AnimationState* getFaceDownState();
	Ogre::AnimationState* getGoToCenterState();


private:
	Ogre::Vector3 _posInicial;
	Ogre::Vector3 _posActual;
	int _estado;
	Ogre::SceneNode *_nodo;
	Ogre::Entity *_entidad;
	string _id;
	string _name;
	unsigned int _mask;

		void createAnimations();
	Ogre::AnimationState* configureAnimation(string name,Ogre::Vector3 posIni, Ogre::Vector3 posFin, int rotIni, int rotFin);

	Ogre::AnimationState *_faceUpState;
	Ogre::AnimationState *_faceDownState;
	Ogre::AnimationState *_goToCenterState;
};

#endif
