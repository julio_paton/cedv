/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#define UNUSED_VARIABLE(x) (void)x

#include "GameManager.h"
#include "IntroState.h"
#include "SelPerState.h"
#include "PlayState.h"
#include "PauseState.h"
#include "RecordsState.h"
#include "FinishState.h"
#include "AyudaState.h"

#include <iostream>

using namespace std;

int main () {

  GameManager* game = new GameManager();
  IntroState* introState = new IntroState();
  SelPerState* selperState = new SelPerState();
  PlayState* playState = new PlayState();
  PauseState* pauseState = new PauseState();
  RecordsState* recordsState = new RecordsState();
  FinishState* finishState = new FinishState();
  AyudaState* ayudaState = new AyudaState();

  UNUSED_VARIABLE(introState);
  UNUSED_VARIABLE(selperState);
  UNUSED_VARIABLE(playState);
  UNUSED_VARIABLE(pauseState);
  UNUSED_VARIABLE(recordsState);
  UNUSED_VARIABLE(finishState);
  UNUSED_VARIABLE(ayudaState);
    
  try
    {
      // Inicializa el juego y transición al primer estado.
      game->start(IntroState::getSingletonPtr());
    }
  catch (Ogre::Exception& e)
    {
      std::cerr << "Excepción detectada: " << e.getFullDescription();
    }
  
  delete game;
  
  return 0;
}
