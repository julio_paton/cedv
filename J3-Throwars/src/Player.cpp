/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "Player.h"
#include "PlayState.h"
#include <Ogre.h>

using namespace Ogre;
using namespace std;

Player::Player(string id, string model, short typePlayer, short collidesWith,
		OgreBulletDynamics::DynamicsWorld *world, Ogre::Vector3 position,
		Ogre::Quaternion rotation, float scale, string materialName) {
	_id = id;
	_typePlayer = typePlayer;
	_vida = 1000;
	_world = world;

	//Weapon
	_shootsCount = 0;
	_weaponModel = "";
	_weaponBodyRestitution = 0;
	_weaponBodyFriction = 0;
	_weaponBodyMass = 0;
	_weaponPosInicialWeapon = Ogre::Vector3::ZERO;
	_weaponQuat = Ogre::Quaternion::IDENTITY;
	_weaponNode = NULL;
	_weaponScale = 0;

	Ogre::SceneManager *scnManager =
			Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
	_entidad = scnManager->createEntity(model);
	if (!materialName.empty())
		_entidad->setMaterialName(materialName);

	_nodeModel = scnManager->getRootSceneNode()->createChildSceneNode(_id);
	_nodeModel->attachObject(_entidad);
	_nodeModel->setScale(scale, scale, scale);

	_animations.push_back(_entidad->getAnimationState("Idle"));
	_animations.push_back(_entidad->getAnimationState("Fire"));
	_animations.push_back(_entidad->getAnimationState("Death"));

	// Default behaviour
	disableAnimations();
	_activeAnimation = _animations.at(0);
	selectAnimation(IDLE, true);

	//Create OgreBullet object

	//Escalado de las formas de colisión
	Matrix4 transf = Matrix4::IDENTITY;
	transf.setScale(_nodeModel->getScale());

	OgreBulletCollisions::StaticMeshToShapeConverter *_trimeshConverter =
			new OgreBulletCollisions::StaticMeshToShapeConverter(_entidad,
					transf);
	_bodyShape = _trimeshConverter->createConvex();
	delete _trimeshConverter;

	_rigidBody = new OgreBulletDynamics::RigidBody(id, _world,
			typePlayer, collidesWith);

	//AQUÍ SE ESTABLECE LA POSICIÓN DEL PERSONAJE!!!!! (posiciones finales?)
	_rigidBody->setShape(_nodeModel, _bodyShape, 0.6 /* Restitucion */,
			0.9 /* Friccion */, 90.0 /* Masa */,
			position /* Posicion inicial */, rotation/* Orientacion */);

	_rigidBody->setKinematicObject(true); // Esto evita que los personajes se muevan.
}

OgreBulletDynamics::RigidBody *Player::getRigidBody() const {
	return _rigidBody;
}

void Player::setRigidBody(OgreBulletDynamics::RigidBody *value) {
	_rigidBody = value;
}

void Player::selectAnimation(const int pIndex, bool pLoop) {
	_activeAnimation->setEnabled(false);

	if (pIndex < _animations.size()) {

		_activeAnimation = _animations.at(pIndex);
		_activeAnimation->setEnabled(true);
		_activeAnimation->setTimePosition(0.0);
		_activeAnimation->setLoop(pLoop);

	} else {
		// Animation doesn't exist
		// Default state
		_activeAnimation = _animations.at(0);
		_activeAnimation->setLoop(true);
	}

}

void Player::disableAnimations() {
	// Desactivate all animations
	itAnimation = _animations.begin();
	while (itAnimation != _animations.end()) {
		(*itAnimation)->setEnabled(false);
		itAnimation++;
	}

}

void Player::update(Ogre::Real pTime) {
	if (!_activeAnimation->hasEnded()) {
		_activeAnimation->addTime(pTime);
	} else {
		// Deafult behaviour. After Fire -> Idle
		if (_activeAnimation->getAnimationName() == "Fire") {
			selectAnimation(0, true);
			_activeAnimation->addTime(pTime);
			shoot();
		} else if (_activeAnimation->getAnimationName() == "Death") {
			dead();
		}
	}
}

Player::~Player() {
	delete _rigidBody;
}

void Player::dispararMisil(float force, const Ogre::Vector3 &vDirection) {
	//    changeState(P_SHOOTING);
	cout << "Estoy disparando con una fuerza de: " << force << endl;
	cout << "Direccion de: " << vDirection << endl;
}

Ogre::SceneNode* Player::getNode() {
	return _nodeModel;
}

void Player::tocado(int dano) {
	cout << "Player " << _id << " Me han dado !" << endl << "Daño: " << dano
			<< endl;
	_vida -= dano;
	if (_vida <= 0) {
		cout << "vIDA 0" << endl;
		selectAnimation(2, false);
	}
}

void Player::setWeaponOptions(string weaponModel, const float bodyRestitution,
		const float bodyFriction, const float bodyMass, const Ogre::Vector3 pos,
		const Ogre::Quaternion quat, const float scale) {

	_weaponModel = weaponModel;
	_weaponBodyRestitution = bodyRestitution;
	_weaponBodyFriction = bodyFriction;
	_weaponBodyMass = bodyMass;
	_weaponPosInicialWeapon = pos;
	_weaponQuat = quat;
	_weaponScale = scale;
}
void Player::shoot() {
	Ogre::SceneManager *scnManager =
			Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
	Entity *lEntity = scnManager->createEntity(_weaponModel);

	_weaponNode = scnManager->getRootSceneNode()->createChildSceneNode();
	_weaponNode->attachObject(lEntity);

	_weaponNode->setScale(_weaponScale, _weaponScale, _weaponScale);

	Matrix4 transf = Matrix4::IDENTITY;
	transf.setScale(_weaponNode->getScale());

	StaticMeshToShapeConverter sphereConverter = StaticMeshToShapeConverter(
			lEntity, transf);
	OgreBulletCollisions::SphereCollisionShape *lBodyShape =
			sphereConverter.createSphere();

	OgreBulletDynamics::RigidBody *lRigidBody = NULL;

	switch (_typePlayer) {
	case BulletConstants::COL_PLAYER1:
		lRigidBody = new OgreBulletDynamics::RigidBody(
				"rigidBodyWeaponPl1"
						+ Ogre::StringConverter::toString(_shootsCount++),
				_world, BulletConstants::COL_WEAPONS_PL1,
				BulletConstants::weaponsPl1CollideWith);

		break;
	case BulletConstants::COL_PLAYER2:
		lRigidBody = new OgreBulletDynamics::RigidBody(
				"rigidBodyWeaponPl2"
						+ Ogre::StringConverter::toString(_shootsCount++),
				_world, BulletConstants::COL_WEAPONS_PL2,
				BulletConstants::weaponsPl2CollideWith);

		break;
	}

	lRigidBody->setShape(_weaponNode, lBodyShape,
			_weaponBodyRestitution /* Restitucion */,
			_weaponBodyFriction /* Friccion */, _weaponBodyMass /* Masa */,
			_weaponPosInicialWeapon
					+ _nodeModel->getPosition() /* Posicion inicial */,
			_weaponQuat /* Orientacion */);

	//Hacer que el ángulo esté con valor positivo
	_angle = Math::Abs(_angle);

	cout << "angulo: " << _angle << endl;

	Vector3 velocity = Vector3::ZERO;

	//Rotar con los grados de entrada
	velocity.x = cos(Degree(_angle).valueRadians());
	velocity.y = sin(Degree(_angle).valueRadians());

	//Se cambia el sentido del disparo para el player2
	if (_typePlayer == BulletConstants::COL_PLAYER2)
		velocity.x = -velocity.x;

	//Aplicar fuerza
	velocity *= _force;

	lRigidBody->setLinearVelocity(velocity);

}

void Player::setShootConditions(float angle, float force) {
	_angle = angle;
	_force = force;
}
void Player::dead() {
	cout << "Estoy diMUERTO" << endl;
	PlayState::getSingletonPtr()->gameIsOver(_id);
}
