#include "Playerfactory.h"

using namespace Ogre;

Player* PlayerFactory::makeMageWithFireball(short player) {

	//Calcular variables que dependen de si es el jugado 1 o el 2
	Vector3 position = Vector3::ZERO;
	Quaternion rotation = Quaternion::IDENTITY;
	Vector3 weaponPosition = Vector3::ZERO;
	short collidesWith = 0;

	switch (player) {
	case BulletConstants::COL_PLAYER1:
		position = Vector3(-10, 5.15, 0);
		rotation = Ogre::Quaternion(Degree(90), Vector3::UNIT_Y);
		weaponPosition = Vector3(0.5, 0.0, 0);
		collidesWith = BulletConstants::player1CollidesWith;

		break;
	case BulletConstants::COL_PLAYER2:
		position = Vector3(10, 5.15, 0);
		rotation = Ogre::Quaternion(Degree(-90), Vector3::UNIT_Y);
		weaponPosition = Vector3(-0.5, 0, 0);
		collidesWith = BulletConstants::player2CollidesWith;
	}

	float scale = 0.1;

	Player *lPlayer = new Player(
			"Player" + Ogre::StringConverter::toString(++_nextId), "Mage.mesh",
			player, collidesWith, _world, position, rotation, scale);

	//Weapon
	lPlayer->setWeaponOptions("bolafuego.mesh", 1.0 /* Restitucion */,
			0.9 /* Friccion */, 1.0 /* Masa */,
			weaponPosition /* Posicion inicial */,
			Ogre::Quaternion::IDENTITY /* Orientacion */, 0.1);

	return lPlayer;

}

Player* PlayerFactory::makeGolemWithFireball(short player, string material) {

	//Calcular variables que dependen de si es el jugado 1 o el 2
	Vector3 position = Vector3::ZERO;
	Quaternion rotation = Quaternion::IDENTITY;
	Vector3 weaponPosition = Vector3::ZERO;
	short collidesWith = 0;

	switch (player) {
	case BulletConstants::COL_PLAYER1:
		position = Vector3(-10, 4, 0);
		rotation = Ogre::Quaternion(Degree(90), Vector3::UNIT_Y);
		weaponPosition = Vector3(0.5, 0.5, 0);
		collidesWith = BulletConstants::player1CollidesWith;

		break;
	case BulletConstants::COL_PLAYER2:
		position = Vector3(10, 4, 0);
		rotation = Ogre::Quaternion(Degree(-90), Vector3::UNIT_Y);
		weaponPosition = Vector3(-0.5, 0.5, 0);
		collidesWith = BulletConstants::player2CollidesWith;
	}

	float scale = 0.2;

	Player *lPlayer = new Player(
			"Player" + Ogre::StringConverter::toString(++_nextId), "Golem.mesh",
			player, collidesWith, _world, position, rotation, scale, material);

	//Weapon
	lPlayer->setWeaponOptions("bolafuego.mesh", 1.0 /* Restitucion */,
			0.9 /* Friccion */, 1.0 /* Masa */,
			weaponPosition /* Posicion inicial */,
			Ogre::Quaternion::IDENTITY /* Orientacion */, 0.1);

	return lPlayer;

}

Player* PlayerFactory::makeDragonWithFireball(short player) {

	//Calcular variables que dependen de si es el jugado 1 o el 2
	Vector3 position = Vector3::ZERO;
	Quaternion rotation = Quaternion::IDENTITY;
	Vector3 weaponPosition = Vector3::ZERO;
	short collidesWith = 0;

	switch (player) {
	case BulletConstants::COL_PLAYER1:
		position = Vector3(-10, 4, 0);
		rotation = Ogre::Quaternion(Degree(90), Vector3::UNIT_Y);
		weaponPosition = Vector3(0.8, 0.6, 0);
		collidesWith = BulletConstants::player1CollidesWith;

		break;
	case BulletConstants::COL_PLAYER2:
		position = Vector3(10, 4, 0);
		rotation = Ogre::Quaternion(Degree(-90), Vector3::UNIT_Y);
		weaponPosition = Vector3(-0.8, 0.6, 0);
		collidesWith = BulletConstants::player2CollidesWith;
	}

	float scale = 0.4;

	Player *lPlayer = new Player(
			"Player" + Ogre::StringConverter::toString(++_nextId), "Dragon.mesh",
			player, collidesWith, _world, position, rotation, scale);

	//Weapon
	lPlayer->setWeaponOptions("bolafuego.mesh", 1.0 /* Restitucion */,
			0.9 /* Friccion */, 1.0 /* Masa */,
			weaponPosition /* Posicion inicial */,
			Ogre::Quaternion::IDENTITY /* Orientacion */, 0.1);

	return lPlayer;

}
