/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#include "RecordsState.h"
#include "PlayState.h"
#include "IntroState.h"

template<> RecordsState* Ogre::Singleton<RecordsState>::msSingleton = 0;

void RecordsState::enter() {
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MenusCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    loadResources();

    _totalWorlds = 3;
    _world = 1;

    createGUI();

    _exitGame = false;
    loadDataWorld();

}

void RecordsState::exit() {
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void RecordsState::pause() {
}

void RecordsState::resume() {
}

bool RecordsState::frameStarted(const Ogre::FrameEvent& evt) {
    CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);
    return true;
}

bool RecordsState::frameEnded(const Ogre::FrameEvent& evt) {
    if (_exitGame)
        return false;

    return true;
}

void RecordsState::keyPressed(const OIS::KeyEvent &e) {
}

void RecordsState::keyReleased(const OIS::KeyEvent &e) {
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
}

void RecordsState::mouseMoved(const OIS::MouseEvent &e) {
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton RecordsState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void RecordsState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void RecordsState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

RecordsState* RecordsState::getSingletonPtr() {
    return msSingleton;
}

RecordsState& RecordsState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void RecordsState::createGUI() {

    //Sheet
    CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout("Records.layout");
    //Menu button
    CEGUI::WindowManager::getSingleton().getWindow("Records/LeftButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&RecordsState::RecIzq, this));
    //Menu button
    CEGUI::WindowManager::getSingleton().getWindow("Records/RightButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&RecordsState::RecDer, this));
    //Menu button
    CEGUI::WindowManager::getSingleton().getWindow("Records/MenuButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&RecordsState::VolverMenu, this));
    //Exit button
    CEGUI::WindowManager::getSingleton().getWindow("Records/ExitButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&RecordsState::Salir, this));

    CEGUI::System::getSingleton().setGUISheet(sheet);
}

bool RecordsState::Salir(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    _exitGame = true;
    return true;
}

bool RecordsState::RecIzq(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();

    if (_world > 1)
        _world--;
    else
        _world = _totalWorlds;
    loadDataWorld();

    return true;
}

bool RecordsState::RecDer(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();

    if (_world <= _totalWorlds)
        _world++;
    else
        _world = 1;
    loadDataWorld();
    return true;
}

void RecordsState::inicializarCEGUI() {

    std::stringstream aux;
    for (unsigned int i = 1; i <= 5; i++) {
        aux.str("");
        aux << "Records/" << i << "/Pos";
        CEGUI::WindowManager::getSingleton().getWindow(aux.str())->setText("");
        aux.str("");
        aux << "Records/" << i << "/Nick";
        CEGUI::WindowManager::getSingleton().getWindow(aux.str())->setText("");
        aux.str("");
        aux << "Records/" << i << "/Score";
        CEGUI::WindowManager::getSingleton().getWindow(aux.str())->setText("");
    }
}

bool RecordsState::VolverMenu(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    changeState(IntroState::getSingletonPtr());
    return true;
}

void RecordsState::loadDataWorld() {
    inicializarCEGUI();
    CEGUI::WindowManager::getSingleton().getWindow("Records/World/Number")->setText(Ogre::StringConverter::toString(_world));

    ScoresData::getInstance().setFilename(Ogre::StringConverter::toString(_world));
    if(ScoresData::getInstance().loadData()){
        std::vector<ScoresData::pair_score> lScores = ScoresData::getInstance().getScores();
        std::stringstream aux;
        std::stringstream aux2;
        for (unsigned int i = 0; i < lScores.size() && i < 5; i++) {
            cout << "Nombre:" << lScores[i].first << " Score: " << lScores[i].second << endl;
            aux.str("");
            aux << "Records/" << i + 1 << "/Pos";
            CEGUI::WindowManager::getSingleton().getWindow(aux.str())->setText(Ogre::StringConverter::toString(i + 1));
            aux.str("");
            aux << "Records/" << i + 1 << "/Nick";
            CEGUI::WindowManager::getSingleton().getWindow(aux.str())->setText(lScores[i].first);
            aux.str("");
            aux << "Records/" << i + 1 << "/Score";
            aux2.str("");
          //  aux2 << lScores[i].second << ".000";
            aux2 << lScores[i].second ;
            CEGUI::WindowManager::getSingleton().getWindow(aux.str())->setText(aux2.str());
        }
    }
}

void RecordsState::loadResources() {
    Ogre::ConfigFile cf;
    cf.load("resources.cfg");

    Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
    Ogre::String sectionstr, typestr, datastr;
    while (sI.hasMoreElements()) {
        sectionstr = sI.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i) {
            typestr = i->first;
            datastr = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation
                    (datastr, typestr, sectionstr);
        }
    }
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}
