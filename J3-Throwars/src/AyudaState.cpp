/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#include "AyudaState.h"
#include "PlayState.h"
#include "IntroState.h"

template<> AyudaState* Ogre::Singleton<AyudaState>::msSingleton = 0;

void AyudaState::enter() {
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    if (_sceneMgr->hasCamera("AyudaCamera")) {
        _camera = _sceneMgr->getCamera("AyudaCamera");
    } else {
        _camera = _sceneMgr->createCamera("AyudaCamera");
    }

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    loadResources();

    createGUI();

    _exitGame = false;

}

void AyudaState::exit() {
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void AyudaState::pause() {
}

void AyudaState::resume() {
}

bool AyudaState::frameStarted(const Ogre::FrameEvent& evt) {
    CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);
    return true;
}

bool AyudaState::frameEnded(const Ogre::FrameEvent& evt) {
    if (_exitGame)
        return false;

    return true;
}

void AyudaState::keyPressed(const OIS::KeyEvent &e) {
}

void AyudaState::keyReleased(const OIS::KeyEvent &e) {
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
}

void AyudaState::mouseMoved(const OIS::MouseEvent &e) {
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton AyudaState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void AyudaState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void AyudaState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

AyudaState* AyudaState::getSingletonPtr() {
    return msSingleton;
}

AyudaState& AyudaState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void AyudaState::createGUI() {

    //Sheet
    CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout("Ayuda.layout");
    //Exit button
    CEGUI::WindowManager::getSingleton().getWindow("Ayuda/ExitButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&AyudaState::Salir, this));

    CEGUI::System::getSingleton().setGUISheet(sheet);
}
bool AyudaState::Salir(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    changeState(IntroState::getSingletonPtr());
    return true;
}

void AyudaState::loadResources() {
    Ogre::ConfigFile cf;
    cf.load("resources.cfg");

    Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
    Ogre::String sectionstr, typestr, datastr;
    while (sI.hasMoreElements()) {
        sectionstr = sI.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i) {
            typestr = i->first;
            datastr = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation
                    (datastr, typestr, sectionstr);
        }
    }
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}
