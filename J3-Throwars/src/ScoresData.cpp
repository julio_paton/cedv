/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/
#include "ScoresData.h"
#include <Ogre.h>
#include <OgreStringConverter.h>
#include <OgreException.h>
#include "Utils.h"

ScoresData::ScoresData() {

//	_filename = "media/scores/scores.save";

	//Load file
//	std::ifstream file;
//	file.open(_filename.c_str());
//	if (!file)
//		OGRE_EXCEPT(Ogre::Exception::ERR_FILE_NOT_FOUND,
//				"File " + _filename + " not found.", "scoresData()")
//
//	std::string line;
//	size_t lineNo = 0;
//	while (std::getline(file, line)) {
//		lineNo++;
//		std::string temp = line;
//
//		if (temp.empty())
//			continue;
//
//		parseLine(temp, lineNo);
//	}
//
//	file.close();

}

ScoresData::~ScoresData() {
}

ScoresData& ScoresData::getInstance() {
	static ScoresData Instance;
	return Instance;
}
bool ScoresData::loadData() {
    bool status=true;
//	_filename = "media/scores/scores.save";
        //Vaciamos lo que haya
        _scores.clear();
	//Load file
	std::ifstream file;
	file.open(_filename.c_str());
	if (!file)
            status=false;
//		OGRE_EXCEPT(Ogre::Exception::ERR_FILE_NOT_FOUND,
//				"File " + _filename + " not found.", "scoresData()")

	std::string line;
	size_t lineNo = 0;
	while (std::getline(file, line)) {
		lineNo++;
		std::string temp = line;

		if (temp.empty())
			continue;

		parseLine(temp, lineNo);
	}

	file.close();
        return status;
}
void ScoresData::setFilename(std::string world){
   _filename = "media/scores/scoresWorld"+world+".save"; 
}

void ScoresData::extractName(std::string &name, size_t const &sepPos,
		const std::string &line) const {
	name = line.substr(0, sepPos);
	if (name.find('\t') != line.npos || name.find(' ') != line.npos)
		name.erase(name.find_first_of("\t "));
}
void ScoresData::extractScore(int& score, size_t const &sepPos,
		const std::string &line) const {
	std::string score_aux = line.substr(sepPos + 1);
	score_aux.erase(0, score_aux.find_first_not_of("\t "));
	score_aux.erase(score_aux.find_last_not_of("\t ") + 1);

	score = Ogre::StringConverter::parseInt(score_aux, 0);
}

void ScoresData::extractContents(const std::string &line) {
	std::string temp = line;
	temp.erase(0, temp.find_first_not_of("\t "));
	size_t sepPos = temp.find(':');

	std::string name;
	int score;
	extractName(name, sepPos, temp);
	extractScore(score, sepPos, temp);

	_scores.push_back(pair_score(name, score));
}

void ScoresData::parseLine(const std::string &line, size_t const lineNo) {
	if (line.find(':') == line.npos)
		Ogre::LogManager::getSingletonPtr()->logMessage(
				"Couldn't find separator on line: " + NumberToString(lineNo)
						+ "\n");

	if (!validLine(line))
		Ogre::LogManager::getSingletonPtr()->logMessage(
				"ScoresData: Bad format for line: " + NumberToString(lineNo)
						+ "\n");

	extractContents(line);
}

bool ScoresData::validLine(const std::string &line) const {
	std::string temp = line;
	temp.erase(0, temp.find_first_not_of("\t "));
	if (temp[0] == ':')
		return false;

	for (size_t i = temp.find(':') + 1; i < temp.length(); i++)
		if (temp[i] != ' ')
			return true;

	return false;
}

ScoresData::vector_scores ScoresData::getScores() {
	std::sort(_scores.begin(),_scores.end(),ScoresData::sort_pred());
	return _scores;
}

void ScoresData::addScore(std::string name, int score) {

	//Load file
	std::ofstream file;
	file.open(_filename.c_str(), std::ios::out | std::ios::app);

	if (file.is_open()) {
		file << name << " : " << score << std::endl;
		file.close();
	}

	_scores.push_back(pair_score(name, score));

}
