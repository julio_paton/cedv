/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#include "IntroState.h"
#include "SelPerState.h"
#include "RecordsState.h"
#include "AyudaState.h"

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

void IntroState::enter() {
    _root = Ogre::Root::getSingletonPtr();

    if (_root->hasSceneManager("SceneManager")) {
        _sceneMgr = _root->getSceneManager("SceneManager");
    } else {
        _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
        _CEGUIRenderer = &CEGUI::OgreRenderer::bootstrapSystem();
    }

    if (_sceneMgr->hasCamera("MenusCamera")) {
        _camera = _sceneMgr->getCamera("MenusCamera");
    } else {
        _camera = _sceneMgr->createCamera("MenusCamera");
    }

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    _viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 1.0));

    loadResources();
    GameManager::getSingletonPtr()->playTrackMenu();

    //CEGUI
    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    CEGUI::Imageset::setDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

    CEGUI::SchemeManager::getSingleton().create("TaharezLook.scheme");
    CEGUI::System::getSingleton().setDefaultFont("Audiowide-10");
    CEGUI::System::getSingleton().setDefaultMouseCursor("TaharezLook", "MouseArrow");

    createGUI();
    _exitGame = false;

    //    createScene();
}

void IntroState::exit() {
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void IntroState::pause() {
    //GameManager::getSingletonPtr()->pauseTrackMenu();
}

void IntroState::resume() {
    createGUI();
    GameManager::getSingletonPtr()->playTrackMenu();
}

bool IntroState::frameStarted(const Ogre::FrameEvent& evt) {
    CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);
    return true;
}

bool IntroState::frameEnded(const Ogre::FrameEvent& evt) {
    if (_exitGame)
        return false;

    return true;
}

void IntroState::keyPressed(const OIS::KeyEvent &e) {
    CEGUI::System::getSingleton().injectKeyDown(e.key);
    CEGUI::System::getSingleton().injectChar(e.text);
}

void IntroState::keyReleased(const OIS::KeyEvent &e) {
    CEGUI::System::getSingleton().injectKeyUp(e.key);
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
}

void IntroState::mouseMoved(const OIS::MouseEvent &e) {
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton IntroState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void IntroState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void IntroState::mouseReleased(const OIS::MouseEvent &e,
        OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

IntroState*
IntroState::getSingletonPtr() {
    return msSingleton;
}

IntroState&
IntroState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void IntroState::createGUI() {

    CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout("Portada.layout");

    //Start button
    CEGUI::WindowManager::getSingleton().getWindow("Portada/PlayButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::Empezar, this));
    //Start button
    CEGUI::WindowManager::getSingleton().getWindow("Portada/OtionsButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::Opciones, this));
    //Records button
    CEGUI::WindowManager::getSingleton().getWindow("Portada/RecordsButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::Records, this));
    //Help button
    CEGUI::WindowManager::getSingleton().getWindow("Portada/HelpButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::Ayuda, this));
    //Exit button
    CEGUI::WindowManager::getSingleton().getWindow("Portada/ExitButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::Salir, this));

    CEGUI::System::getSingleton().setGUISheet(sheet);
}

bool IntroState::Salir(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    _exitGame = true;
    return true;
}

bool IntroState::Empezar(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    changeState(SelPerState::getSingletonPtr());
    return true;
}

bool IntroState::Opciones(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    //    changeState(RecordsState::getSingletonPtr());
    return true;
}

bool IntroState::Ayuda(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    changeState(AyudaState::getSingletonPtr());
    return true;
}

bool IntroState::Records(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    changeState(RecordsState::getSingletonPtr());
    return true;
}

void IntroState::loadResources() {
    Ogre::ConfigFile cf;
    cf.load("resources.cfg");

    Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
    Ogre::String sectionstr, typestr, datastr;
    while (sI.hasMoreElements()) {
        sectionstr = sI.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i) {
            typestr = i->first;
            datastr = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation
                    (datastr, typestr, sectionstr);
        }
    }
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}