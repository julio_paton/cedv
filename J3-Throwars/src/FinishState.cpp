/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#include "FinishState.h"
#include "PlayState.h"
#include "IntroState.h"
#include "SelPerState.h"

template<> FinishState* Ogre::Singleton<FinishState>::msSingleton = 0;

void FinishState::enter() {
	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->createCamera("FinishCamera");

	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

	loadResources();

	createGUI();

	initializeVars();

}

void FinishState::exit() {
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    _sceneMgr->destroyCamera("FinishCamera");
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void FinishState::pause() {
}

void FinishState::resume() {
}

bool FinishState::frameStarted(const Ogre::FrameEvent& evt) {
	CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);
	return true;
}

bool FinishState::frameEnded(const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void FinishState::keyPressed(const OIS::KeyEvent &e) {
}

void FinishState::keyReleased(const OIS::KeyEvent &e) {
	if (e.key == OIS::KC_ESCAPE) {
		_exitGame = true;
	}
}

void FinishState::mouseMoved(const OIS::MouseEvent &e) {
	CEGUI::System &sys = CEGUI::System::getSingleton();
	sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
	// Scroll wheel.
	if (e.state.Z.rel)
		sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton FinishState::convertButton(OIS::MouseButtonID buttonID) {
	switch (buttonID) {
	case OIS::MB_Left:
		return CEGUI::LeftButton;

	case OIS::MB_Right:
		return CEGUI::RightButton;

	case OIS::MB_Middle:
		return CEGUI::MiddleButton;

	default:
		return CEGUI::LeftButton;
	}
}

void FinishState::mousePressed(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {
	CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void FinishState::mouseReleased(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {
	CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

FinishState* FinishState::getSingletonPtr() {
	return msSingleton;
}

FinishState& FinishState::getSingleton() {
	assert(msSingleton);
	return *msSingleton;
}

void FinishState::createGUI() {

	//Mouse
	CEGUI::MouseCursor::getSingleton().setVisible(true);

	//Sheet
	CEGUI::Window* sheet =
			CEGUI::WindowManager::getSingleton().loadWindowLayout(
					"Finish.layout");
	//Menu button

	if (PlayState::getSingletonPtr()->getGameMode() == MODE_PH_V_PH) {
		CEGUI::WindowManager::getSingleton().getWindow("Finish/NextButton")->subscribeEvent(
				CEGUI::PushButton::EventClicked,
				CEGUI::Event::Subscriber(&FinishState::NuevaPartida, this));
	} else {
		CEGUI::WindowManager::getSingleton().getWindow("Finish/NextButton")->subscribeEvent(
				CEGUI::PushButton::EventClicked,
				CEGUI::Event::Subscriber(&FinishState::SiguientePantalla,
						this));
	}
	//Menu button
	CEGUI::WindowManager::getSingleton().getWindow("Finish/RepeatButton")->subscribeEvent(
			CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&FinishState::RepetirPartida, this));
	//Exit button
	CEGUI::WindowManager::getSingleton().getWindow("Finish/ExitButton")->subscribeEvent(
			CEGUI::PushButton::EventClicked,
			CEGUI::Event::Subscriber(&FinishState::Salir, this));

	CEGUI::System::getSingleton().setGUISheet(sheet);
}

bool FinishState::SiguientePantalla(const CEGUI::EventArgs &e) {
	GameManager::getSingletonPtr()->playSoundClick();
	changeState(IntroState::getSingletonPtr());
	return true;
}

bool FinishState::NuevaPartida(const CEGUI::EventArgs &e) {
	GameManager::getSingletonPtr()->playSoundClick();
	changeState(SelPerState::getSingletonPtr());
	return true;
}

bool FinishState::RepetirPartida(const CEGUI::EventArgs &e) {
	GameManager::getSingletonPtr()->playSoundClick();
	changeState(PlayState::getSingletonPtr());
	return true;
}

bool FinishState::Salir(const CEGUI::EventArgs &e) {
	GameManager::getSingletonPtr()->playSoundClick();
	_exitGame = true;
	return true;
}

void FinishState::initializeVars() {
	CEGUI::Window *botonNext = CEGUI::WindowManager::getSingleton().getWindow(
			"Finish/NextButton");
	CEGUI::Window *botonRepeat = CEGUI::WindowManager::getSingleton().getWindow(
			"Finish/RepeatButton");

	CEGUI::Window *scoreNumber = CEGUI::WindowManager::getSingleton().getWindow(
				"Finish/Score/Score");
	CEGUI::Window *scoreResult = CEGUI::WindowManager::getSingleton().getWindow(
					"Finish/Score/Result");

	scoreNumber->setText(Ogre::StringConverter::toString(_puntuacion));
	scoreResult->setText(_playerName + " WINS");

	if (PlayState::getSingletonPtr()->getGameMode() == MODE_PH_V_PH) {
		botonNext->setText("Nueva partida");
		botonRepeat->setText("Volver a jugar");
	} else {
		botonNext->setText("Siguiente mundo");
		botonRepeat->setText("Reintentar");
	}

	ScoresData::getInstance().setFilename(
			PlayState::getSingletonPtr()->getWorldModel());
	ScoresData::getInstance().addScore(_playerName, _puntuacion);

	_exitGame = false;
}

void FinishState::loadResources() {
	Ogre::ConfigFile cf;
	cf.load("resources.cfg");

	Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
	Ogre::String sectionstr, typestr, datastr;
	while (sI.hasMoreElements()) {
		sectionstr = sI.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for (i = settings->begin(); i != settings->end(); ++i) {
			typestr = i->first;
			datastr = i->second;
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
					datastr, typestr, sectionstr);
		}
	}
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

void FinishState::setWinner(String player, String playerName, int puntuacion) {

	_playerWin = player;
	_playerName = playerName;
	_puntuacion = puntuacion;
}
