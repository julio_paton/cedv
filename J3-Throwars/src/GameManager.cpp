/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#include <Ogre.h>

#include "GameManager.h"
#include "GameState.h"
#include "SoundFXManager.h"
#include "TrackManager.h"
#include "PlayState.h"

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

template<> GameManager* Ogre::Singleton<GameManager>::msSingleton = 0;

GameManager::GameManager() {
    _root = 0;
    initSDL();
}

GameManager::~GameManager() {
    while (!_states.empty()) {
        _states.top()->exit();
        _states.pop();
    }

    if (_root)
        delete _root;
}

void GameManager::start(GameState* state) {
    // Creación del objeto Ogre::Root.
    _root = new Ogre::Root();

    loadResources();
    loadSounds();
    _soundVolumen=50;
    if (!configure())
        return;

    _inputMgr = new InputManager;
    _inputMgr->initialise(_renderWindow);

    // Registro como key y mouse listener...
    _inputMgr->addKeyListener(this, "GameManager");
    _inputMgr->addMouseListener(this, "GameManager");

    // El GameManager es un FrameListener.
    _root->addFrameListener(this);

    // Transición al estado inicial.
    changeState(state);

    // Bucle de rendering.
    _root->startRendering();
}

void GameManager::changeState(GameState* state) {
    // Limpieza del estado actual.
    if (!_states.empty()) {
        // exit() sobre el último estado.
        _states.top()->exit();
        // Elimina el último estado.
        _states.pop();
    }

    // Transición al nuevo estado.
    _states.push(state);
    // enter() sobre el nuevo estado.
    _states.top()->enter();
}

void GameManager::pushState(GameState* state) {
    // Pausa del estado actual.
    if (!_states.empty())
        _states.top()->pause();

    // Transición al nuevo estado.
    _states.push(state);
    // enter() sobre el nuevo estado.
    _states.top()->enter();
}

void GameManager::popState() {
    // Limpieza del estado actual.
    if (!_states.empty()) {
        _states.top()->exit();
        _states.pop();
    }

    // Vuelta al estado anterior.
    if (!_states.empty())
        _states.top()->resume();
}

void GameManager::loadResources() {
    Ogre::ConfigFile cf;
    cf.load("resources.cfg");

    Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
    Ogre::String sectionstr, typestr, datastr;
    while (sI.hasMoreElements()) {
        sectionstr = sI.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i) {
            typestr = i->first;
            datastr = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation
                    (datastr, typestr, sectionstr);
        }
    }
}
void GameManager::loadSounds() {
    //Manejador de sonidos
    _pSoundFXManager = new SoundFXManager;
    _pTrackManager = new TrackManager;
    //Sonidos
    _soundClick= _pSoundFXManager->load("ButtonClick.wav");
    _soundDead= _pSoundFXManager->load("Error.wav");
    _soundExplosion= _pSoundFXManager->load("explosion.ogg");
    _soundFiveSeconds= _pSoundFXManager->load("Error.wav");
    _soundHit= _pSoundFXManager->load("Acierto.wav");
    _soundShoot= _pSoundFXManager->load("disparo.ogg");
    
    _trackMenu = _pTrackManager->load("Introduccion.mp3");
    _trackGame1 = _pTrackManager->load("mundo1.mp3");
    _trackGame2 = _pTrackManager->load("mundo1.mp3");
    _trackGame3 = _pTrackManager->load("mundo1.mp3");
    _trackSelection = _pTrackManager->load("eleccion.mp3");
}

bool GameManager::configure() {
    if (!_root->restoreConfig()) {
        if (!_root->showConfigDialog()) {
            return false;
        }
    }

    _renderWindow = _root->initialise(true, "Throwars");

    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

    return true;
}

GameManager* GameManager::getSingletonPtr() {
    return msSingleton;
}

GameManager& GameManager::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

// Las siguientes funciones miembro delegan
// el evento en el estado actual.

bool GameManager::frameStarted(const Ogre::FrameEvent& evt) {
    _inputMgr->capture();
    return _states.top()->frameStarted(evt);
}

bool GameManager::frameEnded(const Ogre::FrameEvent& evt) {
    return _states.top()->frameEnded(evt);
}

bool GameManager::keyPressed(const OIS::KeyEvent &e) {
    _states.top()->keyPressed(e);
    return true;
}

bool GameManager::keyReleased(const OIS::KeyEvent &e) {
    _states.top()->keyReleased(e);
    return true;
}

bool GameManager::mouseMoved(const OIS::MouseEvent &e) {
    _states.top()->mouseMoved(e);
    return true;
}

bool GameManager::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    _states.top()->mousePressed(e, id);
    return true;
}

bool GameManager::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    _states.top()->mouseReleased(e, id);
    return true;
}

bool GameManager::initSDL() {
    // Inicializando SDL...
    if (SDL_Init(SDL_INIT_AUDIO) < 0)
        return false;
    // Llamar a  SDL_Quit al terminar.
    atexit(SDL_Quit);

    // Inicializando SDL mixer...
    if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,
            MIX_DEFAULT_CHANNELS, 4096) < 0)
        return false;

    // Llamar a Mix_CloseAudio al terminar.
    atexit(Mix_CloseAudio);

    return true;
}

void GameManager::playSoundClick() {
    _soundClick->play();
}

void GameManager::playSoundShoot() {
    _soundShoot->play();
}

void GameManager::playSoundExplosion() {
    _soundExplosion->play();
}

void GameManager::playSoundHit() {
    _soundHit->play();
}

void GameManager::playSoundDead() {
    _soundDead->play();
}

void GameManager::playSoundFiveSeconds() {
    _soundFiveSeconds->play();
}

void GameManager::playTrackMenu() {
    _trackMenu->play(5);
}

void GameManager::playTrackGame1() {
    _trackGame1->play(5);
}

void GameManager::playTrackGame2() {
    _trackGame2->play(5);
}

void GameManager::playTrackGame3() {
    _trackGame3->play(5);
}

void GameManager::playTrackSelection(){
	_trackSelection->play(5);
}

void GameManager::pauseTrackMenu() {
    _trackMenu->pause();
}

void GameManager::pauseTrackGame1() {
    _trackGame1->pause();
}

void GameManager::pauseTrackGame2() {
    _trackGame2->pause();
}

void GameManager::pauseTrackGame3() {
    _trackGame3->pause();
}

void GameManager::pauseTrackSelection() {
    _trackSelection->pause();
}

void GameManager::stopTrackMenu() {
    _trackMenu->stop();
}

void GameManager::stopTrackGame1() {
    _trackGame1->stop();
}

void GameManager::stopTrackGame2() {
    _trackGame2->stop();
}

void GameManager::stopTrackGame3() {
    _trackGame3->stop();
}

void GameManager::stopTrackSelection() {
    _trackSelection->stop();
}

void GameManager::setVolumen(int volumen){
    _soundVolumen=volumen;
    Mix_VolumeMusic(_soundVolumen);
}

InputManager*
GameManager::getInputManager() {
	return _inputMgr;
}
