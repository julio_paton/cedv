/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#include "PlayState.h"
#include "SelPerState.h"
#include "PauseState.h"
#include "FinishState.h"
#include <math.h> 

#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void PlayState::enter() {
    GameManager::getSingletonPtr()->stopTrackSelection();
    GameManager::getSingletonPtr()->playTrackGame1();
    _root = Ogre::Root::getSingletonPtr();
    _mInputDevice = GameManager::getSingletonPtr()->getInputManager();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    //    _camera = _sceneMgr->getCamera("IntroCamera");
    _camera = _sceneMgr->createCamera("MainCamera");
    //_camera->setPosition(Vector3(0, 0, 80));
    _camera->setPosition(Ogre::Vector3(0, 6, 20.5)); //antes (0,5,22)->tb vale
    _camera->lookAt(Ogre::Vector3(0, 5, 0));
    _camera->setNearClipDistance(5);
    _camera->setFarClipDistance(5000);

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));
    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    _camera->setAspectRatio(width / height);

    // Creamos la escena
    cout << "1 - creando escena" << endl;
    createScene();
    // Creando la gui
    cout << "2 - creando gui" << endl;
    createGUI();
    // Inicializando variables
    cout << "3 - inicializando variables" << endl;
    initializeVars();
    // Empezamos juego nuevo (llamada a los personajes??)
    cout << "4 - empezamos juego" << endl;
    newGame();
}

void PlayState::exit() {
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    _sceneMgr->destroyAllCameras();
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();

    GameManager::getSingletonPtr()->stopTrackGame1();

    // Eliminar cuerpos rigidos --------------------------------------
    std::deque<OgreBulletDynamics::RigidBody *>::iterator itBody =
            _bodies.begin();
    while (_bodies.end() != itBody) {
        delete *itBody;
        ++itBody;
    }

    // Eliminar formas de colision -----------------------------------
    std::deque<OgreBulletCollisions::CollisionShape *>::iterator itShape =
            _shapes.begin();
    while (_shapes.end() != itShape) {
        delete *itShape;
        ++itShape;
    }

    _bodies.clear();
    _shapes.clear();

    // Eliminar mundo dinamico y debugDrawer -------------------------
    delete _world->getDebugDrawer();
    _world->setDebugDrawer(0);
    delete _world;
}

void PlayState::pause() {
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    //    GameManager::getSingletonPtr()->pauseTrackGame();
    GameManager::getSingletonPtr()->playTrackMenu();
}

void PlayState::resume() {
    createGUI();
    GameManager::getSingletonPtr()->playTrackGame1();
    // Se restaura el background colour.
    // _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}

bool PlayState::frameStarted(const Ogre::FrameEvent& evt) {

    _deltaT = evt.timeSinceLastFrame;

    CEGUI::System::getSingleton().injectTimePulse(_deltaT);

    Ogre::Vector3 vt(0, 0, 0);
    Ogre::Real tSpeed = 20.0;
    bool mbmiddle; // Botones del raton pulsados

    // Actualizar simulacion Bullet
    _world->stepSimulation(_deltaT);
    DetectCollision();

    if (m_pTimer->getMillisecondsCPU() / 1000 >= _turnTime) {
        //std::cout << "Ya han pasado" << std::endl;
        _stopTime = true;
        turnChanged();
    }

    CEGUI::Window* reloj = CEGUI::WindowManager::getSingleton().getWindow(
            "Game/Time");

    std::stringstream tiempo;
    if (!_stopTime) {
        tiempo << (_turnTime - (m_pTimer->getMillisecondsCPU() / 1000.0));
        if ((m_pTimer->getMillisecondsCPU() / 1000) >= (_turnTime - 6)) {
            reloj->setText(
                    "[colour='FFFF0000'][padding='l:15 t:0 r:0 b:0']"
                    + tiempo.str());
        } else {
            reloj->setText(
                    "[colour='FFFFFFFF'][padding='l:15 t:0 r:0 b:0']"
                    + tiempo.str());
        }
    } else {
        tiempo.str("");
        tiempo << _turnTime << ".00";
        reloj->setText(
                "[colour='FFFFFFFF'][padding='l:4 t:0 r:0 b:0']"
                + tiempo.str());
        if (!_waitForColision) {
            CEGUI::WindowManager::getSingleton().getWindow(
                    "Game/Player" + Ogre::StringConverter::toString(_turno)
                    + "/Wait")->setVisible(true);
            if (m_pTimer->getMillisecondsCPU() > 1000) {
                std::stringstream wait;
                wait << (4 - (m_pTimer->getMillisecondsCPU() / 1000));
                CEGUI::WindowManager::getSingleton().getWindow(
                        "Game/Player" + Ogre::StringConverter::toString(_turno)
                        + "/Wait")->setText(
                        "[font='Audiowide-72']" + wait.str());
                if (m_pTimer->getMillisecondsCPU() > 4000) {
                    CEGUI::WindowManager::getSingleton().getWindow(
                            "Game/Player"
                            + Ogre::StringConverter::toString(_turno)
                            + "/Wait")->setText(
                            "[font='Audiowide-72'][colour='FF6EDC2D'] GO!");
                }
            }
            if (m_pTimer->getMillisecondsCPU() > 5000) {
                //std::cout << "Cambio de turno" << std::endl;
                CEGUI::WindowManager::getSingleton().getWindow(
                        "Game/Player" + Ogre::StringConverter::toString(_turno)
                        + "/Wait")->setVisible(false);
                CEGUI::WindowManager::getSingleton().getWindow(
                        "Game/Player" + Ogre::StringConverter::toString(_turno)
                        + "/Apun")->setVisible(true);
                _stopTime = false;
                m_pTimer->reset();
            }
        }
    }

    if (_shootInCharge) {
        CEGUI::ProgressBar* progressBar =
                static_cast<CEGUI::ProgressBar*> (CEGUI::WindowManager::getSingleton().getWindow(
                "Game/Player" + Ogre::StringConverter::toString(_turno)
                + "/Fuerza"));
        if (_power > PlayerConstants::max_power) {
            _power = 0.0;
        } else {
            if (_power > PlayerConstants::max_power / 2) {
                _power += PlayerConstants::max_power * 0.5 * _deltaT;
            } else {
                _power += PlayerConstants::max_power * _deltaT;
            }
        }
        progressBar->setProperty("CurrentProgress",
                Ogre::StringConverter::toString(
                _power / PlayerConstants::max_power));
    }
    if (_angleUp || _angleDown) {

        if (_turno == PLAYER_ONE) {
            if (_angleUp) {
                if (_angleP1 >= -90) {
                    _angleP1 -= 20 * _deltaT;
                }
            }
            if (_angleDown) {
                if (_angleP1 <= 0) {
                    _angleP1 += 20 * _deltaT;
                }
            }
            CEGUI::WindowManager::getSingleton().getWindow("Game/Player1/Apun")->setRotation(
                    CEGUI::Vector3(0, 0, _angleP1));
        } else {
            if (_angleUp) {
                if (_angleP2 >= -90) {
                    _angleP2 -= 20 * _deltaT;
                }
            }
            if (_angleDown) {
                if (_angleP2 <= 0) {
                    _angleP2 += 20 * _deltaT;
                }
            }
            CEGUI::WindowManager::getSingleton().getWindow("Game/Player2/Apun")->setRotation(
                    CEGUI::Vector3(0, 180, -_angleP2));
        }
    }
    _viento = _currentGravity.getX();
    CEGUI::WindowManager::getSingleton().getWindow("Game/Viento")->setText("V: " + Ogre::StringConverter::toString(_viento));

    float auxVida = _playerOne->getVida() / 1000.0;
    CEGUI::WindowManager::getSingleton().getWindow("Game/Player1/Vida")->setProperty(
            "CurrentProgress", Ogre::StringConverter::toString(auxVida));
    auxVida = _playerTwo->getVida() / 1000.0;
    CEGUI::WindowManager::getSingleton().getWindow("Game/Player2/Vida")->setProperty(
            "CurrentProgress", Ogre::StringConverter::toString(auxVida));

    //Animaciones
    _playerOne->update(_deltaT);
    _playerTwo->update(_deltaT);

    //	_camera->moveRelative(vt * _deltaT * tSpeed);
    //	if (_camera->getPosition().length() < 10.0) {
    //		_camera->moveRelative(-vt * _deltaT * tSpeed);
    //	}

    // Si usamos la rueda, desplazamos en Z la camara ------------------
    vt += Ogre::Vector3(0, 0, -50) * _deltaT
            * _mInputDevice->getMouse()->getMouseState().Z.rel;
    _camera->moveRelative(vt * _deltaT * tSpeed);

    // Botones del raton pulsados? -------------------------------------
    mbmiddle = _mInputDevice->getMouse()->getMouseState().buttonDown(
            OIS::MB_Middle);

    if (mbmiddle) { // Con boton medio pulsado, rotamos camara ---------
        float rotx = _mInputDevice->getMouse()->getMouseState().X.rel * _deltaT
                * -1;
        float roty = _mInputDevice->getMouse()->getMouseState().Y.rel * _deltaT
                * -1;
        _camera->yaw(Ogre::Radian(rotx));
        _camera->pitch(Ogre::Radian(roty));
    }

    return true;
}

bool PlayState::frameEnded(const Ogre::FrameEvent& evt) {

    if (_gameIsOver) {
        changeState(FinishState::getSingletonPtr());
    }

    if (_exitGame)
        return false;

    return true;
}

void PlayState::keyPressed(const OIS::KeyEvent &e) {
    // Tecla p --> PauseState.
    if (e.key == OIS::KC_P) {
        pushState(PauseState::getSingletonPtr());
    } else if (e.key == OIS::KC_D) {
        _world->setShowDebugShapes(true);
    } else if (e.key == OIS::KC_1) {
        gameIsOver(_playerOne->getId());
    } else if (e.key == OIS::KC_2) {
        gameIsOver(_playerOne->getId());
    } else if (e.key == OIS::KC_H) {
        _world->setShowDebugShapes(false);
    } else if (!_stopTime) { //Para evitar disparos fuera de turno
        if (e.key == OIS::KC_SPACE) {
            _shootInCharge = true;
        } else if (e.key == OIS::KC_UP) {
            _angleUp = true;
        } else if (e.key == OIS::KC_DOWN) {
            _angleDown = true;
        }
    }
}

void PlayState::keyReleased(const OIS::KeyEvent &e) {

    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    } else if (e.key == OIS::KC_RIGHT) {
        //Actualizar viento
        _currentGravity += btVector3(1, 0, 0);

        _world->getBulletDynamicsWorld()->setGravity(_currentGravity);
        //std::cout << "Current wind: " << _currentGravity.getX() << std::endl;
    } else if (e.key == OIS::KC_LEFT) {
        //Actualizar viento
        _currentGravity -= btVector3(1, 0, 0);
        _world->getBulletDynamicsWorld()->setGravity(_currentGravity);
        //std::cout << "Current wind: " << _currentGravity.getX() << std::endl;
    } else if (!_stopTime) { //Para evitar disparos fuera de turno
        if (e.key == OIS::KC_SPACE) {
            _shootInCharge = false;
            cout << "fuerza: " << _power << endl;

            if (_turno == PLAYER_ONE) {
                cout << "angulo: " << _angleP1 << endl;
                _disparosP1++;
                _activePlayer->setShootConditions(_angleP1, _power);
            } else {
                cout << "angulo: " << _angleP2 << endl;
                _disparosP2++;
                _activePlayer->setShootConditions(_angleP2, _power);
            }
            _activePlayer->selectAnimation(1, false);
            _waitForColision = true;
        } else if (e.key == OIS::KC_UP || e.key == OIS::KC_DOWN) {
            _angleUp = false;
            _angleDown = false;
        }
    }
}

void PlayState::mouseMoved(const OIS::MouseEvent &e) {
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

void PlayState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void PlayState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

PlayState* PlayState::getSingletonPtr() {
    return msSingleton;
}

PlayState& PlayState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void PlayState::changeView(const OIS::KeyEvent &e) {
    // Tecla v --> Cambiar visualizacion
    /*if (e.key == OIS::KC_V) {
     // Se recupera el gestor de escena y la cámara.
     _sceneMgr = _root->getSceneManager("SceneManager");
     // _camera = _sceneMgr->getCamera("IntroCamera");
     _camera = _sceneMgr->createCamera("MainCamera");
     //_camera->setPosition(Vector3(0, 0, 80));
     _camera->setPosition(Ogre::Vector3(-100, 25, 0));
     _camera->lookAt(Ogre::Vector3(0, 0, 0));
     _camera->setNearClipDistance(5);
     _camera->setFarClipDistance(10000);
     }*/

}

void PlayState::createScene() {

    Ogre::SceneNode* nodeLight = _sceneMgr->createSceneNode("LightingNode");
    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
    //Al usar estas sombras, el farClipDistance se pone por defecto al infinito
    _sceneMgr->setShadowUseInfiniteFarPlane(false);

    Ogre::Light* light = _sceneMgr->createLight("Light1");
    light->setType(Ogre::Light::LT_DIRECTIONAL);
    light->setDirection(Ogre::Vector3(1, -1, 0));
    nodeLight->attachObject(light);

    _sceneMgr->getRootSceneNode()->addChild(nodeLight);

    // Creacion del mundo (definicion de los limites y la gravedad) ---
    AxisAlignedBox worldBounds = AxisAlignedBox(Vector3(-100, -100, -100),
            Vector3(100, 100, 100));
    _currentGravity = btVector3(0, -9.81, 0);

    _world = new OgreBulletDynamics::DynamicsWorld(_sceneMgr, worldBounds,
            BtOgreConverter::to(_currentGravity));

    // Creacion de la entidad y del SceneNode ------------------------
    //Creacion del suelo
    Plane plane1(Vector3(0, 1, 0), 0); // Normal y distancia
    MeshManager::getSingleton().createPlane("p1",
            ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane1, 70, 35,
            1, 1, false, 1, 20, 20, Vector3::UNIT_Z);
    SceneNode* nodeGround = _sceneMgr->createSceneNode("ground");
    Entity* groundEnt = _sceneMgr->createEntity("planeEnt", "p1");
    groundEnt->setMaterialName("Ground" + _worldModel);
    nodeGround->attachObject(groundEnt);
    _sceneMgr->getRootSceneNode()->addChild(nodeGround);
    nodeGround->setPosition(0, -1, 0);
    _world->setShowDebugShapes(false); // Muestra los collision shapes

    // Creamos forma de colision para el plano
    OgreBulletCollisions::CollisionShape *Shape;
    Shape = new OgreBulletCollisions::StaticPlaneCollisionShape(
            Ogre::Vector3(0, 1, 0), 0); // Vector normal y distancia
    OgreBulletDynamics::RigidBody *rigidBodyPlane =
            new OgreBulletDynamics::RigidBody("rigidBodyPlane", _world,
            BulletConstants::COL_BUILDINGS,
            BulletConstants::buildingsCollideWith);

    // Creamos la forma estatica (forma, Restitucion, Friccion)
    rigidBodyPlane->setStaticShape(nodeGround, Shape, 0.1, 0.8);

    //_worldModel = "0";

    if (_worldModel != "0") {
        // Creación del cielo
        _sceneMgr->setSkyDome(true, "Skydome" + _worldModel, 65, 8, 4000);
        _mundoNode = _sceneMgr->createSceneNode("Building");
        Entity* entBuilding = _sceneMgr->createEntity(
                "mundo" + _worldModel + ".mesh");
        _mundoNode->attachObject(entBuilding);
        _sceneMgr->getRootSceneNode()->addChild(_mundoNode);
        _mundoNode->setPosition(0, 0, 0);
        //_mundoNode->yaw(Degree(-90));
        _mundoNode->scale(5, 5, 5);

        //La posición se establece con ogrebullet
        //	building->setPosition(0, 7.5, 0);

        //!!CUIDADO!!Si se escala el nodo con ogre la forma de colisión no se escala y no detecta ninguna colisión

        Matrix4 transf = Matrix4::IDENTITY;
        transf.setScale(_mundoNode->getScale());

        OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter =
                new OgreBulletCollisions::StaticMeshToShapeConverter(
                entBuilding, transf);

        OgreBulletCollisions::TriangleMeshCollisionShape *buildingsTrimesh =
                trimeshConverter->createTrimesh();

        OgreBulletDynamics::RigidBody *rbBuildings =
                new OgreBulletDynamics::RigidBody("track", _world,
                BulletConstants::COL_BUILDINGS,
                BulletConstants::buildingsCollideWith);
        rbBuildings->setStaticShape(_mundoNode, buildingsTrimesh, 0.8, 0.95,
                Vector3(0, 0, 0), Quaternion::IDENTITY);

    }

    psExplosion = _sceneMgr->createParticleSystem("Ps", "Explosion");
    psExplosion->setEmitting(false);

    psExplosionNode = _sceneMgr->getRootSceneNode()->createChildSceneNode(
            "PsNode");
    psExplosionNode->attachObject(psExplosion);
}

void PlayState::newGame() {

    _playerFactory = new PlayerFactory(_world);
    int playerOneModel = SelPerState::getSingleton().getPlayer1Model();
    int playerTwoModel = SelPerState::getSingleton().getPlayer2Model();

    switch (playerOneModel) {
        case 0:
            _playerOne = _playerFactory->makeMageWithFireball(
                    BulletConstants::COL_PLAYER1);
            break;
        case 1:
            _playerOne = _playerFactory->makeGolemWithFireball(
                    BulletConstants::COL_PLAYER1, "Golem_Fire");
            break;
        case 2:
            _playerOne = _playerFactory->makeGolemWithFireball(
                    BulletConstants::COL_PLAYER1, "Golem_Ice");
            break;
        case 3:
            _playerOne = _playerFactory->makeGolemWithFireball(
                    BulletConstants::COL_PLAYER1, "Golem_Stone");
            break;
        case 4:
            _playerOne = _playerFactory->makeDragonWithFireball(
                    BulletConstants::COL_PLAYER1);
            break;
    }

    switch (playerTwoModel) {
        case 0:
            _playerTwo = _playerFactory->makeMageWithFireball(
                    BulletConstants::COL_PLAYER2);
            break;
        case 1:
            _playerTwo = _playerFactory->makeGolemWithFireball(
                    BulletConstants::COL_PLAYER2, "Golem_Fire");
            break;
        case 2:
            _playerTwo = _playerFactory->makeGolemWithFireball(
                    BulletConstants::COL_PLAYER2, "Golem_Ice");
            break;
        case 3:
            _playerTwo = _playerFactory->makeGolemWithFireball(
                    BulletConstants::COL_PLAYER2, "Golem_Stone");
            break;
        case 4:
            _playerTwo = _playerFactory->makeDragonWithFireball(
                    BulletConstants::COL_PLAYER2);
            break;
    }
    _activePlayer = _playerOne;
}

void PlayState::initializeVars() {
    _power = 0.0;
    _turnTime = 10; //10 segundos de turno
    _shootInCharge = false;
    _angleP1 = 0.0;
    _angleP2 = 0.0;
    _angleUp = false;
    _angleDown = false;
    _stopTime = true;
    _gameIsOver = false;
    _waitForColision = false;

    _disparosP1 = 0;
    _disparosP2 = 0;

    if (_gameMode == MODE_PH_V_PH) {
        cout << "MODO HUMANO VS HUMANO" << endl;
    } else if (_gameMode == MODE_PH_V_CPU) {
        cout << "MODO HUMANO VS CPU" << endl;
    } else {
        cout << "NO tenemos modo, por defecto el HumanvsHuman" << endl;
        _gameMode = MODE_PH_V_PH;
    }

    _turno = PLAYER_ONE;
    _gravedad = 9.8;
    _viento = 2.0;

    m_pTimer = new Ogre::Timer();
    m_pTimer->reset();

    _exitGame = false;

}

void PlayState::createGUI() {
    CEGUI::MouseCursor::getSingleton().hide();
    //Sheet
    CEGUI::Window *sheet =
            CEGUI::WindowManager::getSingleton().loadWindowLayout(
            "Game.layout");
    CEGUI::System::getSingleton().setGUISheet(sheet);

    CEGUI::WindowManager::getSingleton().getWindow("Game/Player2/Fuerza")->setRotation(
            CEGUI::Vector3(0, 0, 180));
    CEGUI::WindowManager::getSingleton().getWindow("Game/Player2/Apun")->setRotation(
            CEGUI::Vector3(0, 180, 0));
    CEGUI::WindowManager::getSingleton().getWindow("Game/Player1/Apun")->setVisible(
            false);
    CEGUI::WindowManager::getSingleton().getWindow("Game/Player2/Apun")->setVisible(
            false);
    CEGUI::WindowManager::getSingleton().getWindow("Game/Player1/Wait")->setVisible(
            false);
    CEGUI::WindowManager::getSingleton().getWindow("Game/Player2/Wait")->setVisible(
            false);
    CEGUI::WindowManager::getSingleton().getWindow("Game/Player1/Name")->setText(
            "[colour='FF6EDC2D']" + _namePlayer1);
    CEGUI::WindowManager::getSingleton().getWindow("Game/Player2/Name")->setText(
            "[colour='FFFFFFFF']" + _namePlayer2);
    CEGUI::WindowManager::getSingleton().getWindow("Game/Mundo")->setText(
            "Mundo - " + _worldModel);

}

void PlayState::setGameMode(int mode) {
    _gameMode = mode;
}

void PlayState::setPlayerName(string name1, string name2) {
    _namePlayer1 = name1;
    _namePlayer2 = name2;
}

void PlayState::DetectCollision() {
    btCollisionWorld *bulletWorld = _world->getBulletCollisionWorld();
    int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();

    for (int i = 0; i < numManifolds; i++) {
        btPersistentManifold* contactManifold =
                bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);

        btCollisionObject* obA =
                static_cast<btCollisionObject*> (contactManifold->getBody0());
        btCollisionObject* obB =
                static_cast<btCollisionObject*> (contactManifold->getBody1());

        int numContacts = contactManifold->getNumContacts();

        if (numContacts) {

            Ogre::SceneNode* weapon = _activePlayer->getWeaponNode();
            OgreBulletCollisions::Object *obWeapon = NULL;

            if (weapon)
                obWeapon = _world->findObject(weapon);

            OgreBulletCollisions::Object *obOB_A = _world->findObject(obA);
            OgreBulletCollisions::Object *obOB_B = _world->findObject(obB);

            //Sólo se buscan las colisiones de las balas que se lanzan
            //CUIDADO CON CAMBIAR DE TURNO ANTES DE QUE ESTALLE, EL ARMA QUE DEVUELVE EL ACTIVE PLAYER
            //NO SERÍA LA CORRECTA
            if (obWeapon && ((obOB_A == obWeapon) || (obOB_B == obWeapon))) {
                Ogre::SceneNode* node = NULL;

                btManifoldPoint contactPoint = contactManifold->getContactPoint(
                        numContacts - 1);
                btVector3 vPoint;

                if (obOB_A == obWeapon) {
                    vPoint = contactPoint.getPositionWorldOnB();
                    node = obOB_A->getRootNode();
                    delete obOB_A;
                } else if (obOB_B == obWeapon) {
                    vPoint = contactPoint.getPositionWorldOnA();
                    node = obOB_B->getRootNode();
                    delete obOB_B;
                }
                if (node) {
                    //std::cout << node->getName() << std::endl;
                    _sceneMgr->getRootSceneNode()->removeAndDestroyChild(
                            node->getName());
                }

                Vector3 point = OgreBulletCollisions::BtOgreConverter::to(
                        vPoint);

                //Add particle
                psExplosionNode->setPosition(point);
                resetParticleSystem(psExplosion);

                int dano;

                dano = calcularDano();

                if (_activePlayer->getId() == "Player1") {
                    _playerTwo->tocado(dano);
                } else {
                    _playerOne->tocado(dano);
                }

                //Cambiar turno de jugador
                _waitForColision = false;
                turnChanged();
            }
        }
    }
}

void PlayState::setWorldModel(string worldModel) {
    if (worldModel != "1" && worldModel != "2" && worldModel != "3") {
        _worldModel = "1";
    } else {
        _worldModel = worldModel;
    }
}

void PlayState::resetParticleSystem(Ogre::ParticleSystem *ps) {

    for (unsigned short i = 0; i < ps->getNumEmitters(); i++) {
        ps->getEmitter(i)->setEnabled(false);
        ps->getEmitter(i)->setMinRepeatDelay(
                ps->getEmitter(i)->getRepeatDelay()); //This resets the repeatDelay to 0
        ps->getEmitter(i)->setEnabled(true);
    }
    ps->setEmitting(true);
}

void PlayState::turnChanged() {

    if (!_waitForColision) {
        _stopTime = true;
        m_pTimer->reset();

        _power = 0.0;

        string txt1 = "";
        string txt2 = "";
        CEGUI::WindowManager::getSingleton().getWindow(
                "Game/Player" + Ogre::StringConverter::toString(_turno)
                + "/Fuerza")->setProperty("CurrentProgress", "0");
        CEGUI::WindowManager::getSingleton().getWindow(
                "Game/Player" + Ogre::StringConverter::toString(_turno)
                + "/Apun")->setVisible(false);
        if (_turno == PLAYER_ONE) {
            txt1 = "[colour='FFFFFFFF']";
            txt2 = "[colour='FF6EDC2D']";
            _turno = PLAYER_TWO;
            _activePlayer = _playerTwo;
        } else {
            txt1 = "[colour='FF6EDC2D']";
            txt2 = "[colour='FFFFFFFF']";
            _turno = PLAYER_ONE;
            _activePlayer = _playerOne;
        }
        CEGUI::WindowManager::getSingleton().getWindow("Game/Player1/Name")->setText(
                txt1 + _namePlayer1);
        CEGUI::WindowManager::getSingleton().getWindow("Game/Player2/Name")->setText(
                txt2 + _namePlayer2);
    }
}

void PlayState::gameIsOver(String player) {
    String namePlayer = "";
    int numDisparos = 0;
    int vidaRestante = 0;

    if (player == "Player1") {
        namePlayer = _namePlayer1;
        numDisparos = _disparosP1;
        vidaRestante = _playerOne->getVida();
    } else {
        namePlayer = _namePlayer2;
        numDisparos = _disparosP2;
        vidaRestante = _playerTwo->getVida();
    }

    //calcular puntuacion ( vida restante - (100 * cada disparo hecho)
    int puntuacion = 10000 - vidaRestante - (100 * numDisparos);
    if (puntuacion < 0)
        puntuacion = 0;

    cout << "Juego ganado con puntuacion: " << puntuacion << endl;

    FinishState::getSingletonPtr()->setWinner(player, namePlayer, puntuacion);
    _gameIsOver = true;
}

int PlayState::calcularDano() {
    float x;
    float y;
    float z;

    //    if (_turno == PLAYER_ONE) {
    if (_activePlayer->getId() == "Player1") {
        x = (psExplosionNode->getPosition().x
                - _playerTwo->getNode()->getPosition().x);
        y = (psExplosionNode->getPosition().y
                - _playerTwo->getNode()->getPosition().y);
        z = (psExplosionNode->getPosition().z
                - _playerTwo->getNode()->getPosition().z);
    } else {
        x = (psExplosionNode->getPosition().x
                - _playerOne->getNode()->getPosition().x);
        y = (psExplosionNode->getPosition().y
                - _playerOne->getNode()->getPosition().y);
        z = (psExplosionNode->getPosition().z
                - _playerOne->getNode()->getPosition().z);
    }

    int distancia = Math::Floor(Math::Sqr((x * x) + (y * y) + (z * z)));

    int res;

    cout << "Distancia: " << distancia << endl;

    if (distancia > 3) {
        cout << "Distancia demasiado lejos: " << distancia << endl;
        res = 0;
    } else {
        res = 600 - (distancia * 100);
        cout << "Distancia: OK " << distancia << endl << "DANO: " << res
                << endl;
    }
    return res;
}
