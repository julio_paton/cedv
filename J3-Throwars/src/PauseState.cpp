/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#include "PauseState.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

void
PauseState::enter() {
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    if (_sceneMgr->hasCamera("PauseCamera")) {
        _camera = _sceneMgr->getCamera("PauseCamera");
    } else {
        _camera = _sceneMgr->createCamera("PauseCamera");
    }
    
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    
    _exitGame = false;
    createGUI();
}

void
PauseState::exit() {
    CEGUI::WindowManager::getSingleton().destroyWindow("PauseWindow");
    _sceneMgr->destroyCamera("PauseCamera");
}

void
PauseState::pause() {
}

void
PauseState::resume() {
    createGUI();
}

bool
PauseState::frameStarted
(const Ogre::FrameEvent& evt) {
    return true;
}

bool
PauseState::frameEnded
(const Ogre::FrameEvent& evt) {
    if (_exitGame)
        return false;

    return true;
}

void
PauseState::keyPressed
(const OIS::KeyEvent &e) {
    // Tecla p --> Estado anterior.
    if (e.key == OIS::KC_P) {
        popState();
    }
}

void
PauseState::keyReleased
(const OIS::KeyEvent &e) {
}

void
PauseState::mouseMoved(const OIS::MouseEvent &e) {
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton PauseState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void
PauseState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void
PauseState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

PauseState*
PauseState::getSingletonPtr() {
    return msSingleton;
}

PauseState&
PauseState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}
void PauseState::createGUI() {

	//Mouse
	CEGUI::MouseCursor::getSingleton().setVisible(true);
    CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout("Pause.layout");
    //Exit button
    CEGUI::WindowManager::getSingleton().getWindow("Pause/ExitButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PauseState::Salir, this));

    CEGUI::System::getSingleton().setGUISheet(sheet);
}

bool PauseState::Salir(const CEGUI::EventArgs &e) {
	popState();
	return true;
}