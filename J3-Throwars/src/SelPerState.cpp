/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#include "SelPerState.h"
#include "IntroState.h"
#include "PlayState.h"
#include "RecordsState.h"

template<> SelPerState* Ogre::Singleton<SelPerState>::msSingleton = 0;

void SelPerState::enter() {
    _root = Ogre::Root::getSingletonPtr();


    if (_root->hasSceneManager("SceneManager")) {
        _sceneMgr = _root->getSceneManager("SceneManager");
    } else {
        _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
    }

    if (_sceneMgr->hasCamera("MenusCamera")) {
        _camera = _sceneMgr->getCamera("MenusCamera");
    } else {
        _camera = _sceneMgr->createCamera("MenusCamera");
    }

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    
    _Player1Name= static_cast<CEGUI::String> (PlayState::getSingletonPtr()->getNamePlayer1());
    _Player2Name= static_cast<CEGUI::String> (PlayState::getSingletonPtr()->getNamePlayer2());
    
    if ( _Player1Name.empty()) {
         _Player1Name = "Rojo";
         _Player1Model = 0;
    }else{
        _Player1Model = SelPerState::getSingleton().getPlayer1Model();
    }
    if ( _Player2Name.empty()) {
         _Player2Name = "Azul";
         _Player2Model = 0;
    }else{
        _Player2Model = SelPerState::getSingleton().getPlayer2Model();
    }

    _models = 5;
    _posModels = new float [_models];
    _posModels[0] = 0.0;
    _posModels[1] = 50.0;
    _posModels[2] = 100.0;
    _posModels[3] = 150.0;
    _posModels[4] = 200.0;

    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();

    _camera1 = _sceneMgr->createCamera("Camera1");
    _camera1->setPosition(_posModels[_Player1Model], 0.0, 20.0);
    _camera1->lookAt(_posModels[_Player1Model], 0.0, 20.0);
    _camera1->setNearClipDistance(5);
    _camera1->setFarClipDistance(10000);

    _camera2 = _sceneMgr->createCamera("Camera2");
    _camera2->setPosition(_posModels[_Player2Model], 0.0, 20.0);
    _camera2->lookAt(_posModels[_Player2Model], 0.0, 20.0);
    _camera2->setNearClipDistance(5);
    _camera2->setFarClipDistance(10000);

    _camera1->setAspectRatio(width / height);
    _camera2->setAspectRatio(width / height);

    loadResources();
    GameManager::getSingletonPtr()->playTrackSelection();

    createScene();
    _animations.clear();
    _animations.push_back(_sceneMgr->getEntity("Mago")->getAnimationState("Idle"));
    _animations.push_back(_sceneMgr->getEntity("GolemFire")->getAnimationState("Idle"));
    _animations.push_back(_sceneMgr->getEntity("GolemIce")->getAnimationState("Idle"));
    _animations.push_back(_sceneMgr->getEntity("GolemStone")->getAnimationState("Idle"));
    _animations.push_back(_sceneMgr->getEntity("Dragon")->getAnimationState("Idle"));

    itAnimation = _animations.begin();
    while (itAnimation != _animations.end()) {
        (*itAnimation)->setEnabled(true);
        (*itAnimation)->setTimePosition(0.0);
        (*itAnimation)->setLoop(true);
        itAnimation++;
    }

    createGUI();

    _exitGame = false;

}

void SelPerState::exit() {
    CEGUI::ImagesetManager::getSingleton().destroy(CEGUI::ImagesetManager::getSingleton().get("RTTImageset"));
    CEGUI::ImagesetManager::getSingleton().destroy(CEGUI::ImagesetManager::getSingleton().get("RTTImageset2"));
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    _sceneMgr->destroyAllCameras();
    _sceneMgr->clearScene();

    _animations.clear();

    _root->getTextureManager()->removeAll();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void SelPerState::pause() {
    //GameManager::getSingletonPtr()->pauseTrackMenu();
}

void SelPerState::resume() {
    //createGUI();
    GameManager::getSingletonPtr()->playTrackSelection();
}

bool SelPerState::frameStarted(const Ogre::FrameEvent& evt) {

    Ogre::Real deltaT = evt.timeSinceLastFrame;

    itAnimation = _animations.begin();
    while (itAnimation != _animations.end()) {
        if (!(*itAnimation)->hasEnded()) {
            (*itAnimation)->addTime(deltaT);
        }
        itAnimation++;
    }

    CEGUI::System::getSingleton().injectTimePulse(deltaT);
    return true;
}

bool SelPerState::frameEnded(const Ogre::FrameEvent& evt) {
    if (_exitGame)
        return false;

    return true;
}

void SelPerState::keyPressed(const OIS::KeyEvent &e) {
    CEGUI::System::getSingleton().injectKeyDown(e.key);
    CEGUI::System::getSingleton().injectChar(e.text);
}

void SelPerState::keyReleased(const OIS::KeyEvent &e) {
    CEGUI::System::getSingleton().injectKeyUp(e.key);
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
}

void SelPerState::mouseMoved(const OIS::MouseEvent &e) {
    CEGUI::System &sys = CEGUI::System::getSingleton();
    sys.injectMouseMove(e.state.X.rel, e.state.Y.rel);
    // Scroll wheel.
    if (e.state.Z.rel)
        sys.injectMouseWheelChange(e.state.Z.rel / 120.0f);
}

CEGUI::MouseButton SelPerState::convertButton(OIS::MouseButtonID buttonID) {
    switch (buttonID) {
        case OIS::MB_Left:
            return CEGUI::LeftButton;

        case OIS::MB_Right:
            return CEGUI::RightButton;

        case OIS::MB_Middle:
            return CEGUI::MiddleButton;

        default:
            return CEGUI::LeftButton;
    }
}

void SelPerState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
}

void SelPerState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
}

SelPerState* SelPerState::getSingletonPtr() {
    return msSingleton;
}

SelPerState& SelPerState::getSingleton() {
    assert(msSingleton);
    return *msSingleton;
}

void SelPerState::createScene() {
    Ogre::Entity* ent1 = _sceneMgr->createEntity("Mago", "Mage.mesh");
    Ogre::Entity* ent2 = _sceneMgr->createEntity("GolemFire", "Golem.mesh");
    Ogre::Entity* ent3 = _sceneMgr->createEntity("GolemIce", "Golem.mesh");
    Ogre::Entity* ent4 = _sceneMgr->createEntity("GolemStone", "Golem.mesh");
    Ogre::Entity* ent5 = _sceneMgr->createEntity("Dragon", "Dragon.mesh");
    Ogre::SceneNode* node1 = _sceneMgr->createSceneNode("MageNode");
    Ogre::SceneNode* node2 = _sceneMgr->createSceneNode("GolemNodeFire");
    Ogre::SceneNode* node3 = _sceneMgr->createSceneNode("GolemNodeIce");
    Ogre::SceneNode* node4 = _sceneMgr->createSceneNode("GolemNodeStone");
    Ogre::SceneNode* node5 = _sceneMgr->createSceneNode("Dragon");
    node1->setPosition(Ogre::Vector3(0, 1.5, 0));
    node1->scale(0.7, 0.7, 0.7);
    node1->attachObject(ent1);
    
    node2->setPosition(Ogre::Vector3(50, -7, 0));
    node2->scale(2, 2, 2);
    node2->attachObject(ent2);
    
    node3->setPosition(Ogre::Vector3(100, -7, 0));
    node3->scale(2, 2, 2);
    ent3->setMaterialName("Golem_Ice");
    node3->attachObject(ent3);
    
    node4->setPosition(Ogre::Vector3(150, -7, 0));
    node4->scale(2, 2, 2);
    ent4->setMaterialName("Golem_Stone");
    node4->attachObject(ent4);
    
    node5->setPosition(Ogre::Vector3(200, -7, 0));
    node5->scale(4.5, 4.5, 4.5);
    node5->attachObject(ent5);

    _sceneMgr->getRootSceneNode()->addChild(node1);
    _sceneMgr->getRootSceneNode()->addChild(node2);
    _sceneMgr->getRootSceneNode()->addChild(node3);
    _sceneMgr->getRootSceneNode()->addChild(node4);
    _sceneMgr->getRootSceneNode()->addChild(node5);

}

void SelPerState::createGUI() {
    _CEGUIRenderer = IntroState::getSingleton().getCEGUIRenderer();
    CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout("SelPer.layout");
    //Start button
    CEGUI::WindowManager::getSingleton().getWindow("Selper/PlayerVsPlayerButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&SelPerState::HvsH, this));
    //P1 Left button
    CEGUI::WindowManager::getSingleton().getWindow("Selper/Player1/LeftButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&SelPerState::Per1Izq, this));
    //P1 Right button
    CEGUI::WindowManager::getSingleton().getWindow("Selper/Player1/RightButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&SelPerState::Per1Der, this));
    //P2 Left button
    CEGUI::WindowManager::getSingleton().getWindow("Selper/Player2/LeftButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&SelPerState::Per2Izq, this));
    //P2 Right button
    CEGUI::WindowManager::getSingleton().getWindow("Selper/Player2/RightButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&SelPerState::Per2Der, this));
    //Start button
    CEGUI::WindowManager::getSingleton().getWindow("Selper/PlayerVsCPUButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&SelPerState::HvsCPU, this));
    //Menu button
    CEGUI::WindowManager::getSingleton().getWindow("Selper/MenuButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&SelPerState::VolverMenu, this));
    //Exit button
    CEGUI::WindowManager::getSingleton().getWindow("Selper/ExitButton")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&SelPerState::Salir, this));

    //Render Texture - to - target
    Ogre::TexturePtr tex = _root->getTextureManager()->getByName("RTT", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    if (tex.isNull()) {
        tex = _root->getTextureManager()->createManual(
                "RTT",
                Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                Ogre::TEX_TYPE_2D,
                512,
                512,
                0,
                Ogre::PF_R8G8B8,
                Ogre::TU_RENDERTARGET);

        Ogre::TexturePtr tex2 = _root->getTextureManager()->getByName("RTT2", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
        if (tex2.isNull()) {
            tex2 = _root->getTextureManager()->createManual(
                    "RTT2",
                    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                    Ogre::TEX_TYPE_2D,
                    512,
                    512,
                    0,
                    Ogre::PF_R8G8B8,
                    Ogre::TU_RENDERTARGET);
        }

        Ogre::RenderTexture* rtex = tex->getBuffer()->getRenderTarget();
        Ogre::RenderTexture* rtex2 = tex2->getBuffer()->getRenderTarget();

        if (!rtex->hasViewportWithZOrder(0)) {
            Ogre::Viewport* v = rtex->addViewport(_camera1);
            v->setOverlaysEnabled(false);
            v->setClearEveryFrame(true);
        }
        if (!rtex2->hasViewportWithZOrder(0)) {
            Ogre::Viewport* v2 = rtex2->addViewport(_camera2);
            v2->setOverlaysEnabled(false);
            v2->setClearEveryFrame(true);
        }
        CEGUI::Texture& guiTex = _CEGUIRenderer->createTexture(tex);
        CEGUI::Texture& guiTex2 = _CEGUIRenderer->createTexture(tex2);

        CEGUI::Imageset& imageSet = CEGUI::ImagesetManager::getSingleton().create("RTTImageset", guiTex);
        imageSet.defineImage("RTTImage",
                CEGUI::Point(0.0f, 0.0f),
                //            CEGUI::Size(guiTex.getSize().d_width, guiTex.getSize().d_height),
                CEGUI::Size(512, 512),
                CEGUI::Point(0.0f, 0.0f));
        CEGUI::Imageset& imageSet2 = CEGUI::ImagesetManager::getSingleton().create("RTTImageset2", guiTex2);
        imageSet2.defineImage("RTTImage",
                CEGUI::Point(0.0f, 0.0f),
                //            CEGUI::Size(guiTex2.getSize().d_width, guiTex2.getSize().d_height),
                CEGUI::Size(512, 512),
                CEGUI::Point(0.0f, 0.0f));
        CEGUI::Window* RTTWindow = CEGUI::WindowManager::getSingleton().getWindow("Selper/Player1/Per");

        RTTWindow->setProperty("Image", CEGUI::PropertyHelper::imageToString(&imageSet.getImage("RTTImage")));

        RTTWindow = CEGUI::WindowManager::getSingleton().getWindow("Selper/Player2/Per");

        RTTWindow->setProperty("Image", CEGUI::PropertyHelper::imageToString(&imageSet2.getImage("RTTImage")));

    }
    
    CEGUI::WindowManager::getSingleton().getWindow("Selper/Player1/NickEditBox")->setText(_Player1Name.c_str());
    CEGUI::WindowManager::getSingleton().getWindow("Selper/Player2/NickEditBox")->setText(_Player2Name.c_str());
    
    CEGUI::System::getSingleton().setGUISheet(sheet);
}

bool SelPerState::Salir(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    _exitGame = true;
    return true;
}

bool SelPerState::VolverMenu(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    changeState(IntroState::getSingletonPtr());
    return true;
}

bool SelPerState::HvsH(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    PlayState::getSingletonPtr()->setGameMode(MODE_PH_V_PH);
    NewGame();
    return true;
}

bool SelPerState::HvsCPU(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();
    PlayState::getSingletonPtr()->setGameMode(MODE_PH_V_CPU);
    NewGame();
    return true;
}

bool SelPerState::Per1Izq(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();

    if (_Player1Model > 0)
        _Player1Model--;
    else
        _Player1Model = _models - 1;
    ReajustarCameras();

    return true;
}

bool SelPerState::Per1Der(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();

    if (_Player1Model < _models)
        _Player1Model++;
    else
        _Player1Model = 0;
    ReajustarCameras();
    return true;
}

bool SelPerState::Per2Izq(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();

    if (_Player2Model > 0)
        _Player2Model--;
    else
        _Player2Model = _models - 1;

    ReajustarCameras();

    return true;
}

bool SelPerState::Per2Der(const CEGUI::EventArgs &e) {
    GameManager::getSingletonPtr()->playSoundClick();

    if (_Player2Model < 3)
        _Player2Model++;
    else
        _Player2Model = 0;

    ReajustarCameras();

    return true;
}

void SelPerState::ReajustarCameras() {
    _camera1->setPosition(_posModels[_Player1Model], 0.0, 20.0);
    _camera1->lookAt(_posModels[_Player1Model], 0.0, 20.0);
    _camera2->setPosition(_posModels[_Player2Model], 0.0, 20.0);
    _camera2->lookAt(_posModels[_Player2Model], 0.0, 20.0);
}

void SelPerState::NewGame() {
    
    _Player1Name = CEGUI::WindowManager::getSingleton().getWindow("Selper/Player1/NickEditBox")->getText();
    _Player2Name = CEGUI::WindowManager::getSingleton().getWindow("Selper/Player2/NickEditBox")->getText();

    CEGUI::String world = CEGUI::WindowManager::getSingleton().getWindow("Selper/SelectWorld")->getText();

    PlayState::getSingletonPtr()->setPlayerName(_Player1Name.c_str(), _Player2Name.c_str());
    PlayState::getSingletonPtr()->setWorldModel(world.c_str());
    changeState(PlayState::getSingletonPtr());
}

void SelPerState::loadResources() {
    Ogre::ConfigFile cf;
    cf.load("resources.cfg");

    Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
    Ogre::String sectionstr, typestr, datastr;
    while (sI.hasMoreElements()) {
        sectionstr = sI.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i) {
            typestr = i->first;
            datastr = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation
                    (datastr, typestr, sectionstr);
        }
    }
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}
