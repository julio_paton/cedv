/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef SelPerState_H
#define SelPerState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include "CEGUI.h"
#include "RendererModules/Ogre/CEGUIOgreRenderer.h"

#include "GameState.h"

class SelPerState : public Ogre::Singleton<SelPerState>, public GameState {
public:

    SelPerState() {
    }

    void enter();
    void exit();
    void pause();
    void resume();

    void loadResources();
    void createScene();

    void keyPressed(const OIS::KeyEvent &e);
    void keyReleased(const OIS::KeyEvent &e);

    void mouseMoved(const OIS::MouseEvent &e);
    void mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);

    bool frameStarted(const Ogre::FrameEvent& evt);
    bool frameEnded(const Ogre::FrameEvent& evt);

    int getPlayer1Model() {
        return _Player1Model;
    }

    int getPlayer2Model() {
        return _Player2Model;
    }
    // Heredados de Ogre::Singleton.
    static SelPerState& getSingleton();
    static SelPerState* getSingletonPtr();

    // Miembros para widgets menu principal graficamente
    CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);
    void createGUI();

    // Miembros para widgets menu pricipal logicamente
    bool HvsH(const CEGUI::EventArgs &e);
    bool HvsCPU(const CEGUI::EventArgs &e);
    bool VolverMenu(const CEGUI::EventArgs &e);
    bool Salir(const CEGUI::EventArgs &e);

    bool Per1Izq(const CEGUI::EventArgs &e);
    bool Per1Der(const CEGUI::EventArgs &e);

    bool Per2Izq(const CEGUI::EventArgs &e);
    bool Per2Der(const CEGUI::EventArgs &e);

    void ReajustarCameras();

    void NewGame();

protected:
    Ogre::Root* _root;
    Ogre::SceneManager* _sceneMgr;
    Ogre::Viewport* _viewport;
    Ogre::Camera* _camera;
    Ogre::Camera* _camera1;
    Ogre::Camera* _camera2;
    CEGUI::OgreRenderer* _CEGUIRenderer;
    Ogre::AnimationState *_animState;
    Ogre::AnimationState *_animState2;
    int _models;
    float *_posModels;
    //float _pos[3];
    int _Player1Model;
    int _Player2Model;
    CEGUI::String _Player1Name;
    CEGUI::String _Player2Name;

    std::vector<Ogre::AnimationState*> _animations;
    std::vector<Ogre::AnimationState*>::iterator itAnimation;

    bool _exitGame;
};

#endif
