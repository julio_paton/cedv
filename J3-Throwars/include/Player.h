/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef PLAYER_H
#define PLAYER_H

//Modo de jugador
#define PLAYER_HUMAN_1 10 // humano1
#define PLAYER_HUMAN_2 11 // humano2
#define PLAYER_CPU 12     // CPU
//Mascaras para el ray query
#define PLAYER_AMIGO 1 << 1    // Mascara para el PLAYER AMIGO
#define PLAYER_ENEMIGO 1 << 2  // Mascara para el PLAYER ENEMIGO

#include "Player.h"
#include <Ogre.h>
#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsSphereShape.h>
#include "Constants.h"

using namespace std;

class Player {
public:

	enum Animations {
		IDLE, FIRE, DEATH, NONE
	};

	Player(string id, std::string model, short typePlayer, short collidesWith,
			OgreBulletDynamics::DynamicsWorld * world, Ogre::Vector3 position,
			Ogre::Quaternion rotation, float scale, string materialName ="");
	~Player();

	string getId() {
		return _id;
	}
	int getVida() {
		return _vida;
	}
	int getTypePlayer() {
		return _typePlayer;
	}

	void dispararMisil(float force, const Ogre::Vector3 &vDirection);
	void tocado(int fuerzaMisil);

	Ogre::SceneNode* getNode();
	Ogre::Entity* getEntity() {
		return _entidad;
	}

	OgreBulletDynamics::RigidBody *getRigidBody() const;
	void setRigidBody(OgreBulletDynamics::RigidBody *value);

	//Weapon
	void setWeaponOptions(string weaponModel, const float bodyRestitution,
			const float bodyFriction, const float bodyMass,
			const Ogre::Vector3 pos, const Ogre::Quaternion quat, const float scale);
	void setShootConditions(float angle, float force);
	Ogre::SceneNode* getWeaponNode() {
		return _weaponNode;
	}

	//Animaciones
	void selectAnimation(const int pIndex, bool pLoop = false);
	void disableAnimations();
	void update(Ogre::Real pTime);

private:

	short _typePlayer;
	Ogre::SceneNode *_nodeModel;
	Ogre::Entity *_entidad;

	OgreBulletDynamics::RigidBody *_rigidBody;
	OgreBulletCollisions::CollisionShape *_bodyShape;
	OgreBulletDynamics::DynamicsWorld * _world;

	string _id;
	int _vida;

	//Weapon
	int _shootsCount;
	string _weaponModel;
	float _weaponBodyRestitution;
	float _weaponBodyFriction;
	float _weaponBodyMass;
	float _weaponScale;
	Ogre::Vector3 _weaponPosInicialWeapon;
	Ogre::Quaternion _weaponQuat;
	Ogre::SceneNode *_weaponNode;

	float _angle;
	float _force;

	//Animaciones
	std::vector<Ogre::AnimationState*>::iterator itAnimation;
	std::vector<Ogre::AnimationState*> _animations;
	Ogre::AnimationState* _activeAnimation;

	void shoot();
	void dead();
};

#endif
