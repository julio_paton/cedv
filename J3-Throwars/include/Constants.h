#ifndef CONSTANTS_H
#define CONSTANTS_H

#define BIT(X) (1<<(X))

namespace BulletConstants {

enum collisiontypes {
	COL_NOTHING = 0, //Collide with nothing
	COL_PLAYER1 = BIT(1), //Collide with players
	COL_PLAYER2 = BIT(2), //Collide with players
	COL_WEAPONS_PL1 = BIT(3), //Collide with weapons
	COL_WEAPONS_PL2 = BIT(4), //Collide with weapons
	COL_BUILDINGS = BIT(5), //Collide with buildings
};

const short player1CollidesWith = COL_BUILDINGS | COL_WEAPONS_PL2;
const short player2CollidesWith = COL_BUILDINGS | COL_WEAPONS_PL1;
const short weaponsPl1CollideWith = COL_PLAYER2 | COL_BUILDINGS;
const short weaponsPl2CollideWith = COL_PLAYER1 | COL_BUILDINGS;
const short buildingsCollideWith = COL_BUILDINGS | COL_PLAYER1 | COL_PLAYER2
		| COL_WEAPONS_PL1 | COL_WEAPONS_PL2;

}

namespace PlayerConstants {
//Energía máxima que se aplica al objeto
const float max_power = 20;
//Tiempo en milisegundos en el que se aplica la máxima energía
const unsigned long max_power_time = 1500;
}

#endif // CONSTANTS_H
