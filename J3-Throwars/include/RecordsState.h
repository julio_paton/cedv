/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef RecordsState_H
#define RecordsState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include "CEGUI.h"
#include "RendererModules/Ogre/CEGUIOgreRenderer.h"

#include "GameState.h"
#include "ScoresData.h"

class RecordsState : public Ogre::Singleton<RecordsState>, public GameState
{
 public:
  RecordsState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();
  
  void loadResources();
  void createScene();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static RecordsState& getSingleton ();
  static RecordsState* getSingletonPtr ();
  
  // Miembros para widgets menu principal graficamente
  CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);
  void createGUI();
  
  // Miembros para widgets menu pricipal logicamente
  bool VolverMenu(const CEGUI::EventArgs &e);
  bool RecIzq(const CEGUI::EventArgs &e);
  bool RecDer(const CEGUI::EventArgs &e);
  bool Salir(const CEGUI::EventArgs &e);

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;
  CEGUI::OgreRenderer* _CEGUIRenderer;
  
  void loadDataWorld();
  void inicializarCEGUI();
  
  
  int _world;
  int _totalWorlds;
  bool _exitGame;
};

#endif
