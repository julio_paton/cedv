/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef IntroState_H
#define IntroState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include "CEGUI.h"
#include "RendererModules/Ogre/CEGUIOgreRenderer.h"

#include "GameState.h"

class IntroState : public Ogre::Singleton<IntroState>, public GameState
{
 public:
  IntroState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();
  
  void loadResources();
  void createScene();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static IntroState& getSingleton ();
  static IntroState* getSingletonPtr ();
  
  // Miembros para widgets menu principal graficamente
  CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);
  CEGUI::OgreRenderer* getCEGUIRenderer(){return _CEGUIRenderer;}
  void createGUI();
  
  // Miembros para widgets menu pricipal logicamente
  bool Empezar(const CEGUI::EventArgs &e);
  bool Opciones(const CEGUI::EventArgs &e);
  bool Ayuda(const CEGUI::EventArgs &e);
  bool Salir(const CEGUI::EventArgs &e);
  bool Creditos(const CEGUI::EventArgs &e);
  bool Records(const CEGUI::EventArgs &e);

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;
  CEGUI::OgreRenderer* _CEGUIRenderer;

  bool _exitGame;
};

#endif
