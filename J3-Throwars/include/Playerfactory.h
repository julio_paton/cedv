#ifndef PLAYERFACTORY_H
#define PLAYERFACTORY_H

#include "Player.h"
#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>
#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "Constants.h"

class PlayerFactory
{
public:
    PlayerFactory(OgreBulletDynamics::DynamicsWorld *world) : _nextId(0), _world(world){}
    Player* makeMageWithFireball(short player);
    Player* makeGolemWithFireball(short player, string material);
    Player* makeDragonWithFireball(short player);

private:
    int _nextId;
    OgreBulletDynamics::DynamicsWorld * _world;
	OgreBulletCollisions::StaticMeshToShapeConverter *_trimeshConverter;


};

#endif // PLAYERFACTORY_H
