/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef FinishState_H
#define FinishState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include "CEGUI.h"
#include "RendererModules/Ogre/CEGUIOgreRenderer.h"

#include "GameState.h"
#include "PlayState.h"
#include "ScoresData.h"

class FinishState : public Ogre::Singleton<FinishState>, public GameState
{
 public:
  FinishState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();
  
  void loadResources();
  void createScene();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static FinishState& getSingleton ();
  static FinishState* getSingletonPtr ();
  
  // Miembros para widgets menu principal graficamente
  CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);
  
  void createGUI();
  
  void setWinner(String player,String playerName, int puntuacion);
  
  // Miembros para widgets menu pricipal logicamente
  bool SiguientePantalla(const CEGUI::EventArgs &e);
  bool NuevaPartida(const CEGUI::EventArgs &e);
  bool RepetirPartida(const CEGUI::EventArgs &e);
  bool Salir(const CEGUI::EventArgs &e);

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;
  CEGUI::OgreRenderer* _CEGUIRenderer;
  
  void initializeVars();

  bool _exitGame;
  String _playerWin;
  String _playerName;
  int _puntuacion;
};

#endif
