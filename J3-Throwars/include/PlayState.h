/*********************************************************************
 * Juego Throwars
 * Curso de Experto en Desarrollo de Videojuegos
 *
 * Autores:     Juan Ramón Giménez Sanz
 *              Julio Alberto Patón Incertis
 *              Marina Garrido Sánchez
 *
 *********************************************************************/

#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <ctime>

#include "GameState.h"
#include "PauseState.h"
#include "SelPerState.h"
#include "Player.h"
#include "Playerfactory.h"
#include "Constants.h"


#include <CEGUI.h>
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>

#include <OgreTimer.h>
#include "LinearMath/btTransform.h"

//Turnos
#define PLAYER_ONE 1       // Jugador uno
#define PLAYER_TWO 2       // Jugador dos
//Modo de jugador
#define MODE_PH_V_PH 20 // humano VS humano
#define MODE_PH_V_CPU 21 // humano VS cpu

using namespace Ogre;
using namespace OgreBulletCollisions;
using namespace OgreBulletDynamics;

class PlayState : public Ogre::Singleton<PlayState>, public GameState {
public:

    PlayState() {
    }

    void enter();
    void exit();
    void pause();
    void resume();

    void keyPressed(const OIS::KeyEvent &e);
    void keyReleased(const OIS::KeyEvent &e);

    void changeView(const OIS::KeyEvent &e);

    void mouseMoved(const OIS::MouseEvent &e);
    void mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);

    bool frameStarted(const Ogre::FrameEvent& evt);
    bool frameEnded(const Ogre::FrameEvent& evt);

    // Heredados de Ogre::Singleton.
    static PlayState& getSingleton();
    static PlayState* getSingletonPtr();

    void newGame();

    void setViento(float viento);
    void setGravedad(float graveda);
    void setPuntuacion(int puntuacion);
    void setGameMode(int mode);
    void setPlayerName(string name1, string name2);
    void setWorldModel(string worldModel);
    
    
    void gameIsOver(string player);
    
    void turnChanged();
    
    int calcularDano();
    
    float getPower(){return _power;}
    string getWorldModel(){return _worldModel;}


    float getGravedad() {
        return _gravedad;
    }

    float getViento() {
        return _viento;
    }

    int getTurno() {
        return _turno;
    }

    int getGameMode() {
        return _gameMode;
    }

    string getNamePlayer1() {
        return _namePlayer1;
    }

    string getNamePlayer2() {
        return _namePlayer2;
    }

    void salir() {
        _exitGame = true;
    }
private:
    InputManager* _mInputDevice;
  Ogre::Timer* m_pTimer;
protected:
    Ogre::Root* _root;
    Ogre::SceneManager* _sceneMgr;
    Ogre::Viewport* _viewport;
    Ogre::Camera* _camera;
    OverlayManager* _overlayManager;

    Ogre::SceneNode *_mundoNode;

    //Players & weapons
    PlayerFactory *_playerFactory;

    Player *_playerOne;
    Player *_playerTwo;
    Player *_activePlayer;

    Ogre::Real _deltaT;
    float _gravedad;
    float _viento;
    
    string _worldModel;

    //Puntuaciones
    int _disparosP1;
    int _disparosP2;

    int _turnTime;
    float _power;
    float _angleP1;
    float _angleP2;
    bool _shootInCharge;
    bool _angleUp;
    bool _angleDown;
    bool _stopTime;
    bool _gameIsOver;
    bool _waitForColision;
    int _turno;
    int _gameMode;



    string _namePlayer1;
    string _namePlayer2;

    //Particles
    Ogre::ParticleSystem* psExplosion;
    Ogre::SceneNode* psExplosionNode;

    //OGREBullet
    OgreBulletDynamics::DynamicsWorld * _world;

    std::deque<OgreBulletDynamics::RigidBody *> _bodies;
    std::deque<OgreBulletCollisions::CollisionShape *> _shapes;

    btVector3 _currentGravity;

    void createScene();
    //Metodos del mundo
    void World1();
    void World2();
    void World3();

    void createGUI();
    void initializeVars();

    void DetectCollision();

    void createOverlay();


    void resetParticleSystem(Ogre::ParticleSystem *ps);

    bool _exitGame;
    //Controlador de la musica en funcion del mundo
    void sound();


};

#endif
